<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('zCustomers')){ 
            Schema::create('zCustomers', function (Blueprint $table) {
                            $table->increments('id');
                            $table->string('first_name',100);
                            $table->string('last_name',100);
                            $table->integer('user_id')->unsigned();
                            $table->string('email',100);
                            $table->string('search_history')->nullable();
                            $table->string('currency',20);
                            $table->boolean('is_confirmed')->default(0);
                            $table->timestamps();
                            $table->foreign('user_id')->references('id')->on('users');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zCustomers');
    }
}
