<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblproviderpickupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('tblproviderpickups'))
        {
        Schema::create('tblproviderpickups', function (Blueprint $table) {
            $table->increments('pickup_id');
            $table->integer('provider_id');
            $table->dateTime('pickup_time');
            $table->string('description')->nullable();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblproviderpickups');
    }
}
