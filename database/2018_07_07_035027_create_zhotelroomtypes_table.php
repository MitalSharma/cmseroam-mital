<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZhotelroomtypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('zhotelroomtypes')){
        Schema::create('zhotelroomtypes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('pax')->nullable();
            $table->integer('sequence')->nullable();
            $table->tinyInteger('is_dorm')->default(0);
            $table->string('dorm_pax')->nullable();
            $table->tinyInteger('female_only')->nullable();
            $table->timestamps();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zhotelroomtypes');
    }
}
