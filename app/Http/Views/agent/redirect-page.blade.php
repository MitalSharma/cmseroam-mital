@extends('layout/mainlayout')

@section('content')
Redirecting to Frontend...
<form id="invisible_form" action="http://{{$domain}}/get-agent-data" method="post">
  <input id="new_window_parameter_1" name="agent" type="hidden" value="{{ $agent }}">
  <input id="new_window_parameter_2" name="user" type="hidden" value="{{ $user }}">
  <input id="new_window_parameter_3" name="customer" type="hidden" value="{{ $customer }}">
  <input id="new_window_parameter_4" name="prefrences" type="hidden" value="{{ $prefrences }}">
  @if(!empty($saved_trip))
  <input id="new_window_parameter_5" name="saved_trip" type="hidden" value="{{ $saved_trip }}">
  @endif
</form>


<script type="text/javascript">
	$('#invisible_form').submit();
	
</script>

@endsection