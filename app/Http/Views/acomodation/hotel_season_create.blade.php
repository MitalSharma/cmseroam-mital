@extends( 'layout/mainlayout' )

@section('content')
<div class="content-container">
    @if(isset($oSeason))
        <h1 class="page-title">{{ trans('messages.update',['name' => 'Accomodation Season']) }}</h1>
    @else
        <h1 class="page-title">{{ trans('messages.add',['name' => 'Accomodation Season']) }}</h1>
    @endif
    <div class="row">
        @if (Session::has('message'))
        <div class="small-12 small-centered columns success-box">{{ Session::get('message') }}</div>
        @endif

    </div>
    <br>
    <?php $sDisable = ''; ?>
    @if(isset($oSeason))
    <?php $sDisable = 'readonly'; ?>
   {{ Form::model($oSeason, array('url' => route('acomodation.hotel-season-create') ,'method'=>'POST','enctype'=>'multipart/form-data')) }}
   @else
   {{Form::open(array('url' => route('acomodation.hotel-season-create'),'method'=>'Post','enctype'=>'multipart/form-data')) }}
   @endif
    <div class="box-wrapper">
        <p>Hotel & Supplier</p>      
        @if((auth::user()->type) !="admin")
                    <div class="form-group m-t-30">
                        <label class="label-control">Domains <span class="required">*</span></label>
                            <select  class="form-control" multiple="true" name="domains[]">
                                @if(isset($DomainList) && $DomainList->count() > 0)
                                    @foreach($DomainList as $key=>$domain)
                                        <option @if(isset($oSeason))@if ( in_array( $domain->id, explode(",",$oSeason->domain_ids) ) ) selected   @endif  @endif	value="{{$domain->id}}"  id="domain_{{$domain->id}}" >{{$domain->name}}</option>
                                    @endforeach
                                @endif	
                            </select>
                    </div>  
         @endif                       
        @if($nFromFlag == '')
            <div class="form-group m-t-30">
                <label class="label-control">Hotel <span class="required">*</span></label>
               {{ Form::select('hotel_id',$aHotels,Input::old('hotel_id'),['class'=>'form-control','id'=>'hotel_id', $sDisable])}}
            </div>
            @if ( $errors->first( 'hotel_id' ) )
            <small class="error">{{ $errors->first('hotel_id') }}</small>
            @endif
        @endif	  
        <div class="form-group m-t-30">
            <label class="label-control">Supplier <span class="required">*</span></label>
            {{ Form::select('hotel_supplier_id',$oSupplier,Input::old('hotel_supplier_id'),['class'=>'form-control', $sDisable])}}
        </div>
        @if ( $errors->first( 'hotel_supplier_id' ) )
        <small class="error">{{ $errors->first('hotel_supplier_id') }}</small>
        @endif
        <div class="form-group m-t-30">
            <label class="label-control">Season Name <span class="required">*</span></label>
            {{Form::text('name',Input::old('name'),['placeholder'=>'Enter Season Name','id'=>'name','class'=>'form-control',$sDisable])}}
        </div>
        @if ( $errors->first( 'name' ) )
        <small class="error">{{ $errors->first('name') }}</small>
        @endif
        <div class="form-group m-t-30">
            <label class="label-control">Currency</label>
            {{Form::select('currency_id',$oCurrencies,Input::old('currency_id'),['id'=>'currency_id','class'=>'form-control'])}}
        </div>
    </div>
    
    <div class="box-wrapper"> 
        <p>Date</p>  
        <div class="form-group m-t-30">
            <label class="label-control">Starting From <span class="required">*</span></label>
            {{Form::text('from',Input::old('from'),['placeholder'=>'Select Date','id'=>'from','class'=>'form-control'])}}
        </div>
        @if ( $errors->first( 'from' ) )
        <small class="error">{{ $errors->first('from') }}</small>
        @endif
        <div class="form-group m-t-30">
            <label class="label-control">Until <span class="required">*</span></label>
            {{Form::text('to',Input::old('to'),['placeholder'=>'Select Date','id'=>'to','class'=>'form-control'])}}
        </div>
        @if ( $errors->first( 'to' ) )
        <small class="error">{{ $errors->first('to') }}</small>
        @endif
    </div>
    
    <div class="box-wrapper">
        <p>Cancellation Policy</p>

        <div class="form-group m-t-30">
            {{Form::textarea('cancellation_policy',Input::old('cancellation_policy'),['placeholder'=>'Description','id'=>'cancellation_policy','class'=>'form-control'])}}
        </div> 
    </div>  
     <div class="box-wrapper">
        <p>{{ trans('messages.cancellation_policy_percent') }}</p>
        <div class="row tb_added">
            @if(Input::old('days'))
                @foreach(Input::old('days') as $key => $day)
                <div class="col-sm-6">
                    <div class="form-group m-t-30">
                        <label class="label-control">{{ trans('messages.day_before') }}</label>

                        <input placeholder="Enter Days Before" type="number" class="form-control" name="days[]" min="1" id="days" value="{{$day}}">
                    </div> 
                </div> 
                <div class="col-sm-6">
                    <div class="form-group m-t-30">
                        <label class="label-control">{{ trans('messages.percentage') }}</label>

                        <input placeholder="Enter Percentage" type="number" class="form-control" name="percent[]" min="1" id="percent" value="{{Input::old('percent')[$key]}}">

                    </div> 
                </div> 
                @endforeach
            @elseif(isset($decoded_formula))
                @foreach ($decoded_formula as $key => $formula)
                <div class="row tb_added">
                    <div class="col-sm-6">
                        <div class="form-group m-t-30">
                            <label class="label-control">{{ trans('messages.day_before') }}</label>
                            <input type="number" class="form-control" name="days[]" min="1" id="days" value="{{$formula->days}}" placeholder="Enter Days Before">
                        </div> 
                    </div> 
                    <div class="col-sm-6">
                        <div class="form-group m-t-30">
                            <label class="label-control">{{ trans('messages.percentage') }}</label>
                            <input type="number" class="form-control" name="percent[]" min="1" id="percent" value="{{$formula->percent}}" placeholder="Enter Percentage">
                        </div> 
                    </div> 
                </div>
                @endforeach
            @else
                <div class="col-sm-6">
                    <div class="form-group m-t-30">
                        <label class="label-control">{{ trans('messages.day_before') }}</label>
                        {{Form::number('days[]',Input::old('days.0'),['id'=>'days','min'=>'1','class'=>'form-control','placeholder'=>'Enter Days Before'])}}
                    </div> 
                </div> 
                <div class="col-sm-6">
                    <div class="form-group m-t-30">
                        <label class="label-control">{{ trans('messages.percentage') }}</label>
                        {{Form::number('percent[]',Input::old('percent.0'),['id'=>'percent','min'=>'1','class'=>'form-control','placeholder'=>'Enter Percentage'])}}

                    </div> 
                </div> 
            @endif

        </div> 
        <div class="clones"></div>
        <div class="row">
            <div class="col-sm-12">
                <div class="text-center">
                    <i class="fa fa-minus-square right fa-2x" id="fa-minus" aria-hidden="true"></i>
                    <i class="fa fa-plus-square right fa-2x" id="fa-plus" aria-hidden="true"></i>
                </div> 
            </div>      
        </div>     
    </div>  
    <div class="m-t-20 row col-md-8 col-md-offset-2">
        <div class="row">
            <div class="col-sm-6">
                {{Form::submit('Save',['class'=>'button success btn btn-primary btn-block']) }}
            </div>
            <div class="col-sm-6">
                <a href="{{ route('acomodation.hotel-season-list') }}" class="btn btn-primary btn-block">Cancel</a>
            </div>
        </div>
    </div>	
    {{ Form::hidden('id', $nId) }}
    {{ Form::hidden('from_flag', $nFromFlag) }}
    {{Form::close()}}
</div>
@stop

@section('custom-css')
<style>
    .error{
        color:red !important;
    }
    .with_error{
        border-color: red !important;
    }

    div .with_error{
        border:1px solid black;
    }
    .error_message{
        color:red !important;
    }
    .success_message{
        color:green !important;
        text-align: center;
    }
    .fa-plus-square{
        color:green;
        cursor:pointer;
    }
    .fa-minus-square{
        color:red;
        cursor:pointer;
    }
</style>
@stop

@section('custom-js')
<script src="{{ asset('assets/js/star-rating.min.js') }}"></script>
<script>
tinymce.init({
    selector: '#cancellation_policy',
    height: 250,
    menubar: false
});
tinymce.init({
    selector: '#description',
    height: 250,
    menubar: false
});
</script>
<script>


    $(function () {

        $("#from,#to").datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true
        });

    })


    $('#fa-plus').click(function () {
        var count = $('.clones').children().length+1;
        $('.tb_added:last').clone().appendTo(".clones");
        var numItems = $('.tb_added').length;
        $('.tb_added:last input').val('');

    });
    $('#fa-minus').click(function () {
        var numItems = $('.tb_added').length;
        if (numItems > 1) {
            $('.tb_added:last').remove();
        }
    });

    $(document).ready(function(){
        $('#hotel_id').change(function(){
            //alert('test');
        });
    });

</script>
@stop
