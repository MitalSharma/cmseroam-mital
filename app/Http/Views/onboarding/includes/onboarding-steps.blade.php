<?php
if(isset($id) && $id!=''){ $licensee_id = $id; } else { $licensee_id = ''; } 
?>
<div class="boardingSteps">
    <ul>
      <li class="@if(request()->segment(2) === 'general') {{ 'active' }} @endif">
        <a href="<?php if($licensee_id!=''){ echo route('onboarding.general',$licensee_id); } else { echo '#'; } ?>">
          <span class="stepCircle">1</span>
          <p>General</p>
        </a>
      </li>
      <li class="@if(request()->segment(2) === 'frontend') {{ 'active' }} @endif"> 
        <a href="<?php if($licensee_id!=''){ echo route('onboarding.frontend',$licensee_id); } else { echo '#'; } ?>">
          <span class="stepCircle">2</span>
          <p>Front-End Configuration</p>
        </a>
      </li>
      <li class="@if(request()->segment(2) === 'backend') {{ 'active' }} @endif">
        <a href="<?php if($licensee_id!=''){ echo route('onboarding.backend',$licensee_id); } else { echo '#'; } ?>">
          <span class="stepCircle">3</span>
          <p>Back-End Configuration</p>
        </a>
      </li>
      <li class="@if(request()->segment(2) === 'geodata') {{ 'active' }} @endif">
        <a href="<?php if($licensee_id!=''){ echo route('onboarding.geodata',$licensee_id); } else { echo '#'; } ?>">
          <span class="stepCircle">4</span>
          <p>GeoData</p>
        </a>
      </li>
      <li class="@if(request()->segment(2) === 'inventory') {{ 'active' }} @endif">
        <a href="<?php if($licensee_id!=''){ echo route('onboarding.inventory',$licensee_id); } else { echo '#'; } ?>">
          <span class="stepCircle">5</span>
          <p>Inventory MarketPlace</p>
        </a>
      </li>
      <li class="@if(request()->segment(2) === 'usermanagement') {{ 'active' }} @endif"> 
        <a href="<?php if($licensee_id!=''){ echo route('onboarding.usermanagement',$licensee_id); } else { echo '#'; } ?>">
          <span class="stepCircle">6</span>
          <p>User Management</p>
        </a>
      </li>
      <li class="@if(request()->segment(2) === 'pricing') {{ 'active' }} @endif">
        <a href="<?php if($licensee_id!=''){ echo route('onboarding.pricing',$licensee_id); } else { echo '#'; } ?>">
          <span class="stepCircle">7</span>
          <p>Account Configuration</p>
        </a>
      </li>
      <li class="@if(request()->segment(2) === 'account') {{ 'active' }} @endif">
        <a href="<?php if($licensee_id!=''){ echo route('onboarding.account',$licensee_id); } else { echo '#'; } ?>">
          <span class="stepCircle">8</span>
          <p>Account Notes</p>
        </a>
      </li>
    </ul>
</div>