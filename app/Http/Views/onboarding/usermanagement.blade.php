@extends( 'layout/mainlayout' )
@section('content')
<div class="content-container">
    <div class="m-t-10">
        <?php
        if(isset($licensee_id) && $licensee_id!=''){ $licensee_id = $licensee_id; } else { $licensee_id = ''; } 
        ?>
        @include('WebView::onboarding.includes.onboarding-steps', array('id' => $licensee_id))
    </div>
    <div class="alert" role="alert" id="error_msg"></div>
    <h1 class="page-title">Step 6: User Management</h1>
    <form method="post" action="{{route('usermanagement.add')}}" class="add-form" id="user-form">
        {{csrf_field()}}
        <input type="hidden" value="{{$licensee_id}}" name="licensee_id" id="licensee_id">
        <div class="box-wrapper">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="label-control">First Name <span class="aestrik">*</span></label>
                        <input type="text" name="fname" id="fname" class="form-control" placeholder="First Name"> 
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="label-control">Last Name <span class="aestrik">*</span></label>
                        <input type="text" name="lname" id="lname" class="form-control" placeholder="Last Name"> 
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="label-control">Email Address <span class="aestrik">*</span></label>
                        <input type="text" name="username" id="username" class="form-control" placeholder="Email Address"> 
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="label-control">Contact Number <span class="aestrik">*</span></label>
                        <input type="text" name="contact_no" id="contact_no" class="form-control" placeholder="Contact Number"> 
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="label-control">Assign Domain/s <span class="aestrik">*</span></label>
                        <select class="form-control domains" name="domain_id[]" id="domain_id" multiple="multiple" placeholder="Select Domain">
                            @foreach($domains as $domain_key => $domain)
                                <option value="{{$domain['id']}}">{{$domain['name']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="label-control">Role <span class="aestrik">*</span></label>
                        <select class="form-control" name="type" id="type">
                            <option value="">Select Role</option>
                            <option value="licensee_administrator">Licensee Administrator</option>
                            <option value="brand_administrator">Brand Administrator</option>
                            <option value="product_manager">Product Manager</option>
                            <option value="agent">Agent</option>
                            <option value="consultant">Consultant</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="m-t-20 row">
                <div class="col-sm-offset-2 col-sm-8">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3">
                            <div class="row">
                                <button type="sumbit" name="" class="btn btn-primary btn-block">Add</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    @if(!empty($users->toArray()))
    <div class="box-wrapper">
        <div class="table-responsive m-t-20 table_record">
            <table class="table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Role</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>{{$user['name']}}</td>
                        <?php $role = explode('_', $user['type']); ?>
                        <td>{{ucfirst(implode(' ', $role))}}</td>
                        <td class="text-center">
                            <a href="{{ route('usermanagement.userupdate',[ 'user_id' => $user['id'], 'licensee_id' => $licensee_id ])}}" class="button success tiny btn-primary btn-sm">{{ trans('messages.update_btn') }}</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @endif
    <div class="m-t-20 row">
        <div class="col-sm-offset-2 col-sm-8">
            <div class="row">
                <div class="col-sm-6">
                    <a href="{{ route('onboarding.inventory',[$licensee_id]) }}" class="btn btn-primary btn-block">Previous</a>
                </div>
                <div class="col-sm-6">
                    <a href="{{route('onboarding.pricing',$licensee_id)}}" name="" class="btn btn-primary btn-block">Next</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript" src="{{ asset('assets/js/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/blockui.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/onboarding.js') }}"></script>
<script type="text/javascript">
    $(".domains").selectize();
      $("#user-form").validate({
        ignore: [],
        rules: {
            fname: {
                required: true
            },
            lname: {
                required: true
            },
            username: {
                required: true,
                email: true
            },
            contact_no: {
                required: true,
                number: true
            },
            type: {
                required: true,
            },
            'domain_id[]': {
                required: true,
            }
        },
        errorPlacement: function (label, element) {
            label.addClass('error_c');
            label.insertAfter($(element).parent('.form-group'));
        },
        submitHandler: function (form) {
            $('.add-form').sumbit();
        }
    });
</script>
@endpush