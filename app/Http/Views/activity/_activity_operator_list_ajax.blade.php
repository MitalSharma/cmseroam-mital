<table class="table">
    <thead>
        <tr>
            <th>
                <label class="radio-checkbox label_check" for="checkbox-00">
                    <input type="checkbox" id="checkbox-00" value="1" onchange="selectAllRow(this);">&nbsp;
                </label>
            </th>
            <th onclick="getActivitySort(this,'name');" width="20%">{{ trans('messages.name') }} 
                <i class="{{ ( $sOrderBy == 'asc' && $sOrderField == 'name' )? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getActivitySort(this,'marketing_contact_name');">{{ trans('messages.contact_name') }} 
                <i class="{{ ( $sOrderBy == 'asc' && $sOrderField == 'marketing_contact_name' )? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getActivitySort(this,'marketing_contact_phone');">{{ trans('messages.contact_number') }} 
                <i class="{{ ( $sOrderBy == 'asc' && $sOrderField == 'marketing_contact_phone' )? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getActivitySort(this,'marketing_contact_email');">{{ trans('messages.contact_email') }} 
                <i class="{{ ( $sOrderBy == 'asc' && $sOrderField == 'marketing_contact_email' )? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th class="text-center" width="25%">{{ trans('messages.thead_action') }}</th>
        </tr>
    </thead>
    <tbody class="city_list_ajax">
    @if(count($oActivityList) > 0)
        @include('WebView::activity._more_activity_operator_list')
    @else
        <tr><td colspan="10" class="text-center">{{ trans('messages.no_record_found') }}</td></tr>
    @endif
    </tbody>
</table>
<div class="clearfix">
    <div class="col-sm-5">
        <p class="showing-result">
            {{ trans('messages.show_out_of_record',['current' => $oActivityList->count() , 'total'=>$oActivityList->total() ]) }}
        </p>
    </div>
    <div class="col-sm-7 text-right">
      <ul class="pagination">
      </ul>
    </div>
</div>

<script type="text/javascript">
$(function() {
    $('.pagination').pagination({
        pages: {{ $oActivityList->lastPage() }},
        itemsOnPage: 10,
        currentPage: {{ $oActivityList->currentPage() }},
        displayedPages:2,
        edges:1,
        onPageClick(pageNumber, event){
            getPaginationListing(siteUrl('activity/activity-operator-list?page='+pageNumber),event,'table_record');
//            if(pageNumber > 1)
//                getMoreListing(siteUrl('activity/activity-operator-list?page='+pageNumber),event,'city_list_ajax');
//            else
//                getMoreListing(siteUrl('activity/activity-operator-list?page='+pageNumber),event,'tour_list_ajax');
            $('#checkbox-00').prop('checked',false);
            setupLabel();
        }
    });
});
</script>