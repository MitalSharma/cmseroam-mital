@extends( 'layout/mainlayout' )

@section('custom-css')
    <link href="{{ asset('assets/js/MultipleDatesPicker/jquery-ui-theme.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/js/MultipleDatesPicker/jQuery_UI_v1.12.1.css') }}" rel="stylesheet" />

    <style>
        .error{
            color:red !important;
        }
        .success_message{
                color:green !important;
                text-align: center;
        }
        .update-region-btn i {
                margin-left: .5rem;
                font-size: 1.2rem;
        }
        .switch {
                margin-bottom: 0;
        }
        .fa-thumbs-o-up{
                color: #0e95de;
        }
        .ui-datepicker-next, .ui-datepicker-prev{
            display:none;
        }
        
        .icon-edit:before {
            content: "\f044";
        }

        .icon-calendar:before {
            content: "\f073";
        }

        .ui-datepicker-today  .ui-state-highlight{
            background: #000;
            color: #fff;
        }

        .ui-datepicker-group-first{ padding-right:6px;}
        .ui-datepicker-group-middle { padding-right:3px; padding-left:3px;}
        .ui-datepicker-group-last{ padding-left:6px; width:33.4% !important;}
        .ui-datepicker-multi-3 { width:100% !important;}

        .has-js .label_check{
            padding-left: 24px;
        }

        .m_r_15 {
            margin-right: 5px;
        }
    </style>

    <script type="text/javascript">
        var disableDates = {}; 
    </script>
@stop


@section('content')
    <div class="content-container">
    	<h1 class="page-title">{{ trans('messages.tour_step_3')}}</h1>

        @if (Session::has('message'))
           <div class="small-12 small-centered columns success_message">{{ Session::get('message') }}</div>
           <br>
        @endif   
        
        @include('WebView::tour.manage_season_listing')
        
        <input type="hidden" name="tour_id" id="tour_id" value="{{ $oTour['tour_id'] }}" >
        {{Form::open(array('url' => 'tour/manage-dates/'. $oTour['tour_id'],'id'=>'manageDateForm','method'=>'Post')) }}
            <div class="box-wrapper m-t-30">
                <p>{{ trans('messages.currency_detail') }}</p>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="label-control">{{ trans('messages.currency') }}</label>
                            <?php 
                                $attributes = $errors->has('currency') ? 'with_error' : 'form-control';
                                $disabled  = '';
                                //if($oTour['tour_currency'] != ''){ $disabled  = 'disabled="disabled"'; }
                            ?>
                            {{Form::select('currency',$oCurrencies,$oTour['currency']['id'],['id'=>'currency','class'=>$attributes,$disabled])}}
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="label-control">Inventory</label>
                            {{Form::input('text','txtAvailability',Input::old('txtAvailability'),['id'=>'txtAvailability','class' =>$attributes, 'Placeholder'=>'Inventory'])}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-wrapper m-t-30">        
                <p>{{ trans('messages.season_detail') }}</p>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="label-control">{{ trans('messages.season_start_date') }}</label>
                            <?php $attributes ='form-control'; ?>
                            {{Form::input('text','txtStartDate',Input::old('txtStartDate'),['id'=>'txtStartDate','class' =>$attributes, 'Placeholder'=>'Start Date'])}}
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="label-control">{{ trans('messages.season_end_date') }}</label>
                            {{Form::input('text','txtEndDate',Input::old('txtEndDate'),['id'=>'txtEndDate','class' =>$attributes, 'Placeholder'=>'End Date'])}}                  
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="label-control">{{ trans('messages.season_name') }}</label>
                            <?php $attributes ='form-control'; ?>
                            {{Form::input('text','txtSeasonName',Input::old('txtSeasonName'),['id'=>'txtSeasonName','class' =>$attributes, 'Placeholder'=>'Season Name'])}}
                        </div>
                    </div>
                    <div class="col-sm-6"></div>
                </div>
            </div>
            <div class="box-wrapper m-t-30">     
                    <p>{{ trans('messages.season_price_detail') }}</p>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="label-control">{{ trans('messages.single_adult_price') }}</label>
                                {{Form::input('text','txtAdultPriceSingle',round(Input::old('txtAdultPriceSingle'),2),['id'=>'txtAdultPriceSingle','class' =>$attributes, 'Placeholder'=>'Adult Price (Single)'])}}
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="label-control">{{ trans('messages.twin_adult_price') }}</label>
                                {{Form::input('text','txtAdultPrice',round(Input::old('txtAdultPrice'),2),['id'=>'txtAdultPrice','class' =>$attributes, 'Placeholder'=>'Adult Price (Twin Share)'])}}
                            </div>
                        </div>
                        
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="label-control">{{ trans('messages.single_supp_adult_price') }}</label>
                                {{Form::input('text','txtAdultPriceSupplement',Input::old('txtAdultPriceSupplement'),['id'=>'txtAdultPriceSupplement','class' =>$attributes, 'Placeholder'=>'Adult Price (Single Supplement) - Automatically Calculated','readonly'])}}
                                <label for="txtAdultPriceSupplement" generated="true" class="error adultSupplementError"></label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                          
                        </div>
                    </div>

                    <input type="hidden" name="SeasonId" id="SeasonId" value="0">
                    <input type="submit" name="submitDate" value="submit" id="submitDate" style="display: none;" >                                   
            </div>
        {{Form::close()}}
            
        {{Form::open(array('url' => 'tour/manage-dates/'. $oTour['tour_id'],'id'=>'FlightPaymentForm','method'=>'Post')) }}
            <div class="box-wrapper m-t-30">
                <p>{{ trans('messages.flight_included') }}</p>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="label-control">{{ trans('messages.currency') }}</label>
                            <?php $attributes = $errors->has('currency_id') ? 'with_error' : 'form-control' ?>
                            {{Form::select('flight_currency_id',$oCurrencies,$oTour['currency']['id'],['id'=>'flight_currency_id','class'=>$attributes])}}
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="label-control">{{ trans('messages.flight_payment_desc') }}</label>
                            {{Form::input('text','flightDescription',Input::old('flightDescription'),['id'=>'flightDescription','class' =>$attributes, 'Placeholder'=>'Flight Payment Description'])}}
                        </div>
                    </div>

                </div> 
                <div class="row"> 
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="label-control">{{ trans('messages.select_city') }}</label>
                            
                            <?php 
                                $city[''] = 'Select City';
                                $city[30] = 'Melbourne';
                                $city[7] = 'Sydney';
                                $city[15] = 'Brisbane';
                            ?>
                            <?php $attributes = $errors->has('flight_depart_city') ? 'with_error' : 'form-control' ?>
                            {{Form::select('flight_depart_city',$city,$oTour['currency']['id'],['id'=>'flight_depart_city','class'=>$attributes])}}
                        </div>
                    </div>  
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="label-control">{{ trans('messages.flight_payment') }}</label>
                            {{Form::input('text','flightPrice',Input::old('flightPrice'),['id'=>'flightPrice','class' =>$attributes, 'Placeholder'=>'Additional Flight Payment','min'=> 1])}}
                        </div>
                    </div>
                </div>    
                <div class="row">      
                    <div class="col-sm-6 pull-right">
                        <div>
                            <input class="btn btn-primary btn-block" type="submit" name="btnAddPayment" value="Add Flight Payment" id="btnAddFlightPayment">
                        </div>
                    </div>
                </div>
                <input type="hidden" name="season_id" id="season_id" value="0" >

                <div id="UpdateProgressFlightPayment" align="center" style="display:none;" role="status" aria-hidden="true">
                    <img src="{{ url( 'assets/images/ajax_loading.gif' ) }}"> 
                </div>
                <div id="flightPayments"></div>
            </div>
        {{Form::close()}}    
        
        {{Form::open(array('url' => 'tour/manage-dates/'. $oTour['tour_id'],'id'=>'paymentForm','method'=>'Post')) }}
            <div class="box-wrapper m-t-30">
                <p>Local Payments</p>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="label-control">{{ trans('messages.currency') }}</label>
                            <?php $attributes = $errors->has('currency_id') ? 'with_error' : 'form-control' ?>
                            {{Form::select('currency_id',$oCurrencies,$oTour['currency']['id'],['id'=>'currency_id','class'=>$attributes])}}
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="label-control">{{ trans('messages.local_payment_price') }}</label>
                            {{Form::input('text','txtPrice',Input::old('txtPrice'),['id'=>'txtPrice','class' =>$attributes, 'Placeholder'=>'Local Payment Price' ,'min'=> 1])}}
                        </div>
                    </div>
                </div> 
                <div class="row">   
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="label-control">{{ trans('messages.local_payment_desc') }}</label>
                            {{Form::input('text','txtDescription',Input::old('txtDescription'),['id'=>'txtDescription','class' =>$attributes, 'Placeholder'=>'Local Payment Description'])}}
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div>
                            <input class="btn btn-primary btn-block" type="submit" name="btnAddPayment" value="Add Local Payment" id="btnAddPayment">
                        </div>
                    </div>
                </div>
                <input type="hidden" name="season_id" id="season_id" value="0" >

                <div id="UpdateProgressLocalPayment" align="center" style="display:none;" role="status" aria-hidden="true">
                    <img src="{{ url( 'assets/images/ajax_loading.gif' ) }}"> 
                </div>
                <div id="localPayments"></div>
            </div>
        {{Form::close()}}

        <div class="m-t-20 row">
            <div class="col-sm-4">
                <input class="btn btn-primary btn-block" type="button" name="btnSaveAll" value="Add Season" id="btnSaveAll">
            </div>
             <div class="col-sm-4">
                <button type="button" class="btn btn-primary btn-block clear">{{ trans('messages.clear_dates') }}</button>
            </div>
            <div class="col-sm-4">
                <input class="btn btn-primary btn-block" type="button" name="btnReturn" value="Return to Tour" id="btnReturn">
            </div>   
        </div>
    </div>

    <a href="#" data-toggle="modal" data-target="#updateDatesModal" title="Manage Dates" class="manage_dates" style="display:none;">Activity</a>
    <div class="modal fade bs-example-modal-lg" id="updateDatesModal" data-index="0" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close FormCloseButton" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">{{ trans('messages.tour_departure') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="box-wrapper" style="padding:10px;" id="calendarForm">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="label-control">{{ trans('messages.tour_departure') }}</label>
                                    <input id="txtFreeSell" class="form-control" placeholder="Freesell upto days priore" value="" name="txtFreeSell" type="number">                  
                                    <label for="txtFreeSell" generated="true" id="txtFreeSellError" class="error" style="display:none;">This field is required.</label>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="">
                            <div class="col-sm-6">
                                <label class="radio-checkbox label_check m_r_15" for="checkbox-mon">
                                    <input type="checkbox" class="week_check" name="tour" data-id="1" id="checkbox-mon" value="1">Mon
                                </label>

                                <label class="radio-checkbox label_check m_r_15" for="checkbox-tue">
                                    <input type="checkbox" class="week_check" name="tour" data-id="2" id="checkbox-tue" value="1">Tue
                                </label>

                                <label class="radio-checkbox label_check m_r_15" for="checkbox-wed">
                                    <input type="checkbox" class="week_check" name="tour" data-id="3" id="checkbox-wed" value="1">Wed
                                </label>

                                <label class="radio-checkbox label_check m_r_15" for="checkbox-thu">
                                    <input type="checkbox" class="week_check" name="tour" data-id="4" id="checkbox-thu" value="1">Thu
                                </label>

                                <label class="radio-checkbox label_check m_r_15" for="checkbox-fri">
                                    <input type="checkbox" class="week_check" name="tour" data-id="5" id="checkbox-fri" value="1">Fri
                                </label>

                                <label class="radio-checkbox label_check m_r_15" for="checkbox-sat">
                                    <input type="checkbox" class="week_check" name="tour" data-id="6" id="checkbox-sat" value="1">Sat
                                </label>

                                <label class="radio-checkbox label_check m_r_15" for="checkbox-sun">
                                    <input type="checkbox" class="week_check" name="tour" data-id="0" id="checkbox-sun" value="1">Sun
                                </label>
                            </div>

                            <div class="col-sm-6" style="font-size:10px;">
                                <div class="row">
                                    <div class="col-sm-7">
                                        <p>- Seleted dates show tour departure availability</p>
                                        <p>- Click on box to toggle selection</p>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="row">
                                            <div class="col-sm-6">  
                                                <div class="row">
                                                    <div class="col-sm-4" style="padding-top: 7%;">
                                                        <a  style="background:#27A9E6; color:#fff; border-radius:100px; padding:5px;">21</a>
                                                    </div>
                                                    <div class="col-sm-8">Departure Available</div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">   
                                                <div class="row">
                                                    <div class="col-sm-4" style="padding-top: 7%;">
                                                        <a  style="background:#ffffff; color:#212121; border-radius:100px; padding:5px;">21</a>
                                                    </div>
                                                    <div class="col-sm-8">Departure Not Available</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-t-30 multiFullCalDiv"></div>

                        <?php 
                            $no_of_days = 0;
                            if($oTour['durationType'] == 'd'){ $no_of_days = $oTour['no_of_days'];}
                            else {$no_of_days = 0; }  
                        ?>
                        <input type="hidden" name="tourDuration" id="tourDuration" value="{{ $no_of_days}}" >
                        <input type="hidden" name="seasonId" id="seasonId" value="" >
                        <input type="hidden" name="tourId" id="tourId" value="{{ $oTour['tour_id'] }}" >

                        <input type="hidden" name="AdultPrice" id="AdultPrice" value="" >
                        <input type="hidden" name="AdultPriceSingle" id="AdultPriceSingle" value="" >
                        <input type="hidden" name="AdultSupplement" id="AdultSupplement" value="" >

                        <input type="hidden" name="ChildPrice" id="ChildPrice" value="" >
                        <input type="hidden" name="ChildPriceSingle" id="ChildPriceSingle" value="" >
                        <input type="hidden" name="ChildSupplement" id="ChildSupplement" value="" >

                        <input type="hidden" name="Availability" id="Availability" value="" >
                    </div>  

                    <div id="messageForm" >
                        <div class="small-12 small-centered columns success_message"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div id="calendarFormButton">
                        <button type="button" class="btn btn-primary updateTourDates">Save</button>
                        <button type="button" class="btn btn-primary FormCloseButton">Cancel</button>
                    </div>

                    <div id="messageFormButton">
                        <button type="button" class="btn btn-primary FormCloseButton" >Close</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop

@section('custom-js')
<script type="text/javascript" src="{{ asset('assets/js/MultipleDatesPicker/jquery-ui.multidatespicker.js') }}" ></script>


<script>
	var site_url = "{{ url('/') }}/";
    var vars = {};
    <?php foreach ($seasonDates as $key => $sDates) { ?>
        var keyId = <?php echo  $key ;?>;
        vars['key_'+keyId] = [
            <?php foreach ($sDates as $key => $sDate) { ?>
              '<?php echo date('Y-m-d', strtotime($sDate->StartDate));?>',
            <?php } ?>
        ];
    <?php } ?>

	$(document).ready(function () {
        var cmp_date_multiple = [] ;
        
        $("#checkbox-dates").click(function () { 
            var cmp_date = [] ;
            if(this.checked) {
                // Iterate each checkbox
                $('.cmp_date_check').each(function() {
                    this.checked = true;
                    cmp_date.push($(this).val());
                    cmp_date_multiple.push($(this).val());
                });
                $('#dropdownMenu1').prop('disabled', false);
            }else{
                $('.cmp_date_check').each(function() {
                    this.checked = false;
                });
                cmp_date_multiple = [] ;
                $('#dropdownMenu1').prop('disabled', true);
            }
            if(cmp_date.length > 1){
                $("#edit_season_details,#edit_calender_dates").hide();
            }
        });

        $(".cmp_date_check").click(function () { 
            if(this.checked) {
                cmp_date_multiple.push($(this).val());
                if(cmp_date_multiple.length == 1){
                    $("#edit_season_details,#edit_calender_dates").show();
                }else if(cmp_date_multiple.length > 1){
                    $("#edit_season_details,#edit_calender_dates").hide();
                }
                $('#dropdownMenu1').prop('disabled', false);
                var flag = 0;
                $('.cmp_date_check').each(function(i) {
                    if(!this.checked) {
                       flag = 1;
                    }
                    j= i+1;
                    if(j == $('.cmp_date_check').length){
                        if(flag == 1){
                            $('#checkbox-dates').prop('checked', false);
                        }else{
                            $('#checkbox-dates').prop('checked', true);
                        }
                    }
                });
            }else{
                $('#checkbox-dates').prop('checked', false);
                cmp_date_multiple.pop();
                if(cmp_date_multiple.length == 1){
                    $("#edit_season_details,#edit_calender_dates,#delete_season").show();
                    $('#dropdownMenu1').prop('disabled', false);
                }else if(cmp_date_multiple.length < 1){
                    $('#dropdownMenu1').prop('disabled', true);
                }else{
                    $("#edit_season_details,#edit_calender_dates").hide();
                    $('#dropdownMenu1').prop('disabled', false);
                }    
            }
        });

        $('#delete_season').click(function () {
            var cmp_date = [] ;
            $('.cmp_date_check:checked').each(function() {
                cmp_date.push($(this).val());
            });
            if(cmp_date.length){
                if(confirm('Are you sure?')){
                    $.ajax({
                        type:'POST',
                        url:siteUrl('/tour/remove-season'),
                        data:{
                            tourId    : $("#tour_id").val(),
                            season_id : cmp_date
                        },
                        success:function (response) {
                            console.log(response);
                            if(response.trim() == 1){
                                location.reload();
                            }else{
                                alert('Please Try Again');
                            }
                        }   
                    });
                }
            }
        });

        $("#edit_season_details").click(function () {
            var cmp_date = [] ;
            var data_id ='';
            $('.cmp_date_check:checked').each(function() {
                cmp_date.push($(this).val());
                data_id = $(this).attr('data-id');

            });
            editSeason(data_id);
        });

        $('#edit_calender_dates').click(function () { 
            var id          = '';
            var month       = '';
            var advPur      = '';
            var cmp_date = [] ;
            $('.cmp_date_check:checked').each(function() {
                debugger;
                cmp_date.push($(this).val());
                id          = $(this).attr('data-id');
                month       = $(this).attr('data-month');
                advPur      = $(this).attr('data-advPur');
            });
            openMultiCalendar(id, month, advPur);  
        });  

        $('.dateRangeUpdate').click(function () { 
            var id          = $(this).attr('data-id');
            var month       = $(this).attr('data-month');
            var advPur      = $(this).attr('data-advPur');
            openMultiCalendar(id, month, advPur);  
        });

        $('.updateTourDates').click(function () {
            //alert();
            //return false;
            var dates = $('#mdp-demo').multiDatesPicker('getDates');
            var txtFreeSell = $("#txtFreeSell").val();
            var seasonId = $("#seasonId").val();
            var ChildPrice = $("#ChildPrice").val();
            var AdultPrice = $("#AdultPrice").val();
            var Availability = $("#Availability").val();
            var tourId = $("#tourId").val();

            var mon = 0;
            var tue = 0;
            var wed = 0;
            var thu = 0;
            var fri = 0;
            var sat = 0;
            var sund = 0;

            if($("#checkbox-mon").is(':checked')) { mon  = 1;} 
            if($("#checkbox-tue").is(':checked')) { tue  = 1;} 
            if($("#checkbox-wed").is(':checked')) { wed  = 1;} 
            if($("#checkbox-thu").is(':checked')) { thu  = 1;} 
            if($("#checkbox-fri").is(':checked')) { fri  = 1;} 
            if($("#checkbox-sat").is(':checked')) { sat  = 1;} 
            if($("#checkbox-sun").is(':checked')) { sund = 1;} 

            if(txtFreeSell != ''){
                $("#txtFreeSellError").hide();
                $.ajax({
                    type:'POST',
                    url:siteUrl('tour/update/dates/')+tourId,
                    data:{
                        AdultPrice       : $("#AdultPrice").val(), 
                        AdultPriceSingle : $("#AdultPriceSingle").val(), 
                        AdultSupplement  : $("#AdultSupplement").val(), 
                        ChildPrice       : $("#ChildPrice").val(), 
                        ChildPriceSingle : $("#ChildPriceSingle").val(), 
                        ChildSupplement  : $("#ChildSupplement").val(), 
                        Availability : $("#Availability").val(), 
                        adv_purchase : $("#txtFreeSell").val(), 
                        season_id    : $("#seasonId").val(),  
                        tourDuration : $("#tourDuration").val(),
                        mon          : mon, 
                        tue          : tue, 
                        wed          : wed, 
                        thu          : thu, 
                        fri          : fri, 
                        sat          : sat,  
                        sund         : sund,
                        dates        : dates,
                    },
                    success:function (response) { 
                        $(".success_message").html(response+' Dates Saved Successfully!');
                        $("#messageFormButton, #messageForm").show();
                        $("#calendarFormButton, #calendarForm").hide(); 
                    }
                });
            } else {
                $("#txtFreeSellError").show();
            }
        });

        $('.FormCloseButton').click(function () {
            location.reload();
        });

        $("#txtStartDate").datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            startDate: new Date(),
            minDate:new Date(),
            todayHighlight: true,
        }).on('changeDate', function (selected) {
            var minDate  = new Date(selected.date.valueOf());
            $('#txtEndDate').datepicker('setStartDate', minDate);
        });

        $("#txtEndDate").datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true
        }).on('changeDate', function (selected) {
            var maxDate  = new Date(selected.date.valueOf());
            $('#txtStartDate').datepicker('setEndDate', maxDate);
        });

        $(".dateRangeEdit").click(function () { 
            editSeason($(this).attr('data-id'));
        });

        $(".clear").click(function () { 
            $("#txtStartDate").val('').attr('disabled', false);
            $("#txtEndDate").val('').attr('disabled', false);
            $("#txtSeasonName").val('');
            $("#txtAdultPrice, #txtAdultPriceSingle, #txtAdultPriceSupplement").val('');
            //$("#txtChildPrice, #txtChildPriceSingle, #txtChildPriceSupplement").val('');
            $("#txtAvailability").val('');
            $("#btnSaveAll").val('Add Season');
            $("#localPayments").html('');
            $("#flightPayments").html('');
            $("#SeasonId").val('0');
            //$( "#txtEndDate" ).datepicker( "option", "startDate", 0);

            $("#txtStartDate, #txtEndDate").datepicker('remove');

            $("#txtStartDate").datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayHighlight: true,
            }).on('changeDate', function (selected) {
                var minDate  = new Date(selected.date.valueOf());
                $('#txtEndDate').datepicker('setStartDate', minDate);
            });

            $("#txtEndDate").datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayHighlight: true
            }).on('changeDate', function (selected) {
                var maxDate  = new Date(selected.date.valueOf());
                $('#txtStartDate').datepicker('setEndDate', maxDate);
            });
            //$( "#txtStartDate" ).datepicker( "option", "endDate", 0);
            //$('#txtEndDate').datepicker('update', '');
        });

        $("#btnSaveAll").click(function () { 
            $("#submitDate").click();
        });

        $("#btnReturn").click(function () { 
            window.location.href = siteUrl('tour/tour-list');
        });
        
        $(".dateRangeDelete").click(function () { 
            deleteSeason($(this).attr('data-id'));
        });

        $("#txtAdultPriceSingle, #txtAdultPrice").blur(function () { 
            var single  = parseInt($("#txtAdultPriceSingle").val());
            var twin    = parseInt($("#txtAdultPrice").val());
            CalculateSupplement(single, twin, 'adult');
        });

        $("#txtChildPriceSingle, #txtChildPrice").blur(function () { 
            var single  = parseInt($("#txtChildPriceSingle").val());
            var twin    = parseInt($("#txtChildPrice").val());
            CalculateSupplement(single, twin, 'child');
        });

        $.validator.addMethod('ge', function(value, element, param) {
            return this.optional(element) || parseInt(value) > parseInt($(param).val());
        }, 'Single price must be greater then Twin Share price.');
        
        $(".week_check").change(function () {
            var val = $(this).attr('data-id');
            if ($(this).is(":checked")) {
                var addDates = [];
                $(".eroamDaySelect"+val).not( ".ui-state-disabled" ).addClass("ui-state-highlight");
                $(".eroamDaySelect"+val).each(function( index ) {
                    if($(this).hasClass("ui-state-highlight")){
                        addDates.push($(this).attr('data-year') +'-'+ (parseInt($( this ).attr('data-month'))+1) +'-'+ $( this ).text() );
                    }
                });
                $('#mdp-demo').multiDatesPicker('addDates', addDates); 
            } else {
                var addDates = [];
                $(".eroamDaySelect"+val).each(function( index ) {
                    if($(this).hasClass("ui-state-highlight")){
                        addDates.push($(this).attr('data-year') +'-'+ (parseInt($( this ).attr('data-month'))+1) +'-'+ $( this ).text() );
                    }
                });
                $('#mdp-demo').multiDatesPicker('removeDates', addDates); 
                $(".eroamDaySelect"+val).removeClass("ui-state-highlight");
            }
        });
    });

    function CalculateSupplement(single, twin, type){
        //if(single > 0 && twin > 0 && single > twin){
            var diff = single - twin;
            if(type == 'adult'){
                $("#txtAdultPriceSupplement").val(diff);
                $(".adultSupplementError").html('');
            } else {
                //$("#txtChildPriceSupplement").val(diff);
                //$(".childSupplementError").html(''); 
            }
        /*} else {

            if(type == 'adult'){
                if(single > 0 && twin > 0 && twin > single){
                    $(".adultSupplementError").html('Single price must be greater then Twin Share price.');
                } else { $(".adultSupplementError").html(''); }
            } else {
                if(single > 0 && twin > 0 && twin > single){
                    $(".childSupplementError").html('Single price must be greater then Twin Share price.');
                } else { $(".childSupplementError").html(''); }
            }
        } */
    }

    function editSeason(id){
        var startDate       = $("#startDate_"+id).text();
        var endDate         = $("#endDate_"+id).text();
        var seasonname      = $("#seasonname_"+id).text();
        var adultPrice      = $("#adultPrice_"+id).val();
        var adultPriceSingle      = $("#adultPriceSingle_"+id).val();
        var adultSupplement      = $("#adultSupplement_"+id).val();
        var childPrice      = $("#childPrice_"+id).val();
        var childPriceSingle      = $("#childPriceSingle_"+id).val();
        var childSupplement      = $("#childSupplement_"+id).val();
        var availability    = parseInt($("#availability_"+id).text());
        var season_id       = $("#season_id_"+id).val();
        var tour_id         = $("#tour_id").val();
        
       

        $("#txtStartDate").val(startDate).attr('disabled', true);
        $("#txtEndDate").val(endDate).attr('disabled', true);
        $("#txtSeasonName").val(seasonname);
        $("#txtAdultPrice").val(parseFloat(adultPrice).toFixed(2));
        $("#txtAdultPriceSingle").val(parseFloat(adultPriceSingle).toFixed(2));
        $("#txtAdultPriceSupplement").val(parseFloat(adultSupplement).toFixed(2));
        $("#txtChildPrice").val(childPrice);
        $("#txtChildPriceSingle").val(childPriceSingle);
        $("#txtChildPriceSupplement").val(childSupplement);
        $("#txtAvailability").val(availability);
        
        

        $("#btnSaveAll").val('Update');
        $("#SeasonId").val(season_id);
        $("#season_id").val(season_id);

        $("#UpdateProgressLocalPayment").show();
        $.ajax({
            type:'POST',
            url:siteUrl('tour/manage-payment'),
            data:{
                tourId :$("#tour_id").val(), 
                season_id:$("#season_id").val(),
                action : 'View'
            },
            success:function (response) { 
                $("#txtPrice").val('');
                $("#currency_id").val("{{$oTour['currency']['id']}}");
                $("#txtDescription").val('');
                $("#localPayments").html(response);

                $(".localPaymentDelete").click(function(){
                    deletePayment($(this).attr('data-id'));
                });
                $("#UpdateProgressLocalPayment").hide();
                return false;
            },
            error: function( xhr ) {
                $("#UpdateProgressLocalPayment").hide();
            }
        });

        $("#UpdateProgressFlightPayment").show();
        $.ajax({
            type:'POST',
            url:siteUrl('tour/manage-flight-payment'),
            data:{
                tourId :$("#tour_id").val(), 
                season_id:$("#season_id").val(),
                action : 'View'
            },
            success:function (response) { 
                $("#flightPrice").val('');
                $("#flight_currency_id").val("{{$oTour['currency']['id']}}");
                $("#flight_depart_city").val('');
                $("#flightDescription").val('');
                $("#flightPayments").html(response);

                $("#flight_depart_city option").each(function(){
                    if($(this).val()){
                        $("#flight_depart_city option[value='"+$(this).val()+"']").prop('disabled',false); 
                    }
                });
                $(".localFightPaymentDelete").each(function(){
                    $("#flight_depart_city option[value='"+$(this).attr('data-currency')+"']").prop('disabled',true); 
                });
                $(".localFightPaymentDelete").click(function(){
                    deleteFlightPayment($(this).attr('data-id'),$(this).attr('data-currency'));
                });
                $("#UpdateProgressFlightPayment").hide();
                return false;
            },
            error: function( xhr ) {
                $("#UpdateProgressFlightPayment").hide();
            }
        });
    }

    function deletePayment(id) {
        $("#UpdateProgressLocalPayment").show();
        //var id = $(this).attr('data-id');
        $.ajax({
            type:'POST',
            url:siteUrl('tour/manage-payment'),
            data:{
                tourId : $("#tour_id").val(), 
                season_id:$("#season_id").val(),
                Payment_Id : id,
                action : 'Delete'
            },
            success:function (response) { 
                $("#localPayments").html(response);
                $(".localPaymentDelete").click(function(){
                    deletePayment($(this).attr('data-id'));
                });
                $("#UpdateProgressLocalPayment").hide();
            },
            error: function( xhr ) {
                $("#UpdateProgressLocalPayment").hide(); 
            }
        });
    };
    function deleteFlightPayment(id,currency) {
        $("#UpdateProgressFlightPayment").show();
        $("#flight_depart_city option[value='"+currency+"']").prop('disabled',false); 
        //var id = $(this).attr('data-id');
        $.ajax({
            type:'POST',
            url:siteUrl('tour/manage-flight-payment'),
            data:{
                tourId : $("#tour_id").val(), 
                season_id:$("#season_id").val(),
                flight_id : id,
                action : 'Delete'
            },
            success:function (response) { 
                $("#flightPayments").html(response);
                $(".localFightPaymentDelete").click(function(){
                    deleteFlightPayment($(this).attr('data-id'),$(this).attr('data-currency'));
                });
                $("#UpdateProgressFlightPayment").hide();
            },
            error: function( xhr ) {
                $("#UpdateProgressFlightPayment").hide(); 
            }
        });
    };

    function deleteSeason(id){
        $.ajax({
            type:'POST',
            url:siteUrl('tour/manage-season'),
            data:{
                tourId    : $("#tour_id").val(),
                season_id : id,
                action    : 'Delete'
            },
            success:function (response) { 
                $(".clear").click();
                $("#txtPrice").val('');
                $("#season_id").val(0);
                $("#currency_id").val("{{$oTour['currency']['id']}}");
                $("#txtDescription").val('');
                $("#localPayments").html('');
                $("#seasonDateList").html(response);

                $(".dateRangeEdit").click(function(){
                    editSeason($(this).attr('data-id'));
                });

                $(".dateRangeDelete").click(function(){
                    deleteSeason($(this).attr('data-id'));
                });

                $('.dateRangeUpdate').click(function () { 
                    var id          = $(this).attr('data-id');
                    var month       = $(this).attr('data-month');
                    //var day         = $(this).attr('data-day');
                    var advPur      = $(this).attr('data-advPur');
                    openMultiCalendar(id, month, advPur);  
                });
            }   
        });
    };
    
    $("#paymentForm").validate({
        ignore: [],
        rules: {
            currency_id: {
                required: true
            },
            txtPrice: {
                required: true,
                number: true
            },
            txtDescription: {
                required: true, 
            } 
        },
        errorPlacement: function (label, element) {
            label.insertAfter(element);
        },
        submitHandler: function (form) {
            $("#UpdateProgressLocalPayment").show();
            $.ajax({
                type:'POST',
                url:siteUrl('tour/manage-payment'),
                data:{
                    tourId :$("#tour_id").val(), 
                    price :$("#txtPrice").val(), 
                    currency :$("#currency_id").val(), 
                    description:$("#txtDescription").val(), 
                    season_id:$("#season_id").val(),
                    action : 'Add'
                },
                success:function (response) { 
                    $("#txtPrice").val('');
                    $("#season_id").val(0);
                    $("#currency_id").val("{{$oTour['currency']['id']}}");
                    $("#txtDescription").val('');
                    $("#localPayments").html(response);

                    $(".localPaymentDelete").click(function(){
                        deletePayment($(this).attr('data-id'));
                    });
                    $("#UpdateProgressLocalPayment").hide();
                    return false;
                },
                error: function( xhr ) {
                    $("#UpdateProgressLocalPayment").hide();
                }
            });
        }
    }); 
    $("#FlightPaymentForm").validate({
        ignore: [],
        rules: {
            flightPrice: {
                number: true,
                required: true
            },
            flight_depart_city: {
                required: true
               
            }
        },
        errorPlacement: function (label, element) {
            label.insertAfter(element);
        },
        submitHandler: function (form) {
            $("#UpdateProgressFlightPayment").show();
            $.ajax({
                type:'POST',
                url:siteUrl('tour/manage-flight-payment'),
                data:{
                    tourId :$("#tour_id").val(), 
                    flightPrice :$("#flightPrice").val(), 
                    flight_currency_id :$("#flight_currency_id").val(), 
                    flightDescription:$("#flightDescription").val(), 
                    season_id:$("#season_id").val(),
                    flight_depart_city:$("#flight_depart_city option:selected").val(),
                    action : 'Add'
                },
                success:function (response) {
                    $("#flight_depart_city option[value='"+$("#flight_depart_city option:selected").val()+"']").prop('disabled',true); 
                    $("#flight_depart_city").val('');
                    $("#flightPrice").val('');
                    $("#flight_currency_id").val("{{$oTour['currency']['id']}}");
                    $("#flightDescription").val('');
                    if(!$("#season_id").val()){
                        $("#season_id").val(0);
                    }
                    
                    $("#flightPayments").html(response);

                    $(".localFightPaymentDelete").click(function(){
                        deleteFlightPayment($(this).attr('data-id'),$(this).attr('data-currency'));
                    });
                    $("#UpdateProgressFlightPayment").hide();
                    return false;
                },
                error: function( xhr ) {
                    $("#UpdateProgressFlightPayment").hide();
                }
            });
        }
    }); 
    
    $("#manageDateForm").validate({
        ignore: [],
        rules: {
            currency: {
                required: true
            },
            txtStartDate: {
                required: true
            },
            txtEndDate: {
                required: true
            },
            txtSeasonName: {
                required: true
            },
            txtAdultPrice: {
                required: true,
                number: true,
            },
            txtAvailability: {
                required: true,
                digits: true
            },
            txtAdultPriceSingle: {
                required: true,
                number: true,
                //ge: '#txtAdultPrice'
            }
        },
        errorPlacement: function (label, element) {
            label.insertAfter(element);
        },
        submitHandler: function (form) {
            var PaymentId = [];            
            $('input[name^=PaymentId]').each(function(){
                PaymentId.push($(this).val());
            });
            var flight_Id = [];            
            $('input[name^=flight_id]').each(function(){
                flight_Id.push($(this).val());
            });
            $.ajax({
                type:'POST',
                url:siteUrl('tour/manage-season'),
                data:{
                    tourId               :$("#tour_id").val(), 
                    StartDate            :$("#txtStartDate").val(), 
                    EndDate              :$("#txtEndDate").val(), 
                    SeasonName           :$("#txtSeasonName").val(), 
                    AdultPrice           :$("#txtAdultPrice").val(), 
                    AdultPriceSingle     :$("#txtAdultPriceSingle").val(), 
                    AdultPriceSupplement :$("#txtAdultPriceSupplement").val(), 
                    //ChildPrice           :$("#txtChildPrice").val(), 
                    //ChildPriceSingle     :$("#txtChildPriceSingle").val(), 
                    //ChildPriceSupplement :$("#txtChildPriceSupplement").val(), 
                    Availability         :$("#txtAvailability").val(), 
                    season_id            :$("#SeasonId").val(),
                    currency             :$("#currency option:selected" ).text(),
                    PaymentId            :PaymentId,
                    flight_id            :flight_Id,
                    action               :'Add'
                },
                success:function (response) {
                    location.reload();                   
                } 
            });
        }
    });   

    function openMultiCalendar(id, month, advPur){
        //alert(1);
        var startDate           = $("#startDate_"+id).text();
        //var endDate           = $("#endDate_"+id).text();
        var sDateSDAll          = $("#sDateSDAll_"+id).val(); 
        var season_id           = $("#season_id_"+id).val();
        var adultPrice          = $("#adultPrice_"+id).val();
        var adultPriceSingle    = $("#adultPriceSingle_"+id).val(); 
        var adultPriceSupplement= $("#adultSupplement_"+id).val();
        var childPrice          = $("#childPrice_"+id).val();
        var ChildPriceSingle    = $("#childPriceSingle_"+id).val();
        var ChildPriceSupplement= $("#childSupplement_"+id).val();
        var availability        = parseInt($("#availability_"+id).text());

        $("#txtFreeSell").val(advPur);
        $("#seasonId").val(season_id);
        $("#AdultPrice").val(adultPrice);
        $("#AdultPriceSingle").val(adultPriceSingle);
        $("#AdultSupplement").val(adultPriceSupplement);
        $("#ChildPrice").val(childPrice);
        $("#ChildPriceSingle").val(ChildPriceSingle);
        $("#ChildSupplement").val(ChildPriceSupplement);
        $("#Availability").val(availability);

        var mon  = $("#is_week_"+id).attr('data-mon');
        var tue  = $("#is_week_"+id).attr('data-tue');
        var wed  = $("#is_week_"+id).attr('data-wed');
        var thu  = $("#is_week_"+id).attr('data-thu');
        var fri  = $("#is_week_"+id).attr('data-fri');
        var sat  = $("#is_week_"+id).attr('data-sat');
        var sund = $("#is_week_"+id).attr('data-sun');

        if(mon == 1) { $("#checkbox-mon").attr('checked', true); $("#checkbox-mon").parent().addClass('c_on'); } else { $("#checkbox-mon").attr('checked', false); $("#checkbox-mon").parent().removeClass('c_on'); }
        if(tue == 1) { $("#checkbox-tue").attr('checked', true); $("#checkbox-tue").parent().addClass('c_on'); } else { $("#checkbox-tue").attr('checked', false); $("#checkbox-tue").parent().removeClass('c_on');  }
        if(wed == 1) { $("#checkbox-wed").attr('checked', true); $("#checkbox-wed").parent().addClass('c_on'); } else { $("#checkbox-wed").attr('checked', false); $("#checkbox-wed").parent().removeClass('c_on');  }
        if(thu == 1) { $("#checkbox-thu").attr('checked', true); $("#checkbox-thu").parent().addClass('c_on'); } else { $("#checkbox-thu").attr('checked', false); $("#checkbox-thu").parent().removeClass('c_on');  }
        if(fri == 1) { $("#checkbox-fri").attr('checked', true); $("#checkbox-fri").parent().addClass('c_on'); } else { $("#checkbox-fri").attr('checked', false); $("#checkbox-fri").parent().removeClass('c_on');  }
        if(sat == 1) { $("#checkbox-sat").attr('checked', true); $("#checkbox-sat").parent().addClass('c_on'); } else { $("#checkbox-sat").attr('checked', false); $("#checkbox-sat").parent().removeClass('c_on');  }
        if(sund == 1) { $("#checkbox-sun").attr('checked', true); $("#checkbox-sun").parent().addClass('c_on'); } else { $("#checkbox-sun").attr('checked', false); $("#checkbox-sun").parent().removeClass('c_on');  }


        var s = document.createElement("script");
        s.type = "text/javascript";
        s.src = site_url+"/assets/js/MultipleDatesPicker/jquery-ui.js";
        // Use any selector
        $(".multiFullCalDiv").html(s);
        $(".multiFullCalDiv").append('<div id="mdp-demo" ></div>');

        var numberOfMonths = [0,0];
        if(month <= 3) {  
            if(month == 1){ numberOfMonths = [1,1]; }
            else if(month == 2){ numberOfMonths = [1,2]; }
            else { numberOfMonths = [1,3]; } 
        } else { 
            var monthCount = parseInt(month/3);
            var mod = month % 3;
            if(mod > 0){ monthCount = monthCount+1;  }
            numberOfMonths = [monthCount,3]; 
        }

        $('#mdp-demo').multiDatesPicker('destroy');

        if(sDateSDAll > 0){
            if(disableDates['key_'+season_id].length > 0){
                $('#mdp-demo').multiDatesPicker({
                    dateFormat  : "yy-m-d",
                    defaultDate : startDate,
                    addDisabledDates: disableDates['key_'+season_id],
                    //addDates: vars['key_'+season_id],
                    //minDate: 0, // today
                    //maxDate: day, // +30 days from today
                    numberOfMonths: numberOfMonths
                });
            } else {
                $('#mdp-demo').multiDatesPicker({
                    dateFormat  : "yy-m-d",
                    defaultDate : startDate,
                    numberOfMonths: numberOfMonths
                });
            }
            $('#mdp-demo').multiDatesPicker('resetDates');
            $('#mdp-demo').multiDatesPicker('addDates', vars['key_'+season_id]); 

        } else {
            if(disableDates['key_'+season_id].length > 0){
                $('#mdp-demo').multiDatesPicker({
                    dateFormat  : "yy-m-d",
                    defaultDate : startDate,
                    numberOfMonths: numberOfMonths,
                    addDisabledDates: disableDates['key_'+season_id]
                });
            } else {
               $('#mdp-demo').multiDatesPicker({
                    dateFormat  : "yy-m-d",
                    defaultDate : startDate,
                    numberOfMonths: numberOfMonths    
                }); 
            }
            $('#mdp-demo').multiDatesPicker('resetDates');
        }
        
        $("#messageFormButton, #messageForm").hide();
        $("#calendarFormButton, #calendarForm").show();
        $('.manage_dates').click(); 
    }
</script>
@stop