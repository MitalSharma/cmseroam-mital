<table class="table">
    <thead>
        <tr>
            <th>
                <label class="radio-checkbox label_check" for="checkbox-00" onclick = "selectAllRow(this);">
                    <input type="checkbox" id="checkbox-00" value="1" >&nbsp;
                </label>
            </th>
            <th onclick="getTourSort(this,'tour_code');">{{ trans('messages.provider_code') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'tour_code')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getTourSort(this,'t.tour_title');">{{ trans('messages.title') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 't.tour_title')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getTourSort(this,'t.price');"> {{ trans('messages.lowest_price') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 't.price')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th onclick="getTourSort(this,'provider_name');"> {{ trans('messages.provider') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'provider_name')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th>{{ trans('messages.flag') }}</th>
            <th onclick="getTourSort(this,'t.is_active');"> {{ trans('messages.active_status') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 't.is_active')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th onclick="getTourSort(this,'t.is_reviewed');"> {{ trans('messages.review_status') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 't.is_reviewed')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th class="text-center">Active</th>
        </tr>
    </thead>
    <tbody class="tour_list_ajax">
    @if(count($oTourList) > 0)
        @include('WebView::tour._more_tour_list')
    @else
        <tr><td colspan="10" class="text-center">{{ trans('messages.no_record_found') }}</td></tr>
    @endif
    </tbody>
</table>
<div class="clearfix">
    <div class="col-sm-5"><p class="showing-result">{{ trans('messages.show_out_of_record',['current' => $oTourList->count() , 'total'=>$oTourList->total() ]) }}</p></div>
    <div class="col-sm-7 text-right">
      <ul class="pagination">
        
      </ul>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.pagination').pagination({
            pages: {{ $oTourList->lastPage() }},
            itemsOnPage: 10,
            currentPage: {{ $oTourList->currentPage() }},
            displayedPages:2,
            edges:1,
            onPageClick(pageNumber, event){
                getPaginationListing(siteUrl('tour/tour-list?page='+pageNumber),event,'table_record');
                /*if(pageNumber > 1)
                    callTourListing(event,'tour_list_ajax',pageNumber);
                else
                    callTourListing(event,'table_record',pageNumber);*/
                $('#checkbox-00').prop('checked',false);
                setupLabel();
            }
        });
    });

    var cmp_tour = [] ; 
    $(document).on('click',".cmp_tour_check",function () {
        if(this.checked) {
            if(jQuery.inArray($(this).val(), cmp_tour) == -1)
                cmp_tour.push($(this).val());
            $('#dropdownMenu1').prop('disabled', false);
        }else{
            var removeItem = $(this).val();
            cmp_tour = $.grep(cmp_tour, function(value) {
                            return value != removeItem;
                          });
        }
        
        if(cmp_tour.length > 1){
            $("#manage_views, #manage_dates").hide();
        }
        else
            $("#manage_views, #manage_dates").show();
        
        if(cmp_tour.length == 0){
            $('#dropdownMenu1').prop('disabled', true);
        }
});

$(document).on('click','.cmp_tour_check',function(){
    if($('.cmp_tour_check:checked').length == $('.cmp_tour_check').length){
        $('#checkbox-00').prop('checked',true);
    }else{
        $('#checkbox-00').prop('checked',false);
    }
});

$(document).on('click','.label_check',function(){
    setupLabel();
});
</script>