@extends( 'layout/mainlayout')
@section('custom-css')
<style>

    .error{
        color:red !important;
    }

    div .with_error{
        border:1px solid black;
    }
    .test{
        text-decoration: none;
    }
    .error_message{
        background: #f2dede;
        border: solid 1px #ebccd1;
        color: #a94442;
        padding: 11px;
        text-align: center;
        cursor: pointer;
    }
    .with_error{
        border-color: red !important;
    }
    .success_message{
        color:green !important;
        text-align: center;
    }
    .fa-plus-square{
        color:green;
        cursor:pointer;
    }
    .fa-minus-square{
        color:red;
        cursor:pointer;
    }

    .nav>li>a:focus, .nav>li>a:hover {
        background-color: #eee;
    }

    .address{
        margin-left: 45px;
        font-family: sans-serif;
        font-size: 14px;
    }
</style>
@stop
@section('content')

<div class="content-container" >
    <h1 class="page-title">PAX Details</h1> 

    <div class="container col-sm-12">
        <ul class="nav nav-pills nav-tabs">
            <li><a href="{{ route('booking.tour-pax-detail',['nItenaryId'=>$request_id])}}">PAX Details</a></li>
            <li><a href="{{ route('booking.booking-product-detail',['nItenaryId'=>$request_id])}}">Product Details</a></li>
            <li  class="active"><a href="{{ route('booking.tour-supplier-detail',['nItenaryId'=>$request_id])}}">Supplier Details</a></li>
        </ul>
    </div>
    </br>
    </br> 
    </br>
	</br>
	
    <div class="box-wrapper">
         <p class="h4">Supplier Details</p>
        <hr>
        <div class="box-wrapper">
    <div class="panel-body">
        <table class="table table-responsive">
            <tbody>
                @if($getRquestDetails != '')
                <tr>
                    <td>Supplier Name:  {{!empty($getRquestDetails['get_providers']['provider_name'])? $getRquestDetails['get_providers']['provider_name']:'N/A'}} </td>
					<td>Supplier Contact Person:  {{!empty($getRquestDetails['get_providers']['sp_name'])? $getRquestDetails['get_providers']['sp_name']:'N/A'}}</td>
                </tr>
				<tr>
                    <td>Supplier Phone: {{!empty($getRquestDetails['get_providers']['sp_phone'])? $getRquestDetails['get_providers']['sp_phone']:'N/A'}}</td>
					<td>Supplier Email Address : {{!empty($getRquestDetails['get_providers']['sp_email'])? $getRquestDetails['get_providers']['sp_email']:'N/A'}}</td>
				</tr>
                <tr>
                    <td>Supplier Payment Terms : N/A</td>
					<td>Supplier Additional Notes : {{!empty($getRquestDetails['get_providers']['special_notes'])? $getRquestDetails['get_providers']['special_notes']:'N/A'}}</td>
                </tr>
                 @else
                <tr>
                    <td>No Supplier Available</td>
                </tr>
                @endif
                
            </tbody></table>
    </div>
    </div>
    </div>
    <div class="col-sm-offset-2 col-sm-8">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="row">
                    <a href="{{ route('booking.tour-product-detail',['nItenaryId'=>$request_id])}}" class="btn btn-primary btn-block">Previous</a>
                </div>
            </div>
        </div>
    </div>
    @stop