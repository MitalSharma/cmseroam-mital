<body style="margin: 0px; background: #e3e5e7;">
    <table width="100%" style="background: #e3e5e7">
        <tr>
            <td>
                <table style="margin:0 auto; background: #ffffff;" width="760px" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <th style="background-color: #212121; padding-top:20px; padding-bottom: 20px; text-align: center;">
                               <a href="#"><img src="<?php echo $logo;?>" width="180px" alt="" /> </a> 
							</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="background: #fafafa; border-bottom: 1px solid #f0f0f0; padding-top: 5px; padding-bottom: 5px;">
                                <table width="100%">
                                    <?php /*<tr>
                                        <td style="text-align: center">
                                            <img src="http://dev.eroam.com/images/user_icon.png" alt="" />
                                        </td>
                                    </tr>*/ ?>
                                    <tr>
                                        <td style="text-align: center">
                                            <p style="font-family: Arial; font-size: 18px; color: #212121; margin-top: 0px; margin-bottom: 0px;  "><strong> Hi,<?php 
                                            if(isset($name)){ echo $name; } else { echo 'User'; } ?>!</strong></p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="90%;" style="margin: 0 auto;">
                                    <tbody>
                                        <tr><td style="font-family: Arial; font-size: 14px; padding-top: 20px;">We received a request to reset your password. If you did not make the request, just ignore this email.</td></tr>
										<tr><td style="font-family: Arial; font-size: 14px; padding-top: 20px;">Otherwise, you can reset your password by clicking the button below.</td></tr>
                                        <tr><td style="padding-top: 20px;"><p style="text-align: center;"> <a href="{{ $link }}" style="display: inline-block;background: #2AA9DF;border: 2px solid rgba(0, 84, 150, 0.49);color: #f5f5f5;padding: 15px 45px;text-decoration: none;font-weight: bold;"> Reset Password </a></p></td></tr>
										<tr><td style="font-family: Arial; font-size: 14px;padding-bottom: 30px;">This request will expire within 24 hours.</td></tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td style="background-color: #394951; padding-top:6px; padding-bottom: 6px; padding-right: 6px; padding-left: 6px; text-align: center;">
                                <table style="width: 100%;">
                                    <tr>
                                        <td style="font-family: Arial; font-size: 14px;">
											<a href="#"><img src="<?php echo $logo;?>" alt="" /> </a>
										</td>
                                        <td style="color: #fff;font-size: 12px; text-align: right; font-family: Arial; ">Powered by eRoam &copy; Copyright 2018 - 2019. All Rights Reserved. Patent pending AU2016902466</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </td>
        </tr>
    </table>
</body>