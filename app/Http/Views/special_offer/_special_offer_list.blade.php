<table class="table">
    <thead>
        <tr>
            <th>
                <label class="radio-checkbox label_check" for="checkbox-00">
                    <input type="checkbox" id="checkbox-00" value="1" onchange="selectAllRow(this);">&nbsp;
                </label>
            </th>
			
            <th onclick="getSpecialOfferSort(event, this,'offer_name');"> Offer Name <i class="{{ ( $sOrderBy == 'asc' && $sOrderField == 'offer_name' )? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i></th>
            <th onclick="getSpecialOfferSort(event, this,'offer_type');"> Offer Type <i class="{{ ( $sOrderBy == 'asc' && $sOrderField == 'offer_type' )? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i></th>
            <th onclick="getSpecialOfferSort(event, this,'inventory_type');"> Inventory Type <i class="{{ ( $sOrderBy == 'asc' && $sOrderField == 'inventory_type' )? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i></th>
            <th onclick="getSpecialOfferSort(event, this,'client');"> Licensee/Client <i class="{{ ( $sOrderBy == 'asc' && $sOrderField == 'client' )? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i></th>
            <th> Offer Period </th>
            <th> Travel Period </th>
            <th  onclick="getSpecialOfferSort(event, this,'status');" class="text-center">{{ trans('messages.status') }} <i class="{{ ( $sOrderBy == 'asc' && $sOrderField == 'status' )? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i></th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody class="offer_list_ajax">
    @if(count($specialOffer) > 0)
        @include('WebView::special_offer._more_offer_list')
    @else
        <tr><td colspan="10" class="text-center">{{ trans('messages.no_record_found') }}</td></tr>
    @endif
    </tbody>
</table>
<div class="clearfix">
    <div class="col-sm-5"><p class="showing-result">{{ trans('messages.show_out_of_record',['current' => $specialOffer->count() , 'total'=> $specialOffer->total() ]) }}</p></div>
    <div class="col-sm-7 text-right">
      <ul class="pagination">
        
      </ul>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.pagination').pagination({
            pages: {{ $specialOffer->lastPage() }},
            itemsOnPage: 10,
            currentPage: {{ $specialOffer->currentPage() }},
            displayedPages:2,
            edges:1,
            onPageClick(pageNumber, event){
                if(pageNumber > 1)
                    callSpecialOfferListing(event,'table_record',pageNumber);
                else
                    callSpecialOfferListing(event,'table_record',pageNumber);
                $('#checkbox-00').prop('checked',false);
                setupLabel();
            }
        });
    });

</script>