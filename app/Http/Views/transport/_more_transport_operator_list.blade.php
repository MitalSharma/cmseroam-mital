@foreach ($oTransportList as $aTransport)				
    <tr>
        <td>
            <label class="radio-checkbox label_check" for="checkbox-<?php echo $aTransport->id;?>">
                <input type="checkbox" class="cmp_check" id="checkbox-<?php echo $aTransport->id;?>" value="<?php echo $aTransport->id;?>">&nbsp;
            </label>
        </td>
        <td>
            <a href="{{ route('transport.transport-operator-create',[ 'nIdTransport' => $aTransport->id ])}}">
                {{ $aTransport->name }}
            </a>    
        </td>
        <td>{{ $aTransport->marketing_contact_name }}</td>
        <td>{{ $aTransport->marketing_contact_phone }}</td>
        <td>{{ $aTransport->marketing_contact_email }}</td>
        
        <td class="text-center">
            <div class="switch tiny switch_cls">
                <a href="{{ route('transport.transport-operator-create',[ 'id' => $aTransport->id ])}}" class="button success tiny btn-primary btn-sm">{{ trans('messages.update_btn') }}</a>
                <input type="button" class="button btn-delete tiny btn-primary btn-sm" value="{{ trans('messages.delete_btn') }}" onclick="callDeleteRecord(this,'{{ route('transport.transport-operator-delete',['id'=> $aTransport->id]) }}','{{ trans('messages.delete_label')}}')">
            </div>
        </td>
    </tr> 
@endforeach