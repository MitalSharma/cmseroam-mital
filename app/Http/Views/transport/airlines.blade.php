@extends('layout/mainlayout')
@section('content')
<style type="text/css">
	.modal{
background-color:rgba(0,0,0,0.6);
	}
</style>
<div class="content-container">
	<h1 class="page-title">Manage Transport</h1>
	<div class="box-wrapper">
		<div class="row">
			<div class="col-sm-6">
				<button type="button" id="add-airline-btn" class="button tiny button success tiny btn-primary btn-md" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i>Add new airline</button>
			</div>

			<div class="col-sm-6">
				<form id="search-form">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<input type="search" name="q" placeholder="Enter keywords" value="{{ Input::get('q') }}" class="form-control m-t-10">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<select name="by" class="form-control m-t-10">
								<option value="airline_name" {{ Input::get('by') == 'airline_name' ? 'selected' : '' }}>Name</option>
								<option value="airline_code" {{ Input::get('by') == 'airline_code' ? 'selected' : '' }}>Code</option>
								<option value="country_name" {{ Input::get('by') == 'country_name' ? 'selected' : '' }}>Country</option>
								</select>
							</div>
						</div>
						<div class="col-sm-3">
							<button class="button success tiny btn-primary btn-md"><i class="fa fa-search"></i> Search</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		@if (Session::has('success'))
		<div class="small-12 column">
		<h5 style="color: green">Airline Successfully Added!</h5>
		</div>
		@endif
		<div class="row">
			<div class="col-12">
				@if ($airlines)
				<div class="table-responsive m-t-20">
					<table class="table table-striped  table-bordered">
						<thead>
							<tr>
								<th>Code</th>
								<th>Name</th>
								<th>Country</th>
								<th class="text-center">Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($airlines as $airline)
							<tr>
								<td>{{ $airline->airline_code }}</td>
								<td>{{ $airline->airline_name }}</td>
								<?php $country = \App\Country::find($airline->country_id); ?>
								@if ($country)
								<td>{{ $country->name }} ({{ $country->code }})</td>
								@else
								<td>No country selected</td>
								@endif
								<td class="text-center">
									<a href="#" class="button success tiny btn-primary btn-sm update-btn  m-r-10" data-id="{{ $airline->id }}" data-code="{{ $airline->airline_code }}" data-name="{{ $airline->airline_name }}" data-country="{{ $airline->country_id }}"><i class="fa fa-pencil-square-o"></i> Edit</a>

									<a href="#" class="button success tiny btn-primary btn-sm delete-btn m-r-10" data-id="{{ $airline->id }}"><i class="fa fa-trash"></i> Delete</a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				@else
				<h5>No data found.</h5>
				@endif
			</div>
		</div>
	</div>
</div>
<div class="row full-width-row">
	<div class="small-12 column">
		{{ $airlines->appends(Request::except('page'))->links() }}
	</div>
</div>
@endsection
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       <h5 id="modalTitle" class="text-center">ADD NEW AIRLINE</h5>
      </div>
     
      <div class="modal-body">
        <form id="airline-form" method="post" enctype="multipart/form-data">
       
		<div class="row">
			<div class="col-sm-3 ">
				<label for="airline-code" class="right inline">Airline Code</label>
			</div>
			<div class="col-sm-9">
				<div class="form-group">
				<input type="text" name="airline_code" id="airline-code" required class="form-control m-t-10">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3 ">
				<label for="airline-code" class="right inline">Airline Name</label>
			</div>
			<div class="col-sm-9">
				<div class="form-group">
				<input type="text" name="airline_name" id="airline-name" required class="form-control m-t-10">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3 ">
				<label for="airline-code" class="right inline">Country</label>
			</div>
			<div class="col-sm-9">
				<div class="form-group">
				<select name="country_id" id="country" class="form-control m-t-10" required>
					<option value="" selected disabled>Select Country</option>
					@foreach ($countries as $country)
					<option value="{{ $country->id }}">{{ $country->name }} ({{ $country->code }})</option>
					@endforeach
				</select>
				</div>
			</div>
		</div>
		<input type="hidden" name="_token" value="{{ csrf_token() }}"><br><br>
		<div class="row">
			<div class="col-sm-3 ">
			</div>
			<div class="col-sm-9">
				<input type="submit" value="Submit" class="btn btn-info">
			</div>
		</div>		
	</form>
      </div>
    </div>

  </div>
</div>





@section('custom-js')
<script type="text/javascript">
	$(document).ready(function() {
		$('.delete-btn').on('click', function(e) {
			e.preventDefault();
			var id = $(this).data('id');
			var tr = $(this).closest('tr');
			if (confirm('Are you sure you want to delete this airline?')) {
				$.ajax({
					method: 'post',
					url: '{{ url('transport/airlines') }}'+'/'+id+'/delete',
					data: {
						_token: '{{ csrf_token() }}'
					},
					success: function() {
						tr.fadeOut();
					}
				});
			}
		});
	
		$('.update-btn').on('click', function(e) { 
			e.preventDefault();
			var id = $(this).data('id');
			var code = $(this).data('code');
			var name = $(this).data('name');
			var country = $(this).data('country');
	
			$('#airline-code').val(code);
			$('#airline-name').val(name);
			$('#country option[value="'+country+'"]').prop('selected', true);
			$('#airline-form').attr('action', '{{ url('transport/airlines') }}'+'/'+id+'/update');
			$('#myModal').modal('show');
		});
	
		$('#add-airline-btn').on('click', function() {
			$('#airline-form')[0].reset();
			$('#airline-form').attr('action', '{{ url('transport/airlines-add') }}');
			
		});
	
	});
	$('#airline-form').each(function() {
		$(this).find('input').keypress(function(e) {
		   if(e.which == 10 || e.which == 13) {
				return false;
			}
		});
	});
</script>
@endsection