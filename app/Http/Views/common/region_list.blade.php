@extends( 'layout/mainlayout' )

@section('custom-css')
<style type="text/css">
.select-user-type {
	display: inline-block;
	border-radius: 4px;
	text-align: center;
	font-size: 0.9rem;
	background: #dcdcdc;
	padding: 10px 25px;
	color: #333;
	transition: all .2s;
}
.select-user-type:hover, .select-user-type.selected {
	background: #666666;
	color: #fff;
}
.select-user-type.selected {
	cursor: default;
}
.search-box {
	margin: 25px 0;
	position: relative;
}
.search-box i.fa {
	position: absolute;
	top: 10px;
	left: 7px;
}
#search-key {
	padding-left: 25px;
}
.fa-check {
	color: #1c812f;
}
.fa-times,
.fa-exclamation-circle {
	color: #bd1b1b;
}
.user-name a {
	color: #2b78b0;
	font-weight: bold;
}
.ajax-loader {
	font-size: 1.5rem;
	display: none;
}
</style>
@stop

@section('content')

<div class="content-container">
    <h1 class="page-title">{{ trans('messages.manage_list_title', ['name' => 'Geo Data']) }}</h1>

    <div class="row">
        <div class="small-12 small-centered columns delete-box hidden"></div> 
    </div>
    @if(Session::has('message'))
    <div class="row">
        <div class="small-12 small-centered columns success-box">{{ Session::get('message') }}</div> 
    </div>
    <br>
    @endif
    <div class="box-wrapper">
        <!--<div class="row m-t-20 search-wrapper">
            <div class="col-md-7 col-sm-7">
                <div class="input-group input-group-box">
                    <input type="text" class="form-control" placeholder="Search {{ trans('messages.region') }}" name="search_str" value="{{ $sSearchStr }}">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="submit" onclick="getMoreListing(siteUrl('common/region-list'),event,'table_record');"><i class="icon-search-domain"></i></button>
                    </span>
                </div>
            </div>
            <input type="hidden" name="order_field" value="{{ $sOrderField }}" />
            <input type="hidden" name="order_by" value="{{ $sOrderBy }}" />
            <div class="col-md-5 col-sm-5">
                <div class="dropdown">
                    <button class="btn btn-primary btn-block dropdown-toggle btn_action" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" disabled>ACTION</button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    </ul>
                </div>
            </div>
        </div>-->
        <div class="table-responsive m-t-20 table_record">
           
            @include('WebView::common._region_list_ajax')
      
        </div>
		
			<br><br>
			<center>
				<a id="region" data-url="{{route('user-region-geodata')}}" data-id=""   class="btn	btn-info geo-links d-none">Regions</a>
				&nbsp;&nbsp;&nbsp;
				<a id="country" data-url="{{route('user-country-geodata')}}"	data-id=""   class="btn	btn-success	geo-links d-none">Country</a>
				&nbsp;&nbsp;&nbsp;
				<a id="city"data-url="{{route('user-city-geodata')}}"	data-id=""    class="btn	btn-danger	geo-links d-none">City</a>
			</center>

			<center>
				<span id="no-data" class="d-none" >
					 <h4>No Data found for this domain.</h4>
				</span>
				<span id="Loader" class="d-none" >
					<br><br>
					<img  src="{{asset('assets/images/loader_m.gif')}}">
				</span>
			</center>
		
    </div>

</div>

<script type="text/javascript">
$(document).on('ready',function(){
	$('.switch1-state1').bootstrapSwitch();
	$(document).on('tbody switchChange.bootstrapSwitch','.switch1-state1', function (event, state) {
            switchChange(state,this);
	});
    });
	
function getDomainsbyLicenseId(licensee_id)
{
	$.ajax({
		url: "{{ route('common.get-domains-by-licensee_id') }}",
		method: 'post',
		data: {
			licensee_id: licensee_id,
			_token: '{{ csrf_token() }}'
		},
		success: function( response ) 
		{
			if(response){
				var html = '<option value="" selected>Select Domain</option>';
				$("#Domains").html(response).prepend(html);
			}
		}
	});
}
	
function switchChange(state,oEle)
{
    var id = $(oEle).data('id');
    var checked = (state === true) ? 1 : 0;
    var label = $(this).next('label');
    showEroamSwitch("{{ route('common.region-switch') }}"+'/'+id , checked);
}
function getUserSort(element,sOrderField,event)
{
    if($(element).find( "i" ).hasClass('fa-caret-down'))
    {
        $(element).find( "i" ).removeClass('fa-caret-down');
        $(element).find( "i" ).addClass('fa-caret-up');
        $("input[name='order_field']").val(sOrderField);
        $("input[name='order_by']").val('desc');
    }
    else
    {
        $(element).find( "i" ).removeClass('fa-caret-up');
        $(element).find( "i" ).addClass('fa-caret-down');
        $("input[name='order_field']").val(sOrderField);
        $("input[name='order_by']").val('asc');
    }
    getMoreListing(siteUrl('common/region-list'),event,'table_record');
}
$(document).on('click',".cmp_check",function () { 

    if($('.cmp_check:checked').length == $('.cmp_check').length){
        $('#checkbox-00').prop('checked',true);
    }else{
        $('#checkbox-00').prop('checked',false);
    }
});
$(document).on('click','.label_check',function(){
    setupLabel();
});  
</script>
@stop
