@foreach ($oCurrencyList as $aCurrency)
<tr>
    <td>
        <label class="radio-checkbox label_check" for="checkbox-<?php echo $aCurrency->id; ?>">
            <input type="checkbox" class="cmp_coupon_check" id="checkbox-<?php echo $aCurrency->id; ?>" value="<?php echo $aCurrency->id; ?>">&nbsp;
        </label>
    </td>
    <td>
        <a href="#">
            {{$aCurrency->name }}
        </a>
    </td>
    <td>{{ $aCurrency->code }}</td>

    <td>
        <a href="{{ route('common.create-currency',['nIdCoupon'=>$aCurrency->id])}}" class="button success tiny btn-primary btn-sm pull-left m-r-10">{{ trans('messages.update_btn')}}</a>
        <input type="button" class="button btn-delete tiny btn-primary btn-sm" value="{{ trans('messages.delete_btn') }}" onclick="callDeleteRecord(this,'{{ route('common.currency-delete',['id'=> $aCurrency->id]) }}','{{ trans('messages.delete_label')}}')">
    </td>
</tr> 
@endforeach