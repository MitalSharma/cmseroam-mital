@foreach ($oCountryList as $aCountry)
    <tr>
        <td>
            <label class="radio-checkbox label_check" for="checkbox-<?php echo $aCountry->id;?>">
                <input type="checkbox" class="cmp_check" id="checkbox-<?php echo $aCountry->id;?>" value="<?php echo $aCountry->id;?>">&nbsp;
            </label>
        </td>
        <td>
            <a href="#">
                {{ $aCountry->code.'-'.$aCountry->name }}
            </a>
        </td>
        <td>
            <span data-country-id="{{ $aCountry->id }}" data-region-id="{{ $aCountry->region_id }}">
                <a href="javaScript:void(0)" title="Update Region" class="update-region-btn"><i class="fa fa-pencil-square-o"></i></a><span class="region-name">{{ $aCountry->region_name }}</span>
            </span>
            <div class="update-region-box"></div>
        </td>
        <td class="text-center">
            <div class="switch tiny switch_cls">
                <input class="show-on-eroam-btn switch1-state1" data-id="{{ $aCountry->id }}" id="show-on-eroam-{{ $aCountry->id }}" type="checkbox" {{ $aCountry->show_on_eroam == 1 ? 'checked' : '' }}>
                <label for="show-on-eroam-{{ $aCountry->id }}"></label>
            </div>
        </td>
    </tr> 
@endforeach