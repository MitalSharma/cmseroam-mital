@extends( 'layout/mainlayout' )

@section('content')
<div class="content-container">
    @if($nIdCurrency == '')
        <h1 class="page-title">{{ trans('messages.add',['name' => 'Currency']) }}</h1>
    @else
        <h1 class="page-title">{{ trans('messages.update',['name' => 'Currency']) }}</h1>
    @endif
    <div class="row">
        @if (Session::has('message'))
        <div class="small-12 small-centered columns success-box">{{ Session::get('message') }}</div>
        @endif

    </div>
    <br>
    @if(isset($oCurrency))
    {{ Form::model($oCurrency, array('url' => route('common.create-currency') ,'method'=>'POST','id'=>'addForm')) }}
    @else
    <form method="POST" action="{{ route('common.create-currency') }}" id="addForm"> 
    @endif

    <div class="box-wrapper">

        <p>{{ trans('messages.currency_detail') }}</p>
        {{ csrf_field() }}

        <div class="form-group m-t-30">
            <label class="label-control">{{ trans('messages.name') }} <span class="required">*</span></label>
            <?php
            $attributes = 'form-control';
            ?>
            {{Form::text('name',Input::old('name'),['id'=>'name','class'=>$attributes,'placeholder'=>'Enter Name'])}}

        </div>
        @if ( $errors->first( 'name' ) )
        <small class="error">{{ $errors->first('name') }}</small>
        @endif
        <div class="form-group m-t-30">
            <label class="label-control">Code <span class="required">*</span></label>
            <?php
            $attributes = 'form-control';
            ?>
            {{Form::text('code',Input::old('code'),['id'=>'code','class'=>$attributes,'placeholder'=>'Enter Code'])}}

        </div>
        @if ( $errors->first( 'code' ) )
        <small class="error">{{ $errors->first('code') }}</small>
        @endif
    </div>
    <input type="hidden" name="currency_id" value="{{ $nIdCurrency }}" />

    <div class="m-t-20 row col-md-8 col-md-offset-2">
        <div class="row">
            <div class="col-sm-6">
                {{Form::submit('Save',['class'=>'button success btn btn-primary btn-block']) }}
            </div>
            <div class="col-sm-6">
                <a href="{{ route('common.currency-list') }}" class="btn btn-primary btn-block">Cancel</a>
            </div>
        </div>
    </div>	
    {{Form::close()}}
</div>
@stop

@section('custom-css')
<style>

    .error{
        color:red !important;
    }
    .with_error{
        border-color: red !important;
    }

    div .with_error{
        border:1px solid black;
    }
    .error_message{
        color:red !important;
    }

    .success_message{
        color:green !important;
        text-align: center;
    }
</style>
@stop

@section('custom-js')
<script>
$(function() {

        $("#addForm").validate({
            rules: {
                name          : "required",
                code        : "required",
            },
            errorPlacement: function(error, element) {
                var placement = $(element).parent();
                if (placement) {
                  $(error).insertAfter(placement)
                } else {
                  error.insertAfter(element);
                }
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
     });

</script>
@stop
