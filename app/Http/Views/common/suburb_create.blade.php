@extends( 'layout/mainlayout' )

@section('custom-css')
<style>
    .error {
        color: red !important;
    }
    .success_message{
        color:green !important;
        text-align: center;
    }

    .error_message{
        color:red !important;
    }
    .with_error{
        border-color: red !important;
    }
    .success_message{
        color:green !important;
        text-align: center;
    }
    div .with_error{
        border:1px solid black;
    }
</style>
@stop

@section('content')

<div class="content-container">
    @if($nIdSuburb == '')
        <h1 class="page-title">{{ trans('messages.add',['name' => 'Suburb']) }}</h1>
    @else
        <h1 class="page-title">{{ trans('messages.update',['name' => 'Suburb']) }}</h1>
    @endif
    
    @if (Session::has('message'))
    <div class="small-12 small-centered columns success_message">{{ Session::get('message') }}</div>
    <br>
    @endif
    <?php //echo "<pre>";print_r(Input::old('name'));exit; ?>
    @if ($errors->any())
    <div class=" error_message">{{$errors->first()}}</div>
    @endif
    @if($nIdSuburb == '')
        {{ Form::open(array('id' => 'addCouponForm', 'url' => 'common/create-suburb','method'=>'Post')) }}
    @else
        {{ Form::model($oSuburb,array('id' => 'addCouponForm', 'url' => 'common/create-suburb','method'=>'Post')) }}
    @endif
    
    <div class="box-wrapper">
        <div class="form-group">
            <label class="label-control">Name <span class="required">*</span></label>
            {{Form::text('name',Input::old('name'),['id'=>'name','class'=>'form-control','placeholder'=>'Enter Name'])}}
        </div>
        <input type="hidden" name="id" value="{{ isset($oSuburb) ? $oSuburb->id : '' }}" />
        
        <div class="form-group" id="currencyDiv">
            <label class="label-control">Country <span class="required">*</span></label>
            {{ Form::select('country_id',$countries,$nIdCountry,['class'=>'form-control select-country'])}}
        </div>
        
        <div class="row m-t-30">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">City <span class="required">*</span></label>
                    <select name="city_id" id="city_id" class='form-control select-city'>
                        <option value="">Select City</option>

                    </select>
                </div>
                <input type="hidden" id="city" value="{{ isset($oSuburb->city_id) ? $oSuburb->city_id : ''}}" />
                <label for="city_id" generated="true" class="error" style="display:none">This field is required.</label>		
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label class="label-control">HB Zone <span class="required">*</span></label>
                    <select name="hb_zone_id" id="hb_zone_id" class='form-control select-hbzone'>
                        <option value="">Select HB Zone</option>

                    </select>	
                </div>
                <input type="hidden" id="hbzone" value="{{ isset($oSuburb->hb_zone_id) ? $oSuburb->hb_zone_id : ''}}" />
                <label for="postcode" generated="true" class="error" style="display:none">This field is required.</label>	
            </div>	
        </div>
       
    </div>
    <div class="m-t-20 row">
        <div class="col-sm-6">
            {{Form::submit('Save',['class'=>'btn btn-primary btn-block', 'id'=>'btnAddCoupon']) }}
        </div>
        <div class="col-sm-6">
            <a href="{{route('common.suburb-list')}}" class="btn btn-primary btn-block">{{ trans('messages.cancel_btn') }}</a>
        </div>
    </div>
    {{ Form::close() }}
</div>
@stop

@section('custom-js')
<script>
    $(document).ready(function () {
        getCity();
        setTimeout(function(){
            getHBZone();
        },1000);
        $("#addCouponForm").validate({
            rules: {
                name: "required",
                country_id: "required",
                city_id: "required",
                hb_zone_id: "required"
            },
            errorPlacement: function (label, element) {
                label.insertAfter(element);
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    });
    
    $( '.select-country' ).change( function() {
        getCity();
    });
    
    function getCity(){
        var value = $( '.select-country' ).val();
        var city = $( '#city' ).val();
        
        if(value != ''){
        $.ajax({
            url: "{{ route('common.get-cities-by-country') }}",
            method: 'post',
            data: {
                country_id: value,
                _token: '{{ csrf_token() }}'
            },
            success: function( response ) 
            {
                
                var selectCity = $('.select-city');
                selectCity.html('<option value="" selected disabled>Select City</option>');
                for(var i = 0; i < response.length; i++) {
                    var selected = '';
                    if(city == response[i].id) 
                        selected = "selected";
                    var html = '<option value="'+ response[i].id +'" '+selected+'>'+ response[i].name +'</option>';
                    $( '.select-city' ).append(html);
                }
            }
        });
        }
    }
    
    $( '.select-city' ).change( function() {
        getHBZone();
    });
    
    function getHBZone(){
        var value = $( '.select-city' ).val();
        var city = $( '#hbzone' ).val();
        
        if(value != ''){
        $.ajax({
            url: "{{ route('common.get-hbzone-by-cityid') }}",
            method: 'post',
            data: {
                city_id: value,
                _token: '{{ csrf_token() }}'
            },
            success: function( response ) 
            {
                var selectCity = $('.select-hbzone');
                selectCity.html('<option value="" selected disabled>Select HBZone</option>');
                for(var i = 0; i < response.length; i++) {
                    var selected = '';
                    if(city == response[i].id) 
                        selected = "selected";
                    var html = '<option value="'+ response[i].id +'" '+selected+'>'+ response[i].zone_name +'</option>';
                    $( '.select-hbzone' ).append(html);
                }
            }
        });
        }
    }
</script>
@stop
