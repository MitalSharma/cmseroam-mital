<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Cache;
use GuzzleHttp\Client;

use App\City;
use App\Region;
use App\Transport;
use App\LatLong;
use App\RoutePlan;
use App\ActivityNew;
use App\TransportType;
use App\HotelRoomType;
use App\Hotel;
use App\AotSupplier;
use App\ActivityPrice;
use App\TransportPrice;
use App\TransportSupplier;
use App\ViatorLocation;
use App\Currency;
use App\Label;
use App\Suburb;
use App\HotelPrice;
use App\Country;
use App\CitiesLatLong;
use App\Timezone;
use App\HBDestination;
use App\AECity;
use App\AOTLocation;
use App\AOTMapSupplierCity;
use App\HBDestinationsMapped;
use App\AECitiesMapped;
use App\ViatorLocationsMapped;
use App\CityImage;
use App\Coupon;
use App\HotelCategory;
use App\Nationality;
use App\AgeGroup;
use App\Domain;
use DB;
use Log;
use Carbon\Carbon;
use Exception;
use Libraries\Expedia;
use Libraries\Yalago;
use App\YalagoLocations;

class MapApiController extends Controller 
{
	
	/**
	 * Get all related cities using.
	 * Parameters city_id
	 * @return Response
	 */
	
	public function getAllRelatedCities($country_id){
		
		$cities = City::select('id','name','optional_city')->with('latlong', 'iata','iatas')->where('country_id',$country_id)->orderBy('name')->get();
		return \Response::json(['status'=>200,'data'=>$cities]);
	}


	/**
	 * Get all related Countries using.
	 * 
	 * @return Response
	 */
	
	public function getAllCountries(){
            $countries = Region::with('countriesShowonEroam')
                                    ->where('show_on_eroam', 1)
                                    ->get();
            //dd(DB::getQueryLog());
            return \Response::json([ 'status' => 200, 'data' => $countries ]);
	}


	/**
	 * get city data using city_id
	 * @return Response
	 */

	public function getCityByCityId($id){
		$city = city::select('id','name','region_id','geohash','optional_city','default_nights','description','small_image','row_id','country_id','country_id','is_disabled','airport_codes')->with('latlong','iata','iatas','timezone' )->where('id', $id)->first();
		return \Response::json(['status'=>200,'data'=>$city]);
	}

	/**
	 * get nearby city if has transport 
	 * @return Response
	 */

	public function getNearbyCities(){

	$input = Input::all();


    $transports = Transport::where( 'from_city_id', $input['cityId'] )->pluck( 'to_city_id' );
	
	$transports = json_decode(json_encode($transports));	   
   	
	$in_bounds = latlong::all();

	$cities = [];
	foreach ($in_bounds as $key => $in_bound) {
		if($in_bound['lat'] > $input['slat'] && $in_bound['lat'] < $input['nlat']
		  && $in_bound['lng'] < $input['nlng'] && $in_bound['lng'] > $input['slng']  ){
			$cities[] = $in_bound['city_id']; 
		}
	}		                  
	
	$nearby_cities = City::with('latlong', 'iata')
	->whereIn( 'id', $transports )
	->orwhereIn( 'id', $cities )
	->get();

	foreach ( $nearby_cities as $city ) {
	   $city->has_transport = ( in_array( $city->id, $transports ) ) ? 1 : 0;
	}
		return \Response::json(['status'=>200,'data'=>$nearby_cities]);

	}
	/**
	 * get transport
	 * @return Response
	 */

	public function getTransportByCitieIDs(){
		$input = Input::all();
		$input['to_city_id'] = 9;
		$input['from_city_id'] = 26;
		$transports = Transport::with('price','transporttype', 'currency', 'operator')->where('from_city_id',$input['from_city_id'])
							   ->where('to_city_id',$input['to_city_id'])
							   ->where('[default]',1)->get();					   


						   
		$result = array();

		foreach($transports as $transport_key => $transport)
		{	
			$price_count = count($transport->price);
			foreach($transport->price as $price_key => $price)
			{
				if($price->transport_supplier_id != $transport->default_transport_supplier_id)
				{
					$price_count--;
					unset($transport->price[$price_key]); // remove price/room if the supplier is not the hotel's default supplier
				}

				if( $price->price == "0.0" )
				{
					$price_count--;
					unset($transport->price[$price_key]); // remove price/room if the supplier is not the hotel's default supplier
				}

			}

			if($price_count > 0) // remove hotel if there is no price/room
			{
		
				array_push($result, $transport);
			}			
		}					   
		return \Response::json(['status'=>200,'data'=>$result]);


	}


	/**
	 * get autoroutes
	 * @return Response
	 */

	public function getFixedCities() { // UPDATED BY RGR
		$data = [];
		$route_plan = RoutePlan::where([
			'from_city_id' => Input::get('from_city_id'),
			'to_city_id' => Input::get('to_city_id')
		])->with('routes')->first();

		if ($route_plan && $route_plan->routes) {
			$data = $route_plan->routes;
		}

		return \Response::json(['status'=>200, 'data'=> $data]);
	}


	/**
	 * get all cities
	 * @return Response
	 */
	
	public function getAllCities(){

		// start add by miguel to filter out cities without latlong
		$city_ids_with_latlong = LatLong::pluck('city_id');
		// end add by miguel to filter out cities without latlong

		$cities = City::select(
				'zcities.id','zcities.name','zcities.region_id','zcities.geohash',
				'zcities.optional_city','zcities.default_nights','zcities.description','zcities.small_image','zcities.row_id','zcities.country_id','zcities.is_disabled','zcities.airport_codes','zcities.timezone_id',
		    	'zcountries.Name as country_name'
			)
			->join('zcountries', 'zcountries.id', '=', 'zcities.country_id')
			->join('zregions', 'zregions.id', '=', 'zcountries.region_id')
			->with('image','timezone', 'ae_city_mapped','hb_destinations_mapped', 'viator_locations_mapped', 'aot_supplier_locations_mapped', 'latlong','iata','iatas', 'suburbs','yalago_city_mapped')
			->where('zcities.is_disabled', 0)
			->where('zcountries.show_on_eroam', 1)
			->where('zregions.show_on_eroam', 1)
			->whereIn('zcities.id', $city_ids_with_latlong) // add by miguel to filter out cities without latlong
			//->ae_city()
			->get();

		// code to manually append hb_zone data in the suburbs because if parameter limit error on mssql; fix by miguel on 2017-04-26
		
		foreach( $cities as $c_key => $city ){
			foreach( $city->suburbs as $s_key => $suburb ){
				$s = Suburb::where( 'id', $suburb->hb_zone_id )->first();
				$cities[ $c_key ]->suburbs[ $s_key ]->hb_zone = $s;
			}
		}
		//echo '<pre>';print_r($cities);exit;
		return \Response::json(['status'=>200,'data'=>$cities,'city_ids_with_latlong'=>$city_ids_with_latlong]);


	}

	/**
	 * get all the default hotel,activity,transport
	 * @return Response
	 */
	public function findAllDefault(){
		$data   = Input::all();
		//$data = '{"city_ids":["7","37"],"date_from":"2018-09-19","auto_populate":"1","traveller_number":"1","child_number":"0","search_input":{"_token":"Rmj8PH6mHwRCAHAj20hqZUC8GJ8Rav9RBVDLkX2A","country":"513","city":"7","to_country":"513","to_city":"37","auto_populate":"1","countryId":null,"countryRegion":null,"countryRegionName":null,"option":"auto","searchValType2":null,"searchVal2":null,"start_location":"Sydney, Australia","end_location":"Perth, Australia","start_date":"2018-09-19","rooms":"1","num_of_adults":["1"],"num_of_children":["0"],"travellers":"1","total_children":"0","child":[],"interests":[],"travel_preferences":[{"cabin_class":null,"accommodation":[null],"room":["2"],"transport":["0"],"age_group":null,"nationality":null,"gender":null,"interestLists":null,"interestListIds":[null]}]},"rooms":"1","child":[],"pax_information":[],"apis":["static_accomodation","static_transport","static_activity"]}';
		//$data = '{"city_ids":["7","37"],"date_from":"2018-11-19","auto_populate":"1","traveller_number":"1","child_number":"0","search_input":{"_token":"Rmj8PH6mHwRCAHAj20hqZUC8GJ8Rav9RBVDLkX2A","country":"513","city":"7","to_country":"513","to_city":"37","auto_populate":"1","countryId":null,"countryRegion":null,"countryRegionName":null,"option":"auto","searchValType2":null,"searchVal2":null,"start_location":"Sydney, Australia","end_location":"Perth, Australia","start_date":"2018-11-19","rooms":"1","num_of_adults":["1"],"num_of_children":["0"],"travellers":"1","total_children":"0","child":[],"interests":[],"travel_preferences":[{"cabin_class":null,"accommodation":["2"],"room":["2"],"transport":["0"],"age_group":null,"nationality":null,"gender":null,"interestLists":null,"interestListIds":[null]}]},"rooms":"1","child":[],"pax_information":[],"apis":["static_accomodation","static_transport","static_activity"]}';
		//$data = '{"city_ids":["7","37"],"date_from":"2018-10-26","auto_populate":"1","traveller_number":"1","child_number":"0","search_input":{"_token":"anFzvpowrSzBFlSUVImsmjC1LVzkFqLG9o3cilQX","country":"513","city":"7","to_country":"513","to_city":"37","auto_populate":"1","countryId":null,"countryRegion":null,"countryRegionName":null,"option":"auto","searchValType2":null,"searchVal2":null,"start_location":"Sydney, Australia","end_location":"Perth, Australia","start_date":"2018-10-26","rooms":"1","travellers":"1","total_children":"0","num_of_pax":["1"],"is_child":{"1":{"1":"1"}},"firstname":{"1":{"1":"keshav"}},"lastname":{"1":{"1":"khatri"}},"dob":{"1":{"1":"03-05-1985"}},"age":{"1":{"1":"33"}},"child":[],"interests":[],"travel_preferences":[{"cabin_class":"Y","accommodation":["3"],"room":["23"],"transport":["2"],"age_group":"","nationality":null,"gender":null,"interestLists":"Astronomy,Backpacking,Urban-Adventures,Ballooning,Camping,Overland,Expeditions,Festivals,Cycling,Family","interestListIds":["6","18","8","5","12","9","1","13","24","3"]}],"num_of_adults":[1],"num_of_children":[0],"pax":[[{"firstname":"keshav","lastname":"khatri","dob":"03-05-1985","age":"33","child":0}]]},"rooms":"1","child":[],"pax_information":[],"apis":["static_accomodation","static_transport","static_activity","expedia","yalogo","mystifly","busbud","viator"]}';
		//$data = '{"city_ids":["394"],"date_from":"2018-10-28","auto_populate":"1","traveller_number":"1","child_number":"0","search_input":{"_token":"S69SC4C5A9sSxygdJuCktkl2oljW7ePitHG8kula","country":null,"city":null,"to_country":"575","to_city":"394","auto_populate":"1","countryId":null,"countryRegion":null,"countryRegionName":null,"option":"manual","searchValType2":null,"searchVal2":null,"start_location":null,"end_location":"Paris, France","start_date":"2018-10-28","rooms":"1","travellers":"1","total_children":"0","num_of_pax":["1"],"is_child":{"1":{"1":"1"}},"firstname":{"1":{"1":"rekha"}},"lastname":{"1":{"1":"test"}},"dob":{"1":{"1":"03-05-1985"}},"age":{"1":{"1":"33"}},"child":[],"interests":[],"travel_preferences":null,"num_of_adults":[1],"num_of_children":[0],"pax":[[{"firstname":"rekha","lastname":"test","dob":"03-05-1985","age":"33","child":0}]]},"rooms":"1","child":[],"pax_information":[],"apis":["static_accomodation","static_transport","static_activity","expedia","yalago","mystifly","busbud","viator"]}';
		//$data = json_decode($data,true);
		//echo "<pre>";print_r($data);exit;
	
		Log::debug($data);
		
		$aData = $data;
		//changes bcz data pas in different way ..i confirmed with rekha mam << priya
		$hotel_category_id = isset($data['search_input']['travel_preferences'][0]['accommodation'][0]) ? $data['search_input']['travel_preferences'][0]['accommodation']:[] ;
		$transport_types   = isset($data['search_input']['transport_types']) ? $data['search_input']['transport_types']:[];
		$interests         = isset($data['search_input']['interests']) ? array_filter($data['search_input']['interests']):[];
		$room_type_id      = isset($data['search_input']['travel_preferences'][0]['room'][0]) ? array_filter( is_string( $data['search_input']['travel_preferences'][0]['room'][0]) ? explode( ",", $data['search_input']['travel_preferences'][0]['room'][0]) : $$data['search_input']['travel_preferences'][0]['room'][0]  ):[];
		$gender			   = isset($data['search_input']['gender']) ? $data['search_input']['gender']:null;
		$date              = Carbon::parse($data['date_from'])->format('Y-m-d');

		$all_default_data  = []; 
		$total_price       = [];
		Input::merge(['hotel_category_id' => $hotel_category_id ]);
		Input::merge(['transport_types' => $transport_types ]);
		Input::merge(['interests' => $interests]);
		Input::merge(['room_type_id' => $room_type_id]);
		Input::merge(['gender' => $gender]);


		$aData['hotel_category_id'] = $hotel_category_id;
		$aData['transport_types'] = $transport_types;
		$aData['provider'] = 'eroam';
		$aData['interests'] = $interests;
		$aData['room_type_id'] = $room_type_id;
		$aData['gender'] = $gender;
		$aData['rooms'] = $data['rooms'];


		$prev_city_default_night = 0;


        if (!empty($data['apis'])) {
            $apis = !empty($data['apis']) ? $data['apis'] : '';
        }else{
            $domain = request()->header('domain');
            $apis = getInventory($domain);
            $apis = $apis['inventory'];
            foreach ($apis as $api) {
                $apiFinal[] = $api->inventory_config->slug;
            }
            $apis = $apiFinal;
        }

		$allCityData = City::with('latlong','timezone', 'ae_city_mapped','hb_destinations_mapped', 'viator_locations_mapped', 'aot_supplier_locations_mapped','yalago_city_mapped','country','iata','iatas','image')->whereIn('id',$data['city_ids'])->get()->keyBy('id');

        $allCityDataByCityId = $allCityData;
        /*foreach($allCityData as $cityData) {
            $allCityDataByCityId[$cityData->id] = $cityData;
        }*/

		foreach ($data['city_ids'] as $key => $value)
		{		
			$activity_data           = [];
			$data['date_from']       = Carbon::parse($date)->addDays($prev_city_default_night)->format('Y-m-d');			
			$date                    = $data['date_from'];
			//$city_data               = City::with('latlong','timezone', 'ae_city_mapped','hb_destinations_mapped', 'viator_locations_mapped', 'aot_supplier_locations_mapped','country','iata','iatas','image')->where('id',$value)->first();
			$city_data = $allCityDataByCityId[$value];
			$prev_city_default_night = $city_data['default_nights'];

			//$city = City::select('id','name','country_id')->with('Country')->where('id',$value)->first();

            Input::merge(['searchCityName' => $city_data->name]);
            Input::merge(['searchCityId' => $value]);
            Input::merge(['searchCountryCode' => $city_data->Country->code]);

            $aData['searchCityName'] = $city_data->name;
            $aData['searchCityId'] = $value;
            $aData['searchCountryCode'] = $city_data->Country->code;
			
			if( $data['auto_populate'] ){

				Input::merge(['city_ids' => [$value]]);
				Input::merge(['date_to' => $data['date_from']]);
				Input::merge(['date_from' => $data['date_from']]);

				$aData['city_ids'] = $value;
				$aData['date_to'] = $data['date_from'];
				$aData['date_from'] = $data['date_from'];

				if(in_array('static_activity', $apis)) {
					// $result = $this->get_all_activity_by_city_id($aData);
	  		    	// $activity_data = $result->getData()->data;
					$result = [];
					$activity_data = [];
				} else {
					$result = [];
					$activity_data = [];
				}

				
				if(count($activity_data) > 0)
				{
					foreach ($activity_data as $k => $activity)
					{
						
						$activity->price = [];
						foreach ($activity->activity_price as $element => $price)
						{	
							if( strtotime($price->date_from) < strtotime($date) && strtotime($price->date_to) > strtotime($date) )
							{
								array_push( $activity->price, $price );
							}
						}
						$activity->activity_price = $activity->price;

						$activity_data = array_filter($activity_data);	
					}
				}
		   	}

			if(count($data['city_ids']) > 1 && isset($data['city_ids'][$key+1]))
			{
				Input::merge(['date_to' => $data['date_from']]);
				Input::merge(['from_city_id' => $value]);
				Input::merge(['to_city_id' => $data['city_ids'][$key+1]]);
				Input::merge(['day' => 0]);
				Input::merge(['date_from' => $data['date_from']]);
				
				$aData['date_to'] = $data['date_from'];
				$aData['from_city_id'] =  $value;
				$aData['to_city_id'] = $data['city_ids'][$key+1];
				$aData['day'] = 0;
				$aData['date_from'] = $data['date_from'];
				
				$result = array();
				$transport_data = array();
				$preferences = isset($data['search_input']['travel_preferences']) ? $data['search_input']['travel_preferences'] : array();
				if(isset($preferences[0]['CabinClasses']) && $preferences[0]['CabinClasses'] != ''){
                    $CabinClasses = $preferences[0]['CabinClasses'];
                }else{
                    $CabinClasses = 'Y';
                }
                if(in_array('static_transport', $apis)) {
					if(isset($preferences[0]['transport'][0])){
						if($preferences[0]['transport'][0] == 2  && $CabinClasses == 'Y'){
							$result = $this->get_all_transport_by_city_ids($aData);
	  		         		$transport_data = $result->getData()->data;
	  		        		/*$result = array();
							$transport_data = array();*/
						}else{
							$result = array();
							$transport_data = array();
						}
					}else{
						$result = $this->get_all_transport_by_city_ids($aData);
	  		        	$transport_data = $result->getData()->data;
	  		        	/*$result = array();
						$transport_data = array();*/
					}
                } else {
                	$result = array();
					$transport_data = array();
                }
										  	
				if(count($transport_data) > 0)
				{
				 	foreach ($transport_data as $k => $transport)
				 	{
						foreach ($transport->price as $element => $price)
						{	
							// EDITED BY MIGUEL ON 2017-03-28
							// IF season start date is beyond the selected date or the season end date is behind the selected date THEN unset the season/price
							if( strtotime($price->from) > strtotime($date) || strtotime($price->to) < strtotime($date) )
							{
								unset($transport_data[$k]->price[$element]);
								
							}
						}
						// EDITED BY MIGUEL ON 2017-03-28
						// IF the transport does not have prices THEN unset the transport
						if( isset($transport->price) && count((array)$transport->price) < 1 )
						{
						   unset( $transport_data[$k] );
						}

						if(count($transport_types) > 0  && $transport->transport_type_id == $transport_types[0]){
							$transport_data[0] = $transport;
							break;
						}

						if(empty($transport_types) && $transport->transport_type_id == 2 ){ //default coach or minivan
							$transport_data[0] = $transport;
							break;
						}



			  		}	
			    }														  
			}
			//echo "<pre>";print_r($transport_data);exit;
			Input::merge(['city_default_night' => $prev_city_default_night]);

			$aData['city_default_night'] = $prev_city_default_night;

			$hotel_data = [];
			$hotelInfo = [];
			if(in_array('static_accomodation', $apis)) {
				$hotel_data = $this->hotel_by_city_id_v2($aData);
				$hotel_data = $hotel_data->getData()->data;
				if (!empty($hotel_data)) {
					$hotel_data = json_decode(json_encode($hotel_data[0]), True);
				} else {
					$hotel_data = [];
	  		    	$hotelInfo = [];	
				}
			}

			if(empty($hotel_data) && in_array('yalago', $apis) ) {
				$result = $this->get_yalago_hotel_by_city_id($aData);
				$hotel_data = $result['HotelList'];
  		    	$hotelInfo = $result['hotelInfo'];	
			}

			if(empty($hotel_data) && in_array('yalogo', $apis) ) {
				$result = $this->get_yalago_hotel_by_city_id($aData);
				$hotel_data = $result['HotelList'];
  		    	$hotelInfo = $result['hotelInfo'];	
			} 

			if(empty($hotel_data) && in_array('expedia', $apis) ) {
				$result = $this->get_hotel_by_city_id_v2($aData);
				$hotel_data = $result['HotelList'];
  		    	$hotelInfo = $result['hotelInfo'];	
			}
			//echo '<pre>'; print_r($hotel_data); die;  

			// COMMENTED OUT BY MIGUEL ON 2017-03-13 1:41 AM HONGKONG SPRINT
			// THIS LINE OF CODE INCREASES THE DEFAULT NIGHTS BASED ON THE NUMBER OF ACTIVITIES RETURNED
		 	
			$all_default_data[] = [
				'activities' => isset($activity_data[0]) ? array($activity_data[0]) : NULL,
				'transport'  => isset($transport_data[0]) ? $transport_data[0]: NULL,
				'hotel'      => isset($hotel_data) ? $hotel_data : NULL,
				'hotelInfo'      => isset($hotelInfo) ? $hotelInfo : NULL,
				'city'       => $city_data 
			];
		
		}
		//echo "<pre>";print_r($all_default_data);exit;
		return \Response::json(['status'=>200,'data'=>$all_default_data]);
	}
	
	public function findAllDefaultTest(){
		$data   = Input::all();
		$data = '{"city_ids":["364","7"],"date_from":"2018-09-26","auto_populate":"1","traveller_number":"1","child_number":"0","search_input":{"_token":"Bmlcbz15yN1Oxcrhie7jeiPFQH7t55YgThz4fQFF","country":"605","city":"364","to_country":"513","to_city":"7","auto_populate":"1","countryId":null,"countryRegion":null,"countryRegionName":null,"option":"auto","searchValType2":null,"searchVal2":null,"start_location":"Delhi, India","end_location":"Sydney, Australia","start_date":"2018-09-26","rooms":"1","travellers":"1","total_children":"0","num_of_pax":["1"],"firstname":{"1":{"1":"irfan"}},"lastname":{"1":{"1":"Bagwala"}},"dob":{"1":{"1":"03-05-1985"}},"age":{"1":{"1":"33"}},"child":[],"interests":[],"travel_preferences":null,"num_of_adults":[1],"num_of_children":[0],"pax":[[{"firstname":"irfan","lastname":"Bagwala","dob":"03-05-1985","age":"33","child":0}]]},"rooms":"1","child":[],"pax_information":[],"apis":["static_accomodation","static_transport","static_activity","expedia","mystifly"]}';
		$data = json_decode($data,true);
		//echo "<pre>";print_r($data);exit;

		$aData = $data;
		//changes bcz data pas in different way ..i confirmed with rekha mam << priya
		$hotel_category_id = isset($data['search_input']['travel_preferences'][0]['accommodation'][0]) ? $data['search_input']['travel_preferences'][0]['accommodation']:[] ;
		$transport_types   = isset($data['search_input']['transport_types']) ? $data['search_input']['transport_types']:[];
		$interests         = isset($data['search_input']['interests']) ? array_filter($data['search_input']['interests']):[];
		$room_type_id      = isset($data['search_input']['travel_preferences'][0]['room'][0]) ? array_filter( is_string( $data['search_input']['travel_preferences'][0]['room'][0]) ? explode( ",", $data['search_input']['travel_preferences'][0]['room'][0]) : $$data['search_input']['travel_preferences'][0]['room'][0]  ):[];
		$gender			   = isset($data['search_input']['gender']) ? $data['search_input']['gender']:null;
		$date              = Carbon::parse($data['date_from'])->format('Y-m-d');

		$all_default_data  = []; 
		$total_price       = [];
		Input::merge(['hotel_category_id' => $hotel_category_id ]);
		Input::merge(['transport_types' => $transport_types ]);
		Input::merge(['interests' => $interests]);
		Input::merge(['room_type_id' => $room_type_id]);
		Input::merge(['gender' => $gender]);


		$aData['hotel_category_id'] = $hotel_category_id;
		$aData['transport_types'] = $transport_types;
		$aData['provider'] = 'eroam';
		$aData['interests'] = $interests;
		$aData['room_type_id'] = $room_type_id;
		$aData['gender'] = $gender;
		$aData['rooms'] = $data['rooms'];


		$prev_city_default_night = 0;


        if (!empty($data['apis'])) {
            $apis = !empty($data['apis']) ? $data['apis'] : '';
        }else{
            $domain = request()->header('domain');
            $apis = getInventory($domain);
            $apis = $apis['inventory'];
            foreach ($apis as $api) {
                $apiFinal[] = $api->inventory_config->slug;
            }
            $apis = $apiFinal;
        }

		foreach ($data['city_ids'] as $key => $value)
		{		
			$activity_data           = [];
			$data['date_from']       = Carbon::parse($date)->addDays($prev_city_default_night)->format('Y-m-d');			
			$date                    = $data['date_from'];
			$city_data               = City::with('latlong','timezone', 'ae_city_mapped','hb_destinations_mapped', 'viator_locations_mapped', 'aot_supplier_locations_mapped','country','iata','iatas','image')->where('id',$value)->first();
			$prev_city_default_night = $city_data['default_nights'];

			$city = City::select('id','name','country_id')->with('Country')->where('id',$value)->first();
			
			Input::merge(['searchCityName' => $city->name]);
			Input::merge(['searchCityId' => $value]);
			Input::merge(['searchCountryCode' => $city['Country']['code']]);

			$aData['searchCityName'] = $city->name;
			$aData['searchCityId'] = $value;
			$aData['searchCountryCode'] = $city['Country']['code'];
			
			if( $data['auto_populate'] ){

				Input::merge(['city_ids' => [$value]]);
				Input::merge(['date_to' => $data['date_from']]);
				Input::merge(['date_from' => $data['date_from']]);

				$aData['city_ids'] = $value;
				$aData['date_to'] = $data['date_from'];
				$aData['date_from'] = $data['date_from'];

				if(in_array('static_activity', $apis)) {
					// $result = $this->get_all_activity_by_city_id($aData);
	  		    	// $activity_data = $result->getData()->data;
					$result = [];
					$activity_data = [];
				} else {
					$result = [];
					$activity_data = [];
				}

				
				if(count($activity_data) > 0)
				{
					foreach ($activity_data as $k => $activity)
					{
						
						$activity->price = [];
						foreach ($activity->activity_price as $element => $price)
						{	
							if( strtotime($price->date_from) < strtotime($date) && strtotime($price->date_to) > strtotime($date) )
							{
								array_push( $activity->price, $price );
							}
						}
						$activity->activity_price = $activity->price;

						$activity_data = array_filter($activity_data);	
					}
				}
		   	}

			if(count($data['city_ids']) > 1 && isset($data['city_ids'][$key+1]))
			{
				Input::merge(['date_to' => $data['date_from']]);
				Input::merge(['from_city_id' => $value]);
				Input::merge(['to_city_id' => $data['city_ids'][$key+1]]);
				Input::merge(['day' => 0]);
				Input::merge(['date_from' => $data['date_from']]);
				
				$aData['date_to'] = $data['date_from'];
				$aData['from_city_id'] =  $value;
				$aData['to_city_id'] = $data['city_ids'][$key+1];
				$aData['day'] = 0;
				$aData['date_from'] = $data['date_from'];
				
				$result = array();
				$transport_data = array();
				$preferences = isset($data['search_input']['travel_preferences']) ? $data['search_input']['travel_preferences'] : array();
				if(isset($preferences[0]['CabinClasses']) && $preferences[0]['CabinClasses'] != ''){
                    $CabinClasses = $preferences[0]['CabinClasses'];
                }else{
                    $CabinClasses = 'Y';
                }
                if(in_array('static_transport', $apis)) {
					if(isset($preferences[0]['transport'][0])){
						if($preferences[0]['transport'][0] == 2  && $CabinClasses == 'Y'){
							$result = $this->get_all_transport_by_city_ids($aData);
	  		         		$transport_data = $result->getData()->data;
	  		        		/*$result = array();
							$transport_data = array();*/
						}else{
							$result = array();
							$transport_data = array();
						}
					}else{
						$result = $this->get_all_transport_by_city_ids($aData);
	  		        	$transport_data = $result->getData()->data;
	  		        	/*$result = array();
						$transport_data = array();*/
					}
                } else {
                	$result = array();
					$transport_data = array();
                }
											  	
				if(count($transport_data) > 0)
				{
				 	foreach ($transport_data as $k => $transport)
				 	{
						foreach ($transport->price as $element => $price)
						{	
							// EDITED BY MIGUEL ON 2017-03-28
							// IF season start date is beyond the selected date or the season end date is behind the selected date THEN unset the season/price
							if( strtotime($price->from) > strtotime($date) || strtotime($price->to) < strtotime($date) )
							{
								unset($transport_data[$k]->price[$element]);
								
							}
						}
						// EDITED BY MIGUEL ON 2017-03-28
						// IF the transport does not have prices THEN unset the transport
						if( count( $transport_data[$k]->price ) < 1 )
						{
						   unset( $transport_data[$k] );
						}

						if(count($transport_types) > 0  && $transport->transport_type_id == $transport_types[0]){
							$transport_data[0] = $transport;
							break;
						}

						if(empty($transport_types) && $transport->transport_type_id == 2 ){ //default coach or minivan
							$transport_data[0] = $transport;
							break;
						}



			  		}	
			    }														  
			}
			Input::merge(['city_default_night' => $prev_city_default_night]);

			$aData['city_default_night'] = $prev_city_default_night;

			if(in_array('static_accomodation', $apis)) {
				$hotel_data = $this->hotel_by_city_id_v2($aData);
				$hotel_data = $hotel_data->getData()->data;
				if (!empty($hotel_data)) {
					$hotel_data = json_decode(json_encode($hotel_data[0]), True);
				}else{
					$hotel_data = [];
				}
			} else if(in_array('expedia', $apis)) {
				$result = $this->get_hotel_by_city_id_v2($aData);
				$hotel_data = $result['HotelList'];
  		    	$hotelInfo = $result['hotelInfo'];	
			} else {
				$hotel_data = [];
  		    	$hotelInfo = [];	
			}
			
			//$hotel_data = [];
  		    //$hotelInfo = [];	  


			// COMMENTED OUT BY MIGUEL ON 2017-03-13 1:41 AM HONGKONG SPRINT
			// THIS LINE OF CODE INCREASES THE DEFAULT NIGHTS BASED ON THE NUMBER OF ACTIVITIES RETURNED
		 	
			$all_default_data[] = [
				'activities' => isset($activity_data[0]) ? array($activity_data[0]) : NULL,
				'transport'  => isset($transport_data[0]) ? $transport_data[0]: NULL,
				'hotel'      => isset($hotel_data) ? $hotel_data : NULL,
				'hotelInfo'      => isset($hotelInfo) ? $hotelInfo : NULL,
				'city'       => $city_data 
			];
		
		}
		//echo "<pre>";print_r($all_default_data);exit;
		return \Response::json(['status'=>200,'data'=>$all_default_data]);
	}
	public function hotel_by_city_id_v2($data)
	{	//echo "<pre>";print_r($data);exit;
		//$data      = Input::all();
		//$data['city_ids'] = array(772);
		//$data['date_from'] = '2017-11-30';
		//$data['date_from'] = '2017-12-02';
		//$data['traveller_number'] = '1';
		//dd($data);
		//$data = ($data =='') ? Input::all() : '';
		$sSearchInput = $data['search_input'];
		$aSum = array();
		for($i=0;$i<count($sSearchInput['num_of_adults']);$i++) {
		  $aSum[$i] = $sSearchInput['num_of_adults'][$i]+$sSearchInput['num_of_children'][$i];
		}
		$nValue = max($aSum);

		$hotel_ids = HotelPrice::where('allotment','>=',$data['search_input']['rooms'])->where('max_pax','>=',$nValue)->groupBy('hotel_id')->pluck('hotel_id');
		//dd($hotel_ids);
		$price_ids = HotelPrice::where('allotment','>=',$data['search_input']['rooms'])->where('max_pax','>=',$nValue)->pluck('id');
		$hotels    = Hotel::whereIn('id', $hotel_ids);
		
        //dd($hotels);
		// if(isset($data['hotel_category_id']) && count($data['hotel_category_id']) > 0){
		// 	$hotels = $hotels->whereIn('hotel_category_id',$data['hotel_category_id']);
		// }
		if(!isset($data['room_type_id']) || count($data['room_type_id']) == 0 ){
			$room_type = [];
			switch ($data['traveller_number']) {//check the number of traveller then set default room type
				case '1':
					$room_type = HotelRoomType::where('pax',1)->orWhere('is_dorm',1)->get();
					break;
				case '2':
					$room_type = HotelRoomType::where('pax',2)->orWhere('is_dorm',1)->get();
					break;
				case '3':
					$room_type = HotelRoomType::where('pax',3)->orWhere('is_dorm',1)->get();
					break;
				default:
					$room_type = HotelRoomType::where('pax','>=',$data['traveller_number'])->get();
					break;	
			}

			if($room_type){
				foreach ($room_type as $key => $value) {
					if(!isset($data['gender'])){
						
						$data['room_type_id'][] = $value['id'];//filter for form exclude dorm requires female only
					}
					
					if(isset($data['gender']) && strtolower($data['gender']) =='male' && $value['female_only'] == 0){
						
						$data['room_type_id'][] = $value['id'];//filter for form exclude dorm requires female only
					}

					if(isset($data['gender']) && strtolower($data['gender']) =='female'){
						
						$data['room_type_id'][] = $value['id'];//filter for form exclude dorm requires female only
					}
				}	
			}
			
			
		}
		//echo "<pre>";print_r($price_ids);exit;
		$aRoomTypeId = $data['room_type_id'];
		
		if($sSearchInput['total_children'] && $sSearchInput['total_children'] > 0)
			$hotels = $hotels->where('child_allowed',1);	

		if(isset($data['hotel_category_id'][0]) && count($data['hotel_category_id']) > 0)
			$hotels = $hotels->where('star_rating','=',$data['hotel_category_id'][0]);	

		$hotels = $hotels->orderBy('hotel_category_id','asc');	

		
		$nCityId = $data['city_ids'];
		if(!is_array($nCityId))
			$nCityId = explode(',',$nCityId);
		
		$hotels = $hotels->whereIn('city_id', $nCityId)
						->where('zhotel.is_publish','=', 1)
						//->where('zhotel.id','=', 11616)
						->orderBy('ranking', 'ASC')
						->with(['city', 'currency', 'image', 'category',
							'price' => function($query) use ($price_ids,$aRoomTypeId)
							{
								$query->whereIn('zhotelprices.id', $price_ids)
									->with(['base_price_obj','markup_obj',
										'room_type' => function($query)
										{
											if (!empty($aRoomTypeId)) {
											    $query->whereIn('id', $aRoomTypeId);
											}
										},
										'season' => function($query)
										{
											$query->with('currency', 'supplier');
										}
									]);
							}
						])->get();

		//echo "<pre>";print_r($hotels);exit;
		$result = array();
		foreach($hotels as $hotel_key => $hotel)
		{	$temp_price = [];
			$price_count = count($hotel->price);
			$hotel->provider = 'eroam';

			if( $hotel->image == NULL ) // if image is empty then append uploads/no-image.png
			{
				array_push($hotel->image, 'uploads/no-image.png');
			}
			foreach($hotel->price as $price_key => $price)
			{	
			
				
				if(!isset($price->season->hotel_supplier_id) || $price->season->hotel_supplier_id != $hotel->default_hotel_supplier_id)
				{

					$price_count--;

					unset($hotel->price[$price_key]); 
					// remove price/room if the supplier is not the hotel's default supplier
				}else{
					//filter season jayson added
					if((strtotime($price->season->from) > strtotime($data['date_from']) && strtotime($price->season->to) > strtotime($data['date_from'])) || (strtotime($price->season->from) < strtotime($data['date_from']) && strtotime($price->season->to) < strtotime($data['date_from']))  ){
						
						$price_count--;
						unset($hotel->price[$price_key]); 
						
					}
					// else if(isset($data['traveller_number']) && $price['room_type']['pax'] < $data['traveller_number']){

					// 	$price_count--;
					// 	unset($hotel->prices[$price_key]); 
					// }
					else if(isset($data['room_type_id']) &&  count($data['room_type_id']) > 0 && !in_array($price->hotel_room_type_id, $data['room_type_id'])){
						$price_count--;
						unset($hotel->price[$price_key]);

					}else{

							array_push($temp_price, $price); 
							// $temp_price[] = $price;
	
					}

				}



				// added property to determine if filter hotel_room_type_id is set;
				if(!empty($data['room_type_id']))
				{
					$price->is_room_type_filtered = ($data['room_type_id'] == $price->hotel_room_type_id) ? 1: 0;
				}
				else
				{
					$hotel->is_room_type_filtered = 0;
				}
			}

			if($price_count > 0) // remove hotel if there is no price/room
			{   unset($hotel->price) ;
				$hotel->price =  $temp_price;
				array_push($result, $hotel);

			}
			// added property to determine if filter hotel_category_id is set;
			if(!empty($data['category_id']))
			{
				$hotel->is_category_filtered = ($data['category_id'] == $hotel->hotel_category_id) ? 1: 0;
			}
			else
			{
				$hotel->is_category_filtered = 0;
			}
			
		}
		//echo "<pre>";print_r($result);exit;
		return \Response::json([
			'status' => 200,
			'data' => $result
		]);
	}

	public function getActivity(){
		$data = Input::all();	
		$activity_data = ActivityNew::with('price')->where('id',$data['id'])->first();
		return \Response::json(['status'=>200,'data'=>$activity_data]);
	}

    public function get_transport_types()
	{
		$transport_type =  TransportType::where('transport_mode', 'single')->orderBy('sequence')->get();
		return \Response::json(['status'=>200,'data'=>$transport_type]);
	}

	public function get_room_types()
	{
		$room_types =  HotelRoomType::orderBy('sequence')->get();
		return \Response::json(['status'=>200,'data'=>$room_types]);
	}

	public function viewEroamHotel($id){
		$data = Input::all();
		
		$aSumRoomWisePassenger = array();
	    for($i=0;$i<count($data['num_of_adults']);$i++) {
	      $aSumRoomWisePassenger[$i] = $data['num_of_adults'][$i]+$data['num_of_children'][$i];
	    }
	    $nMaxPax = max($aSumRoomWisePassenger);

		$price_ids = HotelPrice::where('allotment','>=',$data['rooms'])->where('max_pax','>=',$nMaxPax)->pluck('id');
		$data  = Hotel::with(['city', 'currency', 'image', 'category',
					'price'=> function($query) use($price_ids)
					{
						$query->whereIn('zhotelprices.id', $price_ids)
						->with(['room_type',
							'season' => function($query)
							{
								$query->with('currency', 'supplier');
							}
						]);
					}])->where('id',$id)->first();
		return \Response::json(['status'=>200,'data'=>$data]);
	}

	public function viewEroamActivity($id){
		$data  = ActivityNew::with(['City', 'Currency', 'image', 'activity_price'])->where('id',$id)->first();
		return \Response::json(['status'=>200,'data'=>$data]);
	}

	public function viewAOTHotel($code){
		$data = AotSupplier::where('SupplierCode',$code)->first();
		return \Response::json(['status'=>200,'data'=>$data]);
	}

	public function get_all_activity_by_city_id($data)
	{	
		$activity_ids = ActivityPrice::where('allotment', '>', '0')->pluck('activity_id');

		$price_ids = ActivityPrice::where('allotment','>','0')->pluck('id');

		$activities = ActivityNew::where('city_id', $data['city_ids'])
			->where('zactivity.is_publish','=', 1)
			->where('zactivity.duration', '=', 1)
			->whereIn('zactivity.id', $activity_ids)
			->with(['City', 'Currency', 'image', 
				'Pivot' => function($query) use ($data)
				{
		         	if(!empty($data['interests'])){
		         		
		          	}
			   	}, 
				'activity_price' => function($query) use ($price_ids)
				{
					$query->with('currency');
					$query->whereIn('zactivityprices.id', $price_ids);
				}
			])
		    ->orderBy('default')
		    ->limit(1)->get();

		$result = array();

		foreach($activities as $activity_key => $activity)
		{	
			if( count( $activity->image ) < 1 )
			{
			} else {

				$price_count = count($activity->activity_price);
				
				$temp_price = [];			
				foreach($activity->activity_price as $price_key => $price)
				{

					if($price->activity_supplier_id != $activity->default_activity_supplier_id)
					{
						//unset($activity->activity_price[ $price_key ]); // remove price/room if the supplier is not the activity's default supplier
					}else{
						//array_push($temp_price, $price);
						//filter season jayson added
						if( (strtotime($price->date_from) > strtotime($data['date_from']) && strtotime($price->date_to) > strtotime($data['date_from'])) || (strtotime($price->date_from) < strtotime($data['date_from']) && strtotime($price->date_to) < strtotime($data['date_from'])) ){
							
							$price_count--;
							unset($activity->activity_price[$price_key]); 
							
						}else{
							array_push($temp_price, $price);
						}

					
					}
				}
				//addition filter for interest
				$label_count = 0;
				if(count($data['interests']) > 0)
				{
					foreach($activity->pivot as $key => $label)
					{
						if(in_array($label->label_id, $data['interests']))
						{
							$label_count = ++$label_count;
						}
					}
				}else{
					$label_count = 1;
				}	
				//end
				
				if($price_count > 0 && $label_count > 0 ){
					unset($activity->activity_price) ;
					$activity->activity_price =  $temp_price;
					array_push($result, $activity);

				}	
			}
		}

		return \Response::json([
			'status' => 200,
			'data' => $result
		]); 
	}

	public function get_all_transport_by_city_ids($data)
	{
		$from_city_id          = $data['from_city_id'];
		$to_city_id            = $data['to_city_id'];
		$date_from             = $data['date_from'];
		$date_to               = $data['date_to'];
		$transport_types 	   = $data['transport_types'];
		$day                   = $data['day'];
		$provider			   = $data['provider'];		
		$transport_etd_is_null = Transport::whereNull('etd')->orWhere(['etd' => ""])->pluck('id');
		$transport_eta_is_null = Transport::whereNull('eta')->orWhere(['eta' => ""])->pluck('id');
		

		$transport_ids = TransportPrice::where('allotment','>','0')
		    ->whereRaw("(((ztransportprices.from <= '".$date_from."') AND (ztransportprices.to >= '".$date_from."')) AND ((ztransportprices.from <= '".$date_to."') AND (ztransportprices.to >= '".$date_to."')))")
		    ->pluck('transport_id');
		   
		$price_ids     = TransportPrice::where('allotment','>','0')
		    ->whereRaw("(((ztransportprices.from <= '".$date_from."') AND (ztransportprices.to >= '".$date_from."')) AND ((ztransportprices.from <= '".$date_to."') AND (ztransportprices.to >= '".$date_to."')))")
		    ->pluck('id');
		    
		$transports = Transport::whereIn('ztransports.id', $transport_ids);


		if(isset($transport_types) && count($transport_types) > 0){
			$transports = $transports->whereIn('transport_type_id',$transport_types);
		}


		$transports = $transports->where([
				'from_city_id' => $from_city_id,
				'to_city_id' => $to_city_id 
				])
			->where('ztransports.is_publish','=', 1)
		    ->whereNotIn('id', $transport_etd_is_null)
		    ->whereNotIn('id', $transport_eta_is_null)
            ->with(['from_city', 'to_city', 'supplier', 'operator', 'transport_type', 'currency','transporttype',
				'price' => function($query) use ($price_ids)
				{
					$query->whereIn('ztransportprices.id', $price_ids);
				}

			])->get();

		$result = array();
		$suppliers = array_column($transports->toArray(), 'default_transport_supplier_id');
		$transport_supplier = TransportSupplier::whereIn('id',$suppliers)->get();
		$transport_supplier_name = array_column($transport_supplier->toArray(), 'name');

		foreach($transports as $transport_key => $transport)
		{	
			$price_count = count($transport->price);
			foreach($transport->price as $price_key => $price)
			{
				if($price->transport_supplier_id != $transport->default_transport_supplier_id)
				{
					$price_count--;
					unset($transport->price[$price_key]); // remove price/room if the supplier is not the hotel's default supplier
				}

				if( $price->price == "0.0" )
				{
					$price_count--;
					unset($transport->price[$price_key]); // remove price/room if the supplier is not the hotel's default supplier
				}

			}

			if($price_count > 0) // remove hotel if there is no price/room
			{
				$transport->day = $day;
				array_push($result, $transport);
			}
		}
		
		if (!empty($result)) {
			$domain = request()->header('domain');
	        if ($domain) {
	            $commissions = getDomainData($domain);
            	if ($commissions['pricing'][0]['status'] == 0) {
		            $result = updateTransportPricing($result,'transport',$transport_supplier_name,$commissions['eroam'],$commissions['pricing']);
		        }
	        }
		}

		return \Response::json([
			'status' => 200,
			'data' => $result
		]);
	}

	public function hotel_by_city_id()
	{	
		$data      = Input::all();
		$hotel_ids = HotelPrice::where('allotment','>','0')->pluck('hotel_id');//dd($hotel_ids);
		$price_ids = HotelPrice::where('allotment','>','0')->pluck('id');
		$hotels    = Hotel::whereIn('id', $hotel_ids);
		$data['traveller_number'] = 1;
		if(!isset($data['room_type_id']) || count($data['room_type_id']) == 0 ){
			$room_type = [];
			switch ($data['traveller_number']) {
				case '1':
					$room_type = HotelRoomType::where('pax',1)->orWhere('is_dorm',1)->get();
					break;
				case '2':
					$room_type = HotelRoomType::where('pax',2)->orWhere('is_dorm',1)->get();
					break;
				case '3':
					$room_type = HotelRoomType::where('pax',3)->orWhere('is_dorm',1)->get();
					break;
				default:
					$room_type = HotelRoomType::where('pax','>=',$data['traveller_number'])->get();
					break;	
			}

			if($room_type){
				foreach ($room_type as $key => $value) {
					if(!isset($data['gender'])){
						
						$data['room_type_id'][] = $value['id'];//filter for form exclude dorm requires female only
					}
					
					if(isset($data['gender']) && strtolower($data['gender']) =='male' && $value['female_only'] == 0){
						
						$data['room_type_id'][] = $value['id'];//filter for form exclude dorm requires female only
					}

					if(isset($data['gender']) && strtolower($data['gender']) =='female'){
						
						$data['room_type_id'][] = $value['id'];//filter for form exclude dorm requires female only
					}
				}	
			}
			
			
		}

		
		$hotels = $hotels->orderBy('id','asc');	

		

		$data['city_ids'] = array();
		$hotels = $hotels->whereIn('city_id', $data['city_ids'])
			->where('zhotel.approved_by_eroam','=', 1)
			->orderBy('id', 'ASC')
			->with(['city', 'currency', 'image', 'category',
				'price' => function($query) use ($price_ids)
				{
					$query->whereIn('zhotelprices.id', $price_ids)
						->with(['room_type',
							'season' => function($query)
							{
								$query->with('currency', 'supplier');
							}
						]);
				}
			])->get();

		
		$result = array();
		foreach($hotels as $hotel_key => $hotel)
		{	$temp_price = [];
			$price_count = count($hotel->price);

			if( $hotel->image == NULL ) // if image is empty then append uploads/no-image.png
			{
				array_push($hotel->image, 'uploads/no-image.png');
			}
			foreach($hotel->prices as $price_key => $price)
			{	
			
				
				if($price->season->hotel_supplier_id != $hotel->default_hotel_supplier_id)
				{

					$price_count--;

					unset($hotel->price[$price_key]); 
				}else{
					//filter season jayson added
					if((strtotime($price->season->from) > strtotime($data['date_from']) && strtotime($price->season->to) > strtotime($data['date_from'])) || (strtotime($price->season->from) < strtotime($data['date_from']) && strtotime($price->season->to) < strtotime($data['date_from']))  ){
						
						$price_count--;
						unset($hotel->price[$price_key]); 
						
					}
					
					else if(isset($data['room_type_id']) &&  count($data['room_type_id']) > 0 && !in_array($price->hotel_room_type_id, $data['room_type_id'])){
						$price_count--;
						unset($hotel->price[$price_key]);

					}else{
							array_push($temp_price, $price); 
	
					}

				}

				// added property to determine if filter hotel_room_type_id is set;
				if(!empty($data['room_type_id']))
				{
					$price->is_room_type_filtered = ($data['room_type_id'] == $price->hotel_room_type_id) ? 1: 0;
				}
				else
				{
					$hotel->is_room_type_filtered = 0;
				}
			}

			if($price_count > 0) // remove hotel if there is no price/room
			{   unset($hotel->price) ;
				$hotel->price =  $temp_price;
				array_push($result, $hotel);

			}
			// added property to determine if filter hotel_category_id is set;
			$hotel->is_category_filtered = 0;
			// if(!empty($data['category_id']))
			// {
			// 	$hotel->is_category_filtered = ($data['category_id'] == $hotel->hotel_category_id) ? 1: 0;
			// }
			// else
			// {
			// 	$hotel->is_category_filtered = 0;
			// }
			
		}

		return \Response::json([
			'status' => 200,
			'data' => $result
		]);
	}

	public function get_hotel_by_city_id_v2($data = [])
	{	
		$result = Expedia::getHotels($data);//For expedia
		//echo $data['searchCityName'].'<pre>'; print_r($result);

		$lowest_price = array();
        if(!empty($result)){
        	$result =json_decode($result, TRUE);
        	 if(isset($result['HotelListResponse']['HotelList']['HotelSummary']) && !empty($result['HotelListResponse']['HotelList']['HotelSummary'])){
        	 	//$HotelList = $result['HotelListResponse']['HotelList']['HotelSummary'];
        	 	
        	 	if(!isset($result['HotelListResponse']['HotelList']['HotelSummary'][0])){
        	 		$HotelSummary['0'] = $result['HotelListResponse']['HotelList']['HotelSummary'];
        	 		$HotelList = $HotelSummary;
        	 	}else{
        	 		$HotelList = $result['HotelListResponse']['HotelList']['HotelSummary'];
        	 	}
        	 	foreach ($HotelList as $key => $value) {
        	 		//echo $value['hotelId']."<br>";
        	 		foreach ($value as $key1 => $value1) {
        	 			if($key1 == 'RoomRateDetailsList'){
        	 				foreach ($value1['RoomRateDetails'] as $key2 => $value2) {
        	 					if($key2 == 'RateInfos'){
        	 						foreach ($value2['RateInfo'] as $key3 => $value3) {
        	 							if($key3 == 'ChargeableRateInfo'){
        	 								$total = '@total';
        	 								$lowest_price[$key] = $value3[$total];
        	 							}
        	 						}
        	 					}
        	 				}
        	 			}
        	 		}
        	 	}
        	}

        	//echo $data['searchCityName'].'<pre>'; print_r($lowest_price);
        	//$min_array = array_keys($lowest_price, min($lowest_price));
        	$min_array = $this->checkExpediaAvaibility($lowest_price,$HotelList,$data,0);
			//echo $data['searchCityName'];
        	//echo '<pre>'; print_r($min_array);  //die;

        	if(!empty($min_array) && isset($min_array[1])){
        		$tmp_data = $result['HotelListResponse'];
	        	 $HotelList[$min_array[0]]['provider'] = 'expedia';
	        	 $HotelList[$min_array[0]]['customerSessionId'] = $tmp_data['customerSessionId'];
	        	 $HotelList[$min_array[0]]['cacheKey'] = $tmp_data['cacheKey'];
	        	 $HotelList[$min_array[0]]['cacheLocation'] = $tmp_data['cacheLocation'];  
	        	 $HotelList[$min_array[0]]['RoomRateDetailsList']['RoomRateDetails']  = isset($min_array[1]) ? $min_array[1] : 'rekha123';

	        	 $hotel_request_data = Input::All();

	        	 $hotelInfo['provider'] = 'expedia';
	        	 $hotelInfo['customerSessionId'] = $tmp_data['customerSessionId'];
	        	 $hotelInfo['cacheKey'] = $tmp_data['cacheKey'];
	        	 $hotelInfo['cacheLocation'] = $tmp_data['cacheLocation']; 
	        	 $hotelInfo['nights'] = $hotel_request_data['city_default_night']; 
	        	 $hotelInfo['checkin'] =  date('Y-m-d', strtotime($hotel_request_data['date_from']));
	        	 $hotelInfo['checkout'] = date('Y-m-d', strtotime($hotel_request_data['date_from'].'+'.$hotel_request_data['city_default_night'].'Days')); 

				 //$result = $result['HotelListResponse']['HotelList']['HotelSummary'][$min_array[0]];
				 $return_data = array();
				 $return_data['HotelList'] = $HotelList[$min_array[0]];
				 $return_data['hotelInfo'] = $hotelInfo;
				 $result = $return_data;
        	} else {
        		$tmp_data = $result['HotelListResponse'];
				$hotel_request_data = Input::All();

				 $hotelInfo['provider'] = 'expedia';
	        	 $hotelInfo['customerSessionId'] = $tmp_data['customerSessionId'];
	        	 $hotelInfo['cacheKey'] = $tmp_data['cacheKey'];
	        	 $hotelInfo['cacheLocation'] = $tmp_data['cacheLocation']; 
	        	 $hotelInfo['nights'] = $hotel_request_data['city_default_night']; 
	        	 $hotelInfo['checkin'] =  date('Y-m-d', strtotime($hotel_request_data['date_from']));
	        	 $hotelInfo['checkout'] = date('Y-m-d', strtotime($hotel_request_data['date_from'].'+'.$hotel_request_data['city_default_night'].'Days')); 

				$return_data = array();
				$return_data['HotelList'] = NULL;
				$return_data['hotelInfo'] = $hotelInfo;
				$result = $return_data;
        	}

     	}else{
     		 $return_data = array();
			 $return_data['HotelList'] = NULL;
			 $return_data['hotelInfo'] = NULL;
			 $result = $return_data;
     	}

     	$domain = request()->header('domain');
        if ($domain) {
            $commissions = getDomainData($domain);
            if ($commissions['pricing'][0]['status'] == 0) {
	            $result = updateAccomodationPricing($result,'accomodation','expedia',$commissions['eroam'],$commissions['pricing']);
    	    }
        }

        return $result;
	}

	public function get_yalago_hotel_by_city_id($data = [])
	{	
		//$data= '{"city_ids":"394","date_from":"2018-10-26","auto_populate":"1","traveller_number":"4","child_number":"0","search_input":{"_token":"XeQ9Zdpt40ZLpSwZrhXRwQ2yXyUvzJ25D79xhfaI","country":null,"city":null,"to_country":"575","to_city":"394","auto_populate":"1","countryId":null,"countryRegion":null,"countryRegionName":null,"option":"manual","searchValType2":null,"searchVal2":null,"start_location":null,"end_location":"Paris, France","start_date":"2018-10-26","rooms":"2","travellers":"4","total_children":"0","num_of_pax":["2","2"],"is_child":{"1":{"1":"0"},"2":{"1":"0"}},"firstname":{"1":{"1":"rekha","2":"dhara"},"2":{"1":"priya","2":"chintan"}},"lastname":{"1":{"1":"test","2":"test"},"2":{"1":"test","2":"test"}},"dob":{"1":{"1":"03-05-1985","2":"03-05-1985"},"2":{"1":"03-05-1985","2":"03-05-1985"}},"age":{"1":{"1":"33","2":"33"},"2":{"1":"33","2":"33"}},"child":[],"interests":[],"travel_preferences":null,"num_of_adults":[2,2],"num_of_children":[0,0],"pax":[[{"firstname":"rekha","lastname":"test","dob":"03-05-1985","age":"33","child":0},{"firstname":"dhara","lastname":"test","dob":"03-05-1985","age":"33","child":0}],[{"firstname":"priya","lastname":"test","dob":"03-05-1985","age":"33","child":0},{"firstname":"chintan","lastname":"test","dob":"03-05-1985","age":"33","child":0}]]},"rooms":"2","child":[],"pax_information":[],"apis":["static_accomodation","static_transport","static_activity","expedia","yalogo","mystifly","busbud","viator"],"hotel_category_id":[],"transport_types":[],"provider":"eroam","interests":[],"room_type_id":[],"gender":null,"searchCityName":"Paris","searchCityId":"394","searchCountryCode":"FR","date_to":"2018-10-25","city_default_night":"3"}';
		//$data = '{"city_ids":"394","date_from":"2018-11-17","auto_populate":"1","traveller_number":"5","child_number":"3","search_input":{"_token":"qPc24Mc8wdKVDShxz6P3VZdyHvYQvIN4FZoFvxfo","country":null,"city":null,"to_country":"575","to_city":"394","auto_populate":"1","countryId":null,"countryRegion":null,"countryRegionName":null,"option":"manual","searchValType2":null,"searchVal2":null,"start_location":null,"end_location":"Paris, France","start_date":"2018-10-17","rooms":"3","travellers":"5","total_children":"3","num_of_pax":["2","3","3"],"is_child":{"1":{"1":"0"},"2":{"1":"0","2":"1","3":"1"},"3":{"1":"0","3":"1"}},"firstname":{"1":{"1":"rekha","2":"dhara"},"2":{"1":"priya","2":"chintan","3":"anjali"},"3":{"1":"sahista","2":"mihir","3":"irfan"}},"lastname":{"1":{"1":"test","2":"test"},"2":{"1":"test","2":"test","3":"test"},"3":{"1":"test","2":"test","3":"test"}},"dob":{"1":{"1":"03-05-1985","2":"03-05-1985"},"2":{"1":"03-05-1985","2":"10-06-2014","3":"02-02-2010"},"3":{"1":"03-05-1985","2":"03-05-1985","3":"17-06-2015"}},"age":{"1":{"1":"33","2":"33"},"2":{"1":"33","2":"0","3":"0"},"3":{"1":"33","2":"33","3":"0"}},"child":{"1":["4","8"],"2":["3"]},"interests":[],"travel_preferences":null,"num_of_adults":[2,1,2],"num_of_children":[0,2,1],"pax":[[{"firstname":"rekha","lastname":"test","dob":"03-05-1985","age":"33","child":0},{"firstname":"dhara","lastname":"test","dob":"03-05-1985","age":"33","child":0}],[{"firstname":"priya","lastname":"test","dob":"03-05-1985","age":"33","child":0},{"firstname":"chintan","lastname":"test","dob":"10-06-2014","age":"4","child":1},{"firstname":"anjali","lastname":"test","dob":"02-02-2010","age":"8","child":1}],[{"firstname":"sahista","lastname":"test","dob":"03-05-1985","age":"33","child":0},{"firstname":"mihir","lastname":"test","dob":"03-05-1985","age":"33","child":0},{"firstname":"irfan","lastname":"test","dob":"17-06-2015","age":"3","child":1}]]},"rooms":"3","child":{"1":["4","8"],"2":["3"]},"pax_information":[],"apis":["static_accomodation","static_transport","static_activity","expedia","mystifly","busbud","viator","yalago"],"hotel_category_id":[],"transport_types":[],"provider":"eroam","interests":[],"room_type_id":[],"gender":null,"searchCityName":"Paris","searchCityId":"394","searchCountryCode":"FR","date_to":"2018-11-17","city_default_night":"3"}';
		//$data = '{"city_ids":"394","date_from":"2018-10-27","auto_populate":"1","traveller_number":"5","child_number":"1","search_input":{"_token":"nt0aDV6FPCyitNWp9pRZXUZtSPxRVUH3NP1plxp0","country":null,"city":null,"to_country":"575","to_city":"394","auto_populate":"1","countryId":null,"countryRegion":null,"countryRegionName":null,"option":"manual","searchValType2":null,"searchVal2":null,"start_location":null,"end_location":"Paris, France","start_date":"2018-10-27","rooms":"2","travellers":"5","total_children":"1","num_of_pax":["2","4"],"is_child":{"1":{"1":"0"},"2":{"1":"0","4":"1"}},"firstname":{"1":{"1":"rekha","2":"dhara"},"2":{"1":"priya","2":"chintan","3":"anjali","4":"sdfsdfs"}},"lastname":{"1":{"1":"test","2":"test"},"2":{"1":"test","2":"test","3":"test","4":"test"}},"dob":{"1":{"1":"03-05-1985","2":"03-05-1985"},"2":{"1":"03-05-1985","2":"03-05-1985","3":"03-05-1985","4":"02-02-2010"}},"age":{"1":{"1":"33","2":"33"},"2":{"1":"33","2":"33","3":"33","4":"0"}},"child":{"1":["8"]},"interests":[],"travel_preferences":null,"num_of_adults":[2,3],"num_of_children":[0,1],"pax":[[{"firstname":"rekha","lastname":"test","dob":"03-05-1985","age":"33","child":0},{"firstname":"dhara","lastname":"test","dob":"03-05-1985","age":"33","child":0}],[{"firstname":"priya","lastname":"test","dob":"03-05-1985","age":"33","child":0},{"firstname":"chintan","lastname":"test","dob":"03-05-1985","age":"33","child":0},{"firstname":"anjali","lastname":"test","dob":"03-05-1985","age":"33","child":0},{"firstname":"sdfsdfs","lastname":"test","dob":"02-02-2010","age":"8","child":1}]]},"rooms":"2","child":{"1":["8"]},"pax_information":[],"apis":["static_accomodation","static_transport","static_activity","expedia","yalogo","mystifly","busbud","viator"],"hotel_category_id":[],"transport_types":[],"provider":"eroam","interests":[],"room_type_id":[],"gender":null,"searchCityName":"Paris","searchCityId":"394","searchCountryCode":"FR","date_to":"2018-10-27","city_default_night":"3"}';
		//$data = json_decode($data,true);

		$this->api = New Yalago;
		$result = $this->api->getAllHotels($data);//For Yalago
	
		$lowest_price = array();
        if(!empty($result)){
        	$result =  json_decode($result->content(),TRUE);
        	//echo '<pre>'; print_r($result); die;

        	//unset($result['data']['Establishments']);
			$allHotels = array();
			$return_data = array();
	        $return_data['HotelList'] = NULL;
			
			$hotel_request_data = $data;
        	$hotelInfo['provider'] = 'yalago';
        	$hotelInfo['nights'] = $hotel_request_data['city_default_night']; 
        	$hotelInfo['checkin'] =  date('Y-m-d', strtotime($hotel_request_data['date_from']));
        	$hotelInfo['checkout'] = date('Y-m-d', strtotime($hotel_request_data['date_from'].'+'.$hotel_request_data['city_default_night'].'Days'));

        	if(isset($result['data']['Establishments'])){
        	 	$HotelList = $result['data']['Establishments'];

        	 	foreach ($HotelList as $key => $value) {
        	 		$hotelPrice = array();
					$allHotels[$value['EstablishmentId']] = $value['Rooms'];
        	 		foreach ($value['Rooms'] as $key1 => $value1) {
        	 			if($value1['QuantityAvailable'] != -1 && $value1['QuantityAvailable'] >= $data['rooms']){
	 						foreach ($value1['Boards'] as $key2 => $value2) {
	 							if($value2['RequestedRoomIndex'] == 0){
	 								$hotelPrice[$value['EstablishmentId'].'_'.$key1.'_'.$key2] = $value2['NetCost']['Amount'];
	 							}
	 						}
        	 			}
        	 		}

        	 		if(!empty($hotelPrice)){
	        	 		$minPrice = array_keys($hotelPrice, min($hotelPrice));
	        	 		$lowest_price[$minPrice[0]] = $hotelPrice[$minPrice[0]];
	        	 	}
        	 	}

        		//$lowest_price = array();
        		if(!empty($lowest_price)){
		        	//echo 'low price<pre>'; print_r($lowest_price); die;
		        	$min_key = array_keys($lowest_price, min($lowest_price));
		        	$min_value = $lowest_price[$min_key[0]];
		        	//echo '<pre>'; print_r($min_key); print_r($min_value);
		        	$hotel_ids = explode('_', $min_key[0]); 
		        	$EstablishmentId = $hotel_ids[0];

		        	$RoomCode = $allHotels[$hotel_ids[0]][$hotel_ids[1]]['Code'];
		        	$BoardCode = $allHotels[$hotel_ids[0]][$hotel_ids[1]]['Boards'][$hotel_ids[2]]['Code'];
		        	$QuantityAvailable = $allHotels[$hotel_ids[0]][$hotel_ids[1]]['QuantityAvailable'];
		        	//$roomDetails = $allHotels[$hotel_ids[0]][$hotel_ids[1]];

		        	$rooms = $data['rooms'];
					$i =0;
					$roomPrice = array();
					//echo '<pre>'; print_r($allHotels[$hotel_ids[0]]); 
		        	foreach ($allHotels[$hotel_ids[0]] as $key => $room) { 
		        		$i++;
		        		if($room['QuantityAvailable'] > $rooms){
			        		//echo $i.'==>'.$room['Description'].'//'.$room['QuantityAvailable'].'<br>';
			        		$arr = explode("(", $room['Description'], 2);
			        		$allHotels[$hotel_ids[0]][$key]['Description']= $arr[0];

			        		preg_match_all('!\d+!', $room['Description'], $matches);
							if(!empty($matches[0])){
								$maxAdult = $matches[0][1];
								$allHotels[$hotel_ids[0]][$key]['minAdult'] = trim($matches[0][0]);
								$allHotels[$hotel_ids[0]][$key]['maxAdult'] = trim($matches[0][1]);

								$totalPass = 0;
								for ($i=0; $i < $rooms; $i++) { 
									$adult = $data['search_input']['num_of_adults'][$i];
									$child = 0;
									if(isset($data['child'])){
						            	$child = array_key_exists($i, $data['child']) ? count($data['child'][$i]) : 0;	
						            } 
						            $totalPass = $adult + $child;

						            if($maxAdult >= $totalPass ){
										foreach ($room['Boards'] as $key1 => $board){
											if($board['RequestedRoomIndex'] == 0){
												$roomPrice[$i][$key.'_'.$key1] = $board['NetCost']['Amount'];
											}
										}
									}
								}
							}
						}
		        	}
		        	//echo '<pre>'; print_r($roomPrice); //die;
		        	$roomArray = array();
			        for ($i=0; $i < $rooms; $i++) { 
			            $roomArray[$i]['Adults'] = $data['search_input']['num_of_adults'][$i];

			            if(isset($data['child'])){
			            	$roomArray[$i]['ChildAges'] = array_key_exists($i, $data['child']) ? $data['child'][$i] : array();	
			            } else {
			            	$roomArray[$i]['ChildAges'] = array();
			            }
			            
			            $min_key = array_keys($roomPrice[$i], min($roomPrice[$i]));
		        		$min_value = $roomPrice[$i][$min_key[0]];
		        		$room_ids = explode('_', $min_key[0]); 
		        		//echo '<pre>'; print_r($room_ids);

		        		$roomArray[$i]['RoomName'] = $allHotels[$hotel_ids[0]][$room_ids[0]]['Description'];
			            $roomArray[$i]['BoardName'] = $allHotels[$hotel_ids[0]][$room_ids[0]]['Boards'][$room_ids[1]]['Description'];
		        		$roomArray[$i]['RoomCode'] = $allHotels[$hotel_ids[0]][$room_ids[0]]['Code'];
			            $roomArray[$i]['BoardCode'] = $allHotels[$hotel_ids[0]][$room_ids[0]]['Boards'][$room_ids[1]]['Code'];
			            $roomArray[$i]['RoomBoardPrice'] = $allHotels[$hotel_ids[0]][$room_ids[0]]['Boards'][$room_ids[1]]['NetCost']['Amount'];
			            $roomArray[$i]['RoomBoardCurrency'] = $allHotels[$hotel_ids[0]][$room_ids[0]]['Boards'][$room_ids[1]]['NetCost']['Currency'];
			            $roomArray[$i]['NonRefundable'] = $allHotels[$hotel_ids[0]][$room_ids[0]]['Boards'][$room_ids[1]]['NonRefundable'];

			            if(isset($allHotels[$hotel_ids[0]][$room_ids[0]]['Boards'][$room_ids[1]]['GrossCost'])){
			            	$roomArray[$i]['GrossPrice'] = $allHotels[$hotel_ids[0]][$room_ids[0]]['Boards'][$room_ids[1]]['GrossCost']['Amount'];
			            	$roomArray[$i]['GrossCurrency'] = $allHotels[$hotel_ids[0]][$room_ids[0]]['Boards'][$room_ids[1]]['GrossCost']['Currency'];
			            }
			            $roomArray[$i]['minAdult'] = $allHotels[$hotel_ids[0]][$room_ids[0]]['minAdult'];
			            $roomArray[$i]['maxAdult'] = $allHotels[$hotel_ids[0]][$room_ids[0]]['maxAdult']; 

			            $roomDetails[$i] = $allHotels[$hotel_ids[0]][$room_ids[0]];
			            $roomDetails[$i]['Boards']= $allHotels[$hotel_ids[0]][$room_ids[0]]['Boards'][$room_ids[1]];
			            //$bordSetalls[$i] = $allHotels[$hotel_ids[0]][$room_ids[0]]['Boards'][$room_ids[1]];
			        }

		        	//$roomDetails['Rooms'] = $bordSetalls;
			        //echo '<pre>'; print_r($bordSetalls); print_r($roomArray);//die;
			        //print_r($allHotels[$hotel_ids[0]]);

			        $detailPath = '/hotels/Inventory/GetEstablishment';
			        $yalagoData = array(
			            "EstablishmentId" =>$EstablishmentId,
			            "Languages"=> ['en'] 
			        );

			        $hotels = $this->api->getApiData($yalagoData,$detailPath,'yalago_info');
			        $hotelDetails = json_decode($hotels->content(),TRUE);
			        $hotelDetails = $hotelDetails['data']['Establishment'];

			        $roomSelected['RoomSelected'] = $roomArray;
			        $hotelDetails = array_merge($hotelDetails, $roomSelected);
			        //echo '<pre>'; print_r($hotelDetails); die;

		        	$location = YalagoLocations::getYalagoCity($hotel_request_data['searchCityName'])->toArray();
		        	$hotelDetails = array_merge($hotelDetails,$location[0]);
		        	//echo '<pre>'; print_r($hotelDetails); die;
	        	}
	        }

        	if(!empty($hotelDetails)){
        		$hotelDetails['name'] = $hotelDetails['Title'];
        		$hotelDetails['hotelId'] = $hotelDetails['EstablishmentId'];
        		$hotelDetails['nights'] = $hotel_request_data['city_default_night']; 
	        	$hotelDetails['provider'] = 'yalago';
	        	$hotelDetails['city'] = $hotel_request_data['searchCityName'];
	        	$hotelDetails['countryCode'] = $location[0]['countryCode'];
	        	$hotelDetails['Rooms'] = $roomDetails;
				$return_data['HotelList'] = $hotelDetails;	 
			} 

			$return_data['hotelInfo'] = $hotelInfo;
			$result = $return_data;

     	}else{
     		 $return_data = array();
			 $return_data['HotelList'] = NULL;
			 $return_data['hotelInfo'] = NULL;
			 $result = $return_data;
     	}

     	/*$domain = request()->header('domain');
        if ($domain) {
            $commissions = getDomainData($domain);
            if ($commissions['pricing'][0]['status'] == 0) {
	            $result = updateAccomodationPricing($result,'accomodation','expedia',$commissions['eroam'],$commissions['pricing']);
    	    }
        }*/
        //echo '<pre>'; print_r($result);
        return $result;
	}

	public function checkExpediaAvaibility($lowest_price,$HotelList,$data,$i){
		$i++;
		$min_array = array_keys($lowest_price, min($lowest_price));
		$hotel = $HotelList[$min_array[0]]; 

		$hotelId = $hotel['hotelId'];
		if(isset($hotel['deepLink'])){
			$date_array = $this->convertUrl($hotel['deepLink']);
		}
		$arrivalDate  = $date_array['arrivalDate'];
		$departureDate = $date_array['departureDate'];

		$num_of_rooms = $data['search_input']['rooms'];
       	$hotel_request_data = $data['search_input'];
       	$children = array();
        if(isset($hotel_request_data['child']) && !empty($hotel_request_data['child'])){
        	$children = $hotel_request_data['child'];
        }

        $room_request  = '';
		if($num_of_rooms == 1){
			$room_request .='&room1='.$data['search_input']['num_of_adults'][0];
			if(isset($children[0]) && !empty($children[0])){
				$c_array = implode(',',$children[0]);
				$room_request .=','.count($children[0]).','.$c_array;
			}
		}else{
			for ($i=0;$i<$num_of_rooms;$i++){
				$n = $i+1;
				$room_request .='&room'.$n.'='.$data['search_input']['num_of_adults'][$i];
				if(isset($children[$i]) && !empty($children[$i])){
					$c_array = implode(',',$children[$i]);
					$room_request .=','.count($children[$i]).','.$c_array;
				}
			}
		}
		$room = $room_request;
		$roomTypeCode = '';
		$rateCode = '';
		$rateKey = '';

		if(isset($hotel['RoomRateDetailsList']['RoomRateDetails']['roomTypeCode'])){
			$roomTypeCode = $hotel['RoomRateDetailsList']['RoomRateDetails']['roomTypeCode'];
		} else{
			if(isset($hotel['RoomRateDetailsList']['RoomRateDetails']['RoomType'])){
				$roomCode = '@roomCode';
				$roomTypeCode = $hotel['RoomRateDetailsList']['RoomRateDetails']['RoomType'][$roomCode];
			}
		}

		if(isset($hotel['RoomRateDetailsList']['RoomRateDetails']['rateCode'])){
			$rateCode = $hotel['RoomRateDetailsList']['RoomRateDetails']['rateCode'];
		}

		if(isset($hotel['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['RoomGroup']['Room'][0]['rateKey'])){
			$rateKey = $hotel['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['RoomGroup']['Room'][0]['rateKey'];
		}else{
			if(isset($hotel['RoomRateDetailsList']['RoomRateDetails']['RateInfos'])){
				$rateKey = $hotel['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['RoomGroup']['Room']['rateKey'];
			} 
		}

		$includeDetails = 'true';
		$options = 'HOTEL_DETAILS,ROOM_TYPES,ROOM_AMENITIES,PROPERTY_AMENITIES,HOTEL_IMAGES';
		$latitude = $hotel['latitude'];
		$longitude = $hotel['longitude'];
		$customerSessionId = '';//$hotel['customerSessionId'];

		$response = Expedia::getHotelDetails($hotelId,$arrivalDate,$departureDate,$room,$roomTypeCode,$includeDetails,$options,$rateKey,$latitude,$longitude,$customerSessionId,1,$rateCode);

		//echo '<pre>'; print_r($response);

		if(empty($response)){
			unset($lowest_price[$min_array[0]]);
			if(!empty($lowest_price)){
				return $this->checkExpediaAvaibility($lowest_price,$HotelList,$data,$i);
			}else {
				return array();
			}
		} else {
			if(isset($response->HotelRoomResponse->rateCode)){
		    	$temp_array[0] = $response->HotelRoomResponse;
			    $response->HotelRoomResponse = $temp_array;
		    }

			$lowest_price1 = array();
		    if(isset($response->HotelRoomResponse)){
		    	//echo '<pre>'; print_r($response); //die;
			    foreach($response->HotelRoomResponse as $key => $RoomResponse){
			    	if(isset($response->checkInInstructions)){
				    	$response->HotelRoomResponse[$key]->checkInInstructions = $response->checkInInstructions;
				    }

				    if(isset($response->specialCheckInInstructions)){
				    	$response->HotelRoomResponse[$key]->specialCheckInInstructions = $response->specialCheckInInstructions;
				    }
			    	$total = '@total';
			    	$lowest_price1[$key] = $RoomResponse->RateInfos->RateInfo->ChargeableRateInfo->$total;
			    }
			    $min_array1 = array_keys($lowest_price1, min($lowest_price1));
				$min_array[1] = $response->HotelRoomResponse[$min_array1[0]];
			}
			//echo $i.'==='.$hotelId.'//'.$hotel['name']."<br/>";
			return $min_array;
		}
	}

	public function convertUrl($url){
	    $remove_http = str_replace('http://', '', $url);
	    $split_url = explode('?', $remove_http);
	    $get_page_name = explode('/', $split_url[0]);
	    $page_name = $get_page_name[1];

	    $split_parameters = explode('&', $split_url[1]);

	    for($i = 0; $i < count($split_parameters); $i++) {
	        $final_split = explode('=', $split_parameters[$i]);
	        $split_complete[$final_split[0]] = $final_split[1];
	    }
	    $return_array['arrivalDate'] = $split_complete['standardCheckin'];
	    $return_array['departureDate'] = $split_complete['standardCheckout'];
	    return $return_array;
	}

	public function get_hotels_by_city_id_v2()
	{	
		$data['city'] = 'Perth';
		$data['countryCode'] = 'AU';
		$data['arrivalDate'] = '2018-09-09';
		$data['departureDate'] = '2018-09-11';
		$data['numberOfAdults'] = 1;
		$data['customerSessionId'] = '0ABAAC10-8071-AB29-1642-87F2E19B992C';
		$data['rooms'] = '1';
		$data['search_input'] = array(
			'child' => array(),
			'num_of_adults' => array('0'=>2)
		);

		$data['city'] = array(
			'default_nights' => 2,
			'id' => 30,
			'name' => 'Perth'
		);
		

		//echo "<pre>";print_r($data);exit;
		//$data = Input::all();

		$result = Expedia::getHotels_v2($data);//For expedia
		return \Response::json([
			'status' => 200,
			'data' => $result
		]);
		
	}

	public function get_hotel_details()
	{	

		$hotelId = '505122';
		$arrivalDate = '09-09-2018'; //m-d-Y format
		$departureDate = '09-11-2018';
		$room = '&room1=2';
		$roomTypeCode = '200970574';
		$includeDetails = 'true';
		$options = 'HOTEL_DETAILS,ROOM_TYPES,ROOM_AMENITIES,PROPERTY_AMENITIES,HOTEL_IMAGES';
		$rateKey = '45be4e53-24fb-4d6f-9c84-2bfca2858149-5221-024';
		$latitude = '-31.69744';
		$longitude = '115.7093';
		$customerSessionId = '0ABAAC10-8071-AB29-1642-87F2E19B992C';
		$type =1;
		$rateCode ='204934267';
		
		//echo "<pre>";prigent_r($data);exit;
		//$data = Input::all();

		$result = Expedia::get_hotel_details_v2($hotelId,$arrivalDate,$departureDate,$room,$roomTypeCode,$includeDetails,$options,$rateKey,$latitude,$longitude,$customerSessionId,$type=1,$rateCode='');//For expedia
		return \Response::json([
			'status' => 200,
			'data' => $result
		]);
		
	}

	public function get_hotel_info()
	{	

		$hotelId = '505122';
		
		
		//echo "<pre>";print_r($data);exit;
		//$data = Input::all();

		$result = Expedia::get_hotel_info_v2($hotelId);
		return \Response::json([
			'status' => 200,
			'data' => $result
		]);
		
	}

	public function hotel_price_check()
	{	

		$data['city'] = 'Perth';
		$data['countryCode'] = 'AU';
		$data['arrivalDate'] = '2018-09-09';
		$data['departureDate'] = '2018-09-11';
		$data['numberOfAdults'] = 1;
		$data['customerSessionId'] = '0ABAAC10-8071-AB29-1642-87F2E19B992C';
		$data['latitude'] = '-31.69744';
		$data['longitude'] = '115.7093';
		$data['rooms'] = '1';
		$data['hotelId'] = '505122';
		$data['roomTypeCode'] = '200970570';
		$data['rateCode'] = '204747944';
		$data['rateKey'] = '4cd885bc-d412-445d-b0de-3ce04ccf516a-5221-024';
		$data['nightlyRateTotal'] = '251.46';
		$data['taxRate'] = '25.14';
		$data['search_input'] = array(
			'child' => array(),
			'num_of_adults' => array('0'=>2)
		);

		$data['city'] = array(
			'default_nights' => 2,
			'id' => 30,
			'name' => 'Perth'
		);
		

		//echo "<pre>";print_r($data);exit;
		//$data = Input::all();

		$result = Expedia::hotel_price_check_v2($data);
		return \Response::json([
			'status' => 200,
			'data' => $result
		]);
		
	}

	public function hotel_booking(){
		$hotelId = '505122';
		$arrivalDate = '09-09-2018'; //m-d-Y format
		$departureDate = '09-11-2018';
		$room = '&room1=2';
		$roomTypeCode = '200970574';
		$includeDetails = 'true';
		$options = 'HOTEL_DETAILS,ROOM_TYPES,ROOM_AMENITIES,PROPERTY_AMENITIES,HOTEL_IMAGES';
		$rateKey = '45be4e53-24fb-4d6f-9c84-2bfca2858149-5221-024';
		$latitude = '-31.69744';
		$longitude = '115.7093';
		$customerSessionId = '0ABAAC10-8071-AB29-1642-87F2E19B992C';
		$type =1;
		$rateCode ='204934267';
		
		$chargeableRate = 251.46 + 25.14;
		$passengers_info['passenger_first_name'] = 'Irfan';
		$passengers_info['passenger_last_name'] ='Bagwala';
		$bedtypes = 15;

		$data['rooms'] = 1;
		$data['search_input'] = array(
			'child' => array(),
			'num_of_adults' => array('0'=>2)
		);

		$data['city'] = array(
			'default_nights' => 2,
			'id' => 30,
			'name' => 'Perth'
		);
		//echo "<pre>";print_r($data);exit;
		//$data = Input::all();

		$result = Expedia::hotel_booking_v2($hotelId,$arrivalDate,$departureDate,$rateKey,$roomTypeCode,$rateCode,$chargeableRate,$passengers_info,$bedtypes,$customerSessionId,$data);

		//echo "<pre>";print_r($result);exit;
		return \Response::json([
			'status' => 200,
			'data' => $result
		]);
	}

	public function get_all_activity_by_city_id_v2()
	{	
		$data   = Input::all();
		
		// $data = '{"city_ids":["37","30"],"interests":[],"date_from":"2018-09-09","auto_populate":"1","traveller_number":"2","search_input":{"country":"513","city":"37","to_country":"513","to_city":"30","auto_populate":"1","countryId":"","countryRegion":"","countryRegionName":"","option":"auto","searchValType":"","searchVal":"","option2":"search-box5","start_location":"Hanoi, Vietnam","end_location":"Singapore, Singapore","start_date":"2018-09-09","travellers":"2","num_of_adults":["1","1","1"],"num_of_children":["0","2","0"],"child":[{"1":"2","2":"3"}],"interests":[]},"rooms":"2","pax_information":[],"child":[{"1":"2","2":"3"}]}';
		
		/*$data = '{"city_ids":["37","30"],"interests":[],"date_from":"2018-09-13","date_to":"2018-09-13"}';
		$data = json_decode($data,true);*/

		
		//$data = Input::Only('city_ids', 'interests', 'date_from', 'date_to');


		$activity_ids = ActivityPrice::where('allotment', '>', '0')->pluck('activity_id');

		$price_ids = ActivityPrice::where('allotment','>','0')->pluck('id');

		$activities = ActivityNew::select('zactivity.*','c.category_id','c.category_name','c.url')->whereIn('city_id', $data['city_ids'])
			->where('zactivity.is_publish','=', 1)
			->leftJoin('tblcategorydef as c','c.category_id','=', 'zactivity.activity_category_id')
			->where('zactivity.duration', '=', 1)
			->where('zactivity.default_activity_supplier_id','!=',44)
			->whereIn('zactivity.id', $activity_ids)
			->with(['City', 'Currency', 'image', 
				'Pivot' => function($query) use ($data)
				{
		         	if(!empty($data['interests'])){
		         		
		          	}
			   	}, 
				'activity_price' => function($query) use ($price_ids)
				{
					$query->with('currency');
					$query->whereIn('zactivityprices.id', $price_ids);
				}
			])
		    ->orderBy('default')
		    ->get();
		//dd($activities);
		$result = array();
		
		foreach($activities as $activity_key => $activity)
		{	
			/*if( count( $activity->image ) < 1 )
			{
			} else {*/

				$price_count = count($activity->activity_price);

				$temp_price = [];			
				foreach($activity->activity_price as $price_key => $price)
				{
					if($price->activity_supplier_id != $activity->default_activity_supplier_id)
					{
						
					}else{
					
						if( (strtotime($price->date_from) > strtotime($data['date_from']) && strtotime($price->date_to) > strtotime($data['date_from'])) || (strtotime($price->date_from) < strtotime($data['date_from']) && strtotime($price->date_to) < strtotime($data['date_from'])) ){
							
							$price_count--;
							unset($activity->activity_price[$price_key]); 
							
						}else{
							array_push($temp_price, $price);
						}

					
					}
				}
				//addition filter for interest
				$label_count = 0;
                if(!empty($data['interests']))
				{
					foreach($activity->pivot as $key => $label)
					{
						if(in_array($label->label_id, $data['interests']))
						{
							$label_count = ++$label_count;
						}
					}
				}else{
					$label_count = 1;
				}	
				if($price_count > 0 && $label_count > 0 ){
					unset($activity->activity_price) ;
					$activity->activity_price =  $temp_price;
					array_push($result, $activity);

				}	
			//}
			

		}
		/*echo "<pre>";
		print_r($result);
		exit;*/

		return \Response::json([
			'status' => 200,
			'data' => $result
		]); 
	}

	public function get_currency_values()
	{	
		$data = array();
        $result = array();
        if (Cache::has('currency'))
        {
            $result = Cache::get('currency');
        }
        else
        {
            $client = new Client();
            $response = $client->get('https://apilayer.net/api/live?access_key=18501dd8f3a1112a586e60281e150213&source=AUD');
            $currency_obj = json_decode( $response->getBody() );
            if( $response->getStatusCode() == 200 )
            {
                if( $currency_obj->success == TRUE )
                {
                    foreach( $currency_obj->quotes as $key => $value)
                    {
                        $data = array_merge( $data, [ substr($key, 3) => $value ] );
                    }
                    $expiresAt = Carbon::now()->addHours(1);
                    
                    Cache::put('currency', $data, $expiresAt);

                    if( Cache::has('currency_backup') )
                    {
                        Cache::forget('currency_backup');
                    }
                    Cache::forever('currency_backup', $data);

                    $result = $data;
                }
                else
                {
                    Cache::forever('currency_backup', $data);
                }
            }
            else
            {
                $result = Cache::get('currency_backup');
            }
        }
        return $result;
		return (new CurrencyLayer())->get();
	}
	public function get_all(){
		return Currency::get();

	}

	public function get_all_label(){
		$labels = Label::orderBy('sequence')->get();
		return \Response::json([
			'status' => 200,
			'data' => $labels]);
	}

	public function get_all_suburb_by_city($city_id){
		
		$data = Suburb::where('city_id',$city_id)->with('hb_zone')->get();
		return \Response::json([
			'status' => 200,
			'data' => $data
		]);
	
	}
	public function getRegions(){
        $regions = Region::with([ 
            'countries' => function( $country_query ){ 
                $country_query->with([ 
                    'city' => function( $city_query ){          
                        $city_query->with([ 'hb_destinations_mapped', 'suburbs' ]); 
                    }]); 
            }])
        ->get();
        return \Response::json([ 'status' => 200, 'data' => $regions ]);
    }
	
	public function get_traveler_profile_options()
    {

        $data            = array();
        $categories      = HotelCategory::orderBy('sequence')->get();
        $room_types      = HotelRoomType::orderBy('sequence')->get();
        $transport_types = TransportType::orderBy('sequence')->get();
        $age_groups      = AgeGroup::orderBy('id')->get();
        $featured        = Nationality::where('is_featured', TRUE)->orderBy('name')->get();
        $not_featured    = Nationality::where('is_featured', FALSE)->orderBy('name')->get();
        $nationalities   = array('featured' => $featured, 'not_featured' => $not_featured);
        return compact(
            'categories', 
            'room_types', 
            'transport_types', 
            'age_groups',
            'nationalities'
        );
    }

    public function get_all_hotel_categories(){
		$hotel_category = HotelCategory::get();

		return \Response::json([
			'status' => 200,
			'data' => $hotel_category
		]);

	}

	
	public function expediaMappedHotels(){
		$data = Input::all();
		$result = Expedia::expediaMappedHotels($data);//For expedia
		return \Response::json([
			'status' => 200,
			'data' => $result
		]);
	}
	public function getCardinfo(){
		$result = Expedia::getCardinfo();//For expedia
		return \Response::json([
			'status' => 200,
			'data' => $result
		]);
	}
	
	public function getRefreshCache() {
		DB::enableQueryLog();
		$data   = Input::all();
		$domain = isset($data['domain']) ? $data['domain']: '' ;
        $response = DB::table('cache_reload')->select('is_reload')->where('domain', $domain)->first();
        return \Response::json([ 'status' => 200, 'data' =>$response ]);
    }

    public function updateRefreshCache() {
    	$data   = Input::all();
		$domain = isset($data['domain']) ? $data['domain']: '' ;
		$response = DB::table('cache_reload')->where('domain', $domain)->update(['is_reload' => 0]);
        return \Response::json([ 'status' => 200, 'data' => 0 ]);
    }
	public function get_all_transport_by_city_ids_v2()
	{
		$domain = Input::get('domain');
		$inventory = getInventory($domain);
		if(!empty($inventory['inventory'])) {
			foreach($inventory['inventory'] as $inven) {
				$apis[] = $inven['inventory_config']['slug'];
			}	
		}

		if(in_array('static_transport', $apis)){
			$from_city_id          = Input::get('from_city_id');
			$to_city_id            = Input::get('to_city_id');
			$date_from             = Input::get('date_from');
			$date_to               = Input::get('date_to');
			$day                   = Input::get('day');
			$transport_types 	   = Input::get('transport_types');
			$transport_etd_is_null = Transport::whereNull('etd')->orWhere(['etd' => ""])->pluck('id');
			$transport_eta_is_null = Transport::whereNull('eta')->orWhere(['eta' => ""])->pluck('id');

			$transport_ids = TransportPrice::where('allotment','>','0')
			    ->whereRaw("((ztransportprices.from <= '".$date_from."') AND (ztransportprices.to >= '".$date_from."')) AND ((ztransportprices.from <= '".$date_to."') AND (ztransportprices.to >= '".$date_to."'))")
			    ->pluck('transport_id');

			$price_ids     = TransportPrice::where('allotment','>','0')
			    ->whereRaw("((ztransportprices.from <= '".$date_from."') AND (ztransportprices.to >= '".$date_from."')) AND ((ztransportprices.from <= '".$date_to."') AND (ztransportprices.to >= '".$date_to."'))")
			    ->pluck('id');

			$transports = Transport::whereIn('id', $transport_ids);
			if(isset($transport_types) && count($transport_types) > 0){
				$transports = $transports->whereIn('transport_type_id',$transport_types);
			}

			$transports = $transports->where([
					'from_city_id' => $from_city_id,
					'to_city_id' => $to_city_id 
					])
				->where('ztransports.is_publish','=', 1)
			    ->whereNotIn('id', $transport_etd_is_null)
			    ->whereNotIn('id', $transport_eta_is_null)
	            ->with(['from_city', 'to_city', 'supplier', 'operator', 'transport_type', 'currency','transporttype',
					'price' => function($query) use ($price_ids)
					{
						$query->whereIn('ztransportprices.id', $price_ids);
					}

				])->get();

			$result = array();
            $suppliers = array_column($transports->toArray(), 'default_transport_supplier_id');
            $transport_supplier = TransportSupplier::whereIn('id',$suppliers)->get();
            $transport_supplier_name = array_column($transport_supplier->toArray(), 'name');

			foreach($transports as $transport_key => $transport)
			{	
				$price_count = count($transport->price);
				foreach($transport->price as $price_key => $price)
				{
					if($price->transport_supplier_id != $transport->default_transport_supplier_id)
					{
						$price_count--;
						unset($transport->price[$price_key]);
					}

					if( $price->price == "0.0" )
					{
						$price_count--;
						unset($transport->price[$price_key]); 
					}

				}

				if($price_count > 0)
				{
					$transport->day = $day;
					array_push($result, $transport);
				}
			}

			if (!empty($result)) {
				
		        if ($domain) {
		            $commissions = getDomainData($domain);
	            	if ($commissions['pricing'][0]['status'] == 0) {
			            $result = updateTransportPricing($result,'transport',['eroam'],$commissions['eroam'],$commissions['pricing']);
			        }
		        }
			}
		} else {
			$result = [];
		}

		return \Response::json([
			'status' => 200,
			'data' => $result
		]);
	}

	public function getDefaultActivity()
	{
		$data   = Input::all();
		$activity_ids = ActivityPrice::where('allotment', '>', '0')->pluck('activity_id');

		$date_from = $data['date_from'];
		$price_ids = ActivityPrice::where('allotment','>','0')->where('minimum_pax','<=',$data['passenger'])
			->where(function ($query) use ($date_from) {
		    $query->where('date_from', '<=', $date_from);
		    $query->where('date_to', '>=', $date_from);
		})
		->pluck('id');

		$activities = ActivityNew::select('zactivity.*','c.category_id','c.category_name','c.url')->where('city_id', $data['city_id'])
			->where('zactivity.is_publish','=', 1)
			->where('zactivity.default','=', 1)
			->leftJoin('tblcategorydef as c','c.category_id','=', 'zactivity.activity_category_id')			
			->where('zactivity.duration', '=', 1)
			->whereNull('zactivity.code')
			->whereIn('zactivity.id', $activity_ids)
			->with(['City', 'Currency', 'image',
				'activity_price' => function($query) use ($price_ids,$date_from)
				{
					$query->with('currency');
					$query->whereIn('zactivityprices.id', $price_ids);
				}
			])		    
		    ->get();
		
		$result = array();
		
		foreach($activities as $activity_key => $activity)
		{	
			/*if( count( $activity->image ) < 1 )
			{
			} else {*/
			
				$price_count = count($activity->activity_price);

				$temp_price = [];			
				foreach($activity->activity_price as $price_key => $price)
				{
					if($price->activity_supplier_id != $activity->default_activity_supplier_id)
					{
						
					}else{
					
						if( (strtotime($price->date_from) > strtotime($data['date_from']) && strtotime($price->date_to) > strtotime($data['date_from'])) || (strtotime($price->date_from) < strtotime($data['date_from']) && strtotime($price->date_to) < strtotime($data['date_from'])) ){
							
							$price_count--;
							unset($activity->activity_price[$price_key]); 
							
						}else{
							array_push($temp_price, $price);
						}

					
					}
				}
									
				if($price_count > 0 ){
					unset($activity->activity_price) ;
					$activity->activity_price =  $temp_price;
					array_push($result, $activity);
					break;
				}	
			//}			

		}		

		return \Response::json([
			'status' => 200,
			'data' => $result
		]); 
	}
	public function getDefaultActivityTest()
	{
		$result = array();
		return \Response::json([
			'status' => 200,
			'data' => $result
		]); 
		$data   = Input::all();
		$activity_ids = ActivityPrice::where('allotment', '>', '0')->pluck('activity_id');

		$date_from = $data['date_from'];
		$price_ids = ActivityPrice::where('allotment','>','0')->where('minimum_pax','<=',$data['passenger'])
			->where(function ($query) use ($date_from) {
		    $query->where('date_from', '<=', $date_from);
		    $query->where('date_to', '>=', $date_from);
		})
		->pluck('id');

		$activities = ActivityNew::select('zactivity.*','c.category_id','c.category_name','c.url')->where('city_id', $data['city_id'])
			->where('zactivity.is_publish','=', 1)
			->where('zactivity.default','=', 1)
			->leftJoin('tblcategorydef as c','c.category_id','=', 'zactivity.activity_category_id')			
			->where('zactivity.duration', '=', 1)
			->whereNull('zactivity.code')
			->whereIn('zactivity.id', $activity_ids)
			->with(['City', 'Currency', 'image',
				'activity_price' => function($query) use ($price_ids,$date_from)
				{
					$query->with('currency');
					$query->whereIn('zactivityprices.id', $price_ids);
				}
			])		    
		    ->get();
		
		$result = array();
		
		foreach($activities as $activity_key => $activity)
		{	
			/*if( count( $activity->image ) < 1 )
			{
			} else {*/
			
				$price_count = count($activity->activity_price);

				$temp_price = [];			
				foreach($activity->activity_price as $price_key => $price)
				{
					if($price->activity_supplier_id != $activity->default_activity_supplier_id)
					{
						
					}else{
					
						if( (strtotime($price->date_from) > strtotime($data['date_from']) && strtotime($price->date_to) > strtotime($data['date_from'])) || (strtotime($price->date_from) < strtotime($data['date_from']) && strtotime($price->date_to) < strtotime($data['date_from'])) ){
							
							$price_count--;
							unset($activity->activity_price[$price_key]); 
							
						}else{
							array_push($temp_price, $price);
						}

					
					}
				}
									
				if($price_count > 0 ){
					unset($activity->activity_price) ;
					$activity->activity_price =  $temp_price;
					array_push($result, $activity);
					break;
				}	
			//}			

		}		
	}
}
