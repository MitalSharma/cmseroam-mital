<?php
//priya
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Tour;
use App\Season;
use App\Dates;
use App\BookingRequest;
use App\BookingRequestsAdditionalPerson;
use DB;
use Response;
use App\BookingCommission;
class ApiTourController extends Controller
{
    public function getTours(Request $oRequest) 
    {
        $oTour = Tour::getHomeTours();
        
        $nTourIds = array_column($oTour->toArray(), 'tour_id');
        //print_r($nTourIds);exit;
        $tourFlightResult = Season::getSeasonFlightList($nTourIds);  
        if (!empty($tourFlightResult)) {
            //echo "<pre>";print_r($oTour);exit;
            foreach ($oTour as $key1 => $value1) {
                $oTour[$key1]->sumPrice = 0;
            }
            foreach ($tourFlightResult as $key => $value) {

                foreach ($oTour as $key1 => $value1) {

                    if ($value1->departure == $value->flight_depart_city && $value->tour_id == $value1->tour_id) {
                        $sumPrice = $value->AdultPrice + $value->flightPrice;
                        if (empty($oTour[$key1]->sumPrice) || $oTour[$key1]->sumPrice > $sumPrice) {
                            $oTour[$key1]->sumPrice = $sumPrice;
                            $oTour[$key1]->price = $value->AdultPrice;
                            $oTour[$key1]->season_id = $value->season_id;
                            $oTour[$key1]->flightDepart = $value->name;
                            $oTour[$key1]->flightPrice = $value->flightPrice;
                        }
                    }
                }
            }
        }
        //echo $tourFlightResult ;exit;     
        return \Response::json(array(   
	        'status' => 200,
	        'data' => $oTour));
    }
    
    public function searchTour()
    {
        /*$data = [   'countryNames'=> 'India',
                   'tourTitle'=>'',
                   'provider'=> 'searchTour',
                   'tour_type'=> 'country',
                   'search_type'=> '',
                   'search_country_id'=> 605,
                   'search_city_id'=> 37,
                   'search_value'=> 'INDIAN EXPRESS',
                   'date'=> '1970-01-01',
                   'default_selected_city'=> 30,
                   'offset'=> 0,
                   'limit'=> 5];*/
        //$data = array();
        $data = Input::all();
        $count = 0;
        $oTour = Tour::getTour($data);
        $flight_depart_city = 30;

        $nTourIds = array_column($oTour->toArray(), 'tour_id');
        $tourFlightResult = Season::getSeasonFlightList($nTourIds);
        if (!empty($tourFlightResult)) {

            foreach ($oTour as $key1 => $value1) {
                $oTour[$key1]->sumPrice = 0;
            }
            foreach ($tourFlightResult as $key => $value) {
                foreach ($oTour as $key1 => $value1) {
                    if ($flight_depart_city == $value->flight_depart_city && $value->tour_id == $value1->tour_id) {
                        $sumPrice = $value->AdultPrice + $value->flightPrice;
                        if (empty($oTour[$key1]->sumPrice) || $oTour[$key1]->sumPrice > $sumPrice) {
                            $oTour[$key1]->sumPrice = $sumPrice;
                            $oTour[$key1]->price = $value->AdultPrice;
                            $oTour[$key1]->season_id = $value->season_id;
                            $oTour[$key1]->flightDepart = $value->name;
                            $oTour[$key1]->flightPrice = $value->flightPrice;
                        }
                    }
                }
            }
        }
        
        $domain = request()->header('domain');
        if ($domain) {
            $commissions = getDomainData($domain);
            if ($commissions['pricing'][0]['status'] == 0) {
                $suppliers = array_column($commissions['eroam']->toArray(), 'supplier');
                foreach ($oTour->toArray() as $tourkey => $tour) {
                    if (in_array($tour['provider_name'], $suppliers)) {
                        $tours[] = updateTourPricing($tour,'tour',$tour['provider_name'],$commissions['eroam'],$commissions['pricing']);
                    }else{
                        $tours[] = $tour;
                    }
                }
            }
        }

        $data1['results'] = isset($tours) ? $tours : $oTour;

        return Response::json(array(
                    'status' => 200,
                    'data' => $data1));
    }
    
    public function getTotalTourByCountry()
    {
        /*$datainp['countries'] = "Australia";
        $datainp['date'] = "";
        $datainp['tour_type'] = "both";*/
        $datainp    = Input::all();
        $countries  = ($datainp['countries']) ? explode('# ', $datainp['countries']) : array();
        $count      = 0;
        $data       = [];
        $date       = (array_key_exists('date',$datainp)&& isset($datainp['date'])) ? $datainp['date'] : '';
        $tour_type  = (array_key_exists('tour_type',$datainp)&& isset($datainp['tour_type'])) ? $datainp['tour_type'] : '';
        
        foreach ($countries as $key => $value) {
            $sCName = trim($value);
            if ($value == 'Myanmar [Burma]') {
                $sCName = 'Myanmar';
            } else if ($value == 'Indonesia [Bali]') {
                $sCName = 'Indonesia';
            } else if ($value == 'Malaysia [Borneo]') {
                $sCName = 'Malaysia';
            }

            $nTour = Tour::getTotalTourByCountry($sCName, $date, $tour_type);
            $data[$value] = $nTour[0]->count_tour;
            $count += $nTour[0]->count_tour;
        }

        return Response::json(array(
                    'status' => 200,
                    'data' => $data,
                    'count' => $count));
    }
    
    public function getTotalTourByRegion()
    {
        //$data = [];   
        $aData = Input::all();
        $nTour = Tour::getTotalTourByRegion($aData);

        return Response::json(array(
                    'status' => 200,
                    'data' => $nTour));
    }
    
    public function getTotalTourByCity(){
        $data = Input::all();
        $cities = $data['cities'];
        $count = 0;

        $data = [];
        if (!empty($cities)) {
            $i = 0;
            foreach ($cities as $key => $value) {
                $city_id = $value['id'];
                $nTour = Tour::getTotalTourByCity($sCName);
                $data[$value['name']] = $nTour;
                $count += $nTour;
                $i++;
            }
        }

        return Response::json(array(
                    'status' => 200,
                    'data' => $data,
                    'count' => $count));
    }

    //remaining
    public function getTourBooking()
    {
//        $datas = Array
//(
//    ['tour_all_data'] => Array
//        (
//            ['tour_id'] => '12049',
//            ['tour_title'] => '',
//            ['departure_date'] => '1970-01-01 00:00:00',
//            ['return_date'] => '1970-01-01 00:00:00',
//            ['provider'] => '3',
//            ['code'] => 'TTAV',
//            ['price'] => '1998',
//            ['singletotal'] => '0.00',
//            ['twintotal'] => '1998.00',
//            ['sub_total'] => '1998.00',
//            ['grand_total'] => '1998.00',
//            ['booking_currency'] => '$AUD',
//            ['FName'] => 'chintan',
//            ['SurName'] => patel,
//            ['contact'] => 9998004908,
//            ['email'] => dhara@corigami.com.au,
//            ['card_holder_name'] => chintan chintan,
//            ['eway_authcode'] => 943091,
//            ['eway_transaction_no'] => 18370956,
//            ['eway_txn_number'] => 18370956,
//            ['credit_card_fee'] => 0.00,
//            ['postal_code'] => 123456,
//            ['billing_first_name'] => chintan,
//            ['billing_family_name'] => chintan,
//            ['billing_email'] => chintan@corigami.com.au,
//            ['billing_contact'] => 9998004908,
//            ['billing_company'] => ,
//            ['billing_street_addr'] => test,
//            ['billing_additional_addr'] => Melbourne VIC, Australia,
//            ['suburb'] => ,
//            ['state'] => ,
//            ['country'] => 513,
//            ['request_date'] => 2018-07-19 12:43:20,
//            ['status'] => 0,
//            ['Agent_id'] => 1,
//            ['payment_method'] => 1,
//            ['BookingType'] => Direct into account,
//            ['domain_id'] => 1,
//            ['addon_total_price'] => 0,
//            ['eway_result'] => Confirmed,
//            ['domain'] => 127.0.0.1,
//            ['user_id'] => 1141
//        )
//
//    ['additional_person'] => Array
//        (
//            [0] => Array
//                (
//                    ['tour_id'] => 12049,
//                    ['type'] => adult,
//                    ['title'] => ms,
//                    ['first_name'] => a,
//                    ['family_name'] => b,
//                    ['email'] => aa@dsf.sdsf,
//                    ['contact'] => 123456,
//                    ['child_age'] => 
//                )
//
//        )
//
//)
        
        
    	$datas 	= Input::all();
    	$data 	= $datas[0]['tour_all_data'];
        if (isset($data['detailPricing'])) {
            $detailPricing = $data['detailPricing'];
            unset($data['detailPricing']);
        }
        //return Response::json(['data' => $datas]);
        $aNewData = array();
        foreach($data as $key => $value){
            $aNewData[$key] = $value;
        }
        $oBookingRequest = BookingRequest::create($aNewData);
        // $booking_commission = BookingCommission::create();
    	$result['result'] = 1;
        $result['lastInsertId'] = $oBookingRequest->request_id;

        if(isset($detailPricing)) {
            $booking_commission = BookingCommission::create([
                'booking_id' => $result['lastInsertId'],
                'product_type' => 'tour',
                'licensee_id' => $detailPricing['licensee_id'],
                'reseller_cost' => $detailPricing['reseller_cost'],
                'reseller_rrp_per' => $detailPricing['reseller_rrp_per'],
                'reseller_rrp' => $detailPricing['reseller_rrp'],
                'reseller_margin' => $detailPricing['reseller_margin'],
                'licensee_commission' => $detailPricing['licensee_commission'],
                'licensee_rrp' => $detailPricing['licensee_rrp'],
                'licensee_cost' => $detailPricing['licensee_cost'],
                'licensee_benefit' => $detailPricing['licensee_benefit'],
                'reseller_benefit' => $detailPricing['reseller_benefit']
            ]);
        }

		if(isset($datas[0]['additional_person']))
		{
			$additionalPersonInfo = $datas[0]['additional_person'];
			if(count($additionalPersonInfo) > 0)
			{
                            $aAddData = array();
				foreach ($additionalPersonInfo as $key => $value) {
                                    //$aAddData = array();
                                    $additionalPersonInfo[$key]['request_id'] = $result['lastInsertId'];
                                    //$aAddData
                                    BookingRequestsAdditionalPerson::create($additionalPersonInfo[$key]);
		        }
	        }
	    }
    	return Response::json(['data' => $result]);
    }
    
    public function getTourDetail()
    {
       // $data = array();
       // $data['default_selected_city'] = 30;
        $data = Input::all();

        $sUrl = (isset($data['url']) ) ? $data['url'] : 'active_chiang_mai';
        $nTourId = (isset($data['id']) ) ? $data['id'] : 12049;
        $default_selected_city = (isset($data['default_selected_city']) ) ? $data['default_selected_city'] : '';
        
        $flight_depart_city = $default_selected_city;
        if (empty($flight_depart_city)) {
            $flight_depart_city = 30;
        }
        
        $results = Tour::getTourDetail($nTourId,$sUrl);

        $domain = request()->header('domain');
        if ($domain) {
            $commissions = getDomainData($domain);
            if ($commissions['pricing'][0]['status'] == 0) {
                $suppliers = array_column($commissions['eroam']->toArray(), 'supplier');
                if (in_array($results['provider_name'], $suppliers)) {
                    $results = updateTourPricing($results,'tour',$results['provider_name'],$commissions['eroam'],$commissions['pricing']);
                }
            }
        }

        $data['results'][0] = $results;

        $currentdate = date('Y-m-d');
        $atour['0']= $nTourId;
        $tourFlightResult = Season::getSeasonFlightList($atour);

        if (!empty($tourFlightResult)) {
            $results->sumPrice = 0;
            foreach ($tourFlightResult as $key => $value) {
                if ($flight_depart_city == $value->flight_depart_city && $value->tour_id == $results->tour_id) {
                    $sumPrice = $value->AdultPrice + $value->flightPrice;
                    if (empty($results->sumPrice) || $results->sumPrice > $sumPrice) {
                        $results->sumPrice = $sumPrice;
                        $results->price = $value->AdultPrice;
                        $results->season_id = $value->season_id;
                        $results->flightDepart = $value->name;
                        $results->flightPrice = $value->flightPrice;
                    }
                }
            }
        }
        $data['tourFlightResult'] = $tourFlightResult;
        $data['reviews'] = $results->reviews;

        $countries = Tour::getTourCountries($nTourId);
        $data['countries'] = $countries;

        $images = Tour::getTourImages($nTourId);
        $data['images'] = $images;

        $category = Tour::getTourCategory($nTourId);
        $data['category'] = $category;

        $provider_id = $results->provider_id;
        $tour_id = $results->tour_id;
        $tour_currency = $results->tour_currency;

        $provider_tour_id = $results->provider_tour_id;
        $tour_code = $results->code;

        $dates = array();

        $datesSean = Season::getTourSeason($results->tour_id);
        
        if ($datesSean) {
            $i = 0;
            $updatePricing = false;
            if ($domain) {
                $commissions = getDomainData($domain);
                if ($commissions['pricing'][0]['status'] == 0) {
                    $suppliers = array_column($commissions['eroam']->toArray(), 'supplier');
                    if (in_array($results['provider_name'], $suppliers)) {
                        $updatePricing = true;
                    }
                }
            }
            foreach ($datesSean as $date) {
                $currentdate = date('Y-m-d', strtotime('+' . ($date->adv_purchase + 1) . ' days'));
                $sdates = Dates::getTourDate($results->tour_id,$date->season_id,$currentdate);

                if ($sdates) {
                    foreach ($sdates as $sdate) {

                        if ($updatePricing) {
                            $sdate = updateTourDatePricing($sdate,'tour',$results['provider_name'],$commissions['eroam'],$commissions['pricing']);
                        }

                        $dates[$i]['departureCode'] = $results->tour_code; //$date->season_id;
                        $dates[$i]['departureName'] = '';
                        $dates[$i]['startDate'] = $sdate['StartDate'];
                        $dates[$i]['finishDate'] = $sdate['EndDate'];
                        $dates[$i]['startCity'] = $results['departure'];
                        $dates[$i]['finishCity'] = $results['destination'];
                        $dates[$i]['availability'] = $sdate['Availability'];
                        $dates[$i]['currency'] = $tour_currency;
                        $dates[$i]['base'] = $sdate['AdultPrice'];
                        $dates[$i]['total'] = $sdate['AdultPrice'];
                        $dates[$i]['singlePrice'] = $sdate['AdultPriceSingle'];
                        $dates[$i]['supplement'] = $sdate['AdultSupplement'];
                        $dates[$i]['season_id'] = $sdate['season_id'];
                        if ($updatePricing) {
                            $dates[$i]['AdultPrice_detailPricing'] = $sdate['AdultPrice_detailPricing'];
                            $dates[$i]['AdultPriceSingle_detailPricing'] = $sdate['AdultPriceSingle_detailPricing'];
                        }
                        $i++;
                    }
                    $dates= $dates;
                } else {
                    $dates = array();
                }
            }
        } else {
            $dates = array();
        }
        $data['Seasons'] = $datesSean;
        $data['dates'] = $dates;

        Tour::where('tour_id', '=', $tour_id)->increment('views');

        return Response::json(['data' => $data]);
    }
    
    public function getTotalTourByCountryLists() 
    {
        $aData = Input::all();
        
        $oCountry = Tour::getAvailableTourCountry($aData);
        return Response::json(array(
                    'status' => 200,
                    'data' => $oCountry));
    }

    public function getTourCitiesAvailable() {
        $oTourCity = Tour::getAvailableTourCities();

        return Response::json(array(
                    'status' => 200,
                    'data' => $oTourCity));
    }
    
    public function getTotalTourByCityId()
    {
        /*$input = [
                'tour_type' => 'both'];*/
        $count = 0;
        $input 	= Input::all();
        $data = [];
        //echo "dfgdf";exit;
        //DB::enableQueryLog();
	   $oCountry = Tour::getTotalTourByCityId($input);	
	   //dd(DB::getQueryLog());
        return Response::json(array(
	        'status' => 200,
	        'data' => $oCountry));
	}
}
