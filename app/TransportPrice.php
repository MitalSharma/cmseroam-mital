<?php
namespace App;
use Eloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
class TransportPrice extends Model {

    protected $table = 'ztransportprices';
    protected $guarded = array('id');

    protected $dates = ['deleted_at'];
    protected $primaryKey = 'id';
    public $timestamps = false;


    public static function getTransportPriceList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord)
    {
        if($sOrderField):
            $transports = Transport::getRelationships(NULL)->orderBy($sOrderField, $sOrderBy)->paginate($nShowRecord);
        else:
            $transports = Transport::getRelationships(NULL)->orderBy('from_city_id', 'ASC')->paginate($nShowRecord);
        endif;
		//echo '<pre>'; print_r($transports);die;
        if ($sSearchBy) {
			
            switch($sSearchBy){
                case "origin":
                    $city_ids = City::where('name', 'LIKE', '%' . $sSearchStr . '%')->pluck('id');
                    $transports = Transport::whereIn('from_city_id', $city_ids)
                        ->getRelationships(NULL)
                        ->orderBy($sOrderField, $sOrderBy)
                        ->paginate($nShowRecord);
                    break;
                case "destination":
                    $city_ids = City::where('name', 'LIKE', '%' . $sSearchStr . '%')->pluck('id');
                    $transports = Transport::whereIn('to_city_id', $city_ids)
                        ->getRelationships(NULL)
                        ->orderBy($sOrderField, $sOrderBy)
                        ->paginate($nShowRecord);
                    //$transports->appends(Request::only('search','search-by'))->links();
                    break;
                case "type":
                    $transport_type_ids = TransportType::where('name', 'LIKE', '%' . $sSearchStr . '%')
                        ->pluck('id');
                    $transports = Transport::whereIn('transport_type_id', $transport_type_ids)
                        ->getRelationships(NULL)
                        ->orderBy($sOrderField, $sOrderBy)
                        ->paginate($nShowRecord);
                    //$transports->appends(Request::only('search','search-by'))->links();
                    break;
                case "operator":
                    $operator_ids = TransportOperator::where('name', 'LIKE', '%' . $sSearchStr . '%')
                        ->pluck('id');
                    $transports = Transport::whereIn('operator_id', $operator_ids)
                        ->getRelationships(NULL)
                        ->orderBy($sOrderField, $sOrderBy)
                        ->paginate($nShowRecord);
                    //$transports->appends(Request::only('search','search-by'))->links();
                    break;
                case "price":
                    if (is_numeric($sSearchStr)) {
                        $markup         = TransportMarkup::with(['markup_percentage'])->where(['is_default' => 1])->first();
                        $base_price     = round(( $sSearchStr / 100 ) * (100 - $markup->markup_percentage->percentage ));
                        $base_price_ids = TransportBasePrice::where(['base_price' => $base_price])->pluck('id');
                        $transport_ids  = TransportPrice::whereIn('transport_base_price_id', $base_price_ids)->pluck('transport_id');
                        $price_ids      = TransportPrice::whereIn('transport_base_price_id', $base_price_ids)->pluck('id');
                        $transports     = Transport::whereIn('zTransports.id', $transport_ids)
                            ->getRelationships($price_ids)
                            ->orderBy($sOrderField, $sOrderBy)
                            ->paginate($nShowRecord);
                        //$transports->appends(Request::only('search','search-by'))->links();
                    }
                    break;
                case "pax":
                    if (is_numeric($sSearchStr)) {
                        $transport_ids = TransportPrice::where('minimum_pax', $sSearchStr)->pluck('transport_id');
                        $price_ids     = TransportPrice::where('minimum_pax', $sSearchStr)->pluck('id');
                        $transports    = Transport::whereIn('ztransports.id', $transport_ids)
                            ->getRelationships($price_ids)
                            ->orderBy($sOrderField, $sOrderBy)
                            ->paginate($nShowRecord);
                        //$transports->appends(Request::only('search','search-by'))->links();
                    }
                    break;
                case "allotment":
                    if (is_numeric($sSearchStr)) {
                        $transport_ids = TransportPrice::where('allotment', $sSearchStr)->pluck('transport_id');
                        $price_ids     = TransportPrice::where('allotment', $sSearchStr)->pluck('id');
                        $transports    = Transport::whereIn('ztransports.id', $transport_ids)
                            ->getRelationships($price_ids)
                            ->orderBy($sOrderField, $sOrderBy)
                            ->paginate($nShowRecord);
                        //$transports->appends(Request::only('search','search-by'))->links();
                    }
                    break;
                case "supplier":
                    $supplier_ids  = TransportSupplier::where('name', 'LIKE', '%' . $sSearchStr . '%')->pluck('id');
                    $transport_ids = TransportPrice::whereIn('transport_supplier_id', $supplier_ids)->pluck('transport_id');
                    $price_ids     = TransportPrice::whereIn('transport_supplier_id', $supplier_ids)->pluck('id');
                    $transports    = Transport::whereIn('ztransports.id', $transport_ids)
                        ->getRelationships($price_ids)
                        ->orderBy($sOrderField, $sOrderBy)
                        ->paginate($nShowRecord);
                    //$transports->appends(Request::only('search','search-by'))->links();
                    break;
            }
        
        }
        return $transports;
    } 
    
    public function passenger(){
        return $this->hasOne('\App\TransportPassengerType','id','passenger_type_id');
    }

    public function transport(){
        return $this->hasOne('\App\Transport','id','transport_id')->with('supplier','operator','to_city','from_city','transporttype');
    }

    public function currency(){
        return $this->hasOne('\App\Currency','id','currency_id');
    }    

    public function supplier(){
        return $this->hasOne('\App\TransportSupplier', 'id', 'transport_supplier_id');
    }

    public function markup_obj(){
        return $this->hasMany('\App\TransportMarkup','id','transport_markup_id');
    }

    public function base_price_obj(){
        return $this->hasOne('\App\TransportBasePrice','id','transport_base_price_id');
    }    

    public function scopePrice_summary($query){
        return $query
            ->join('ztransportbaseprices AS bs', 'bs.id', '=', 'ztransportprices.transport_base_price_id')
            ->join('ztransportmarkuppercentages AS ms', 'ms.id', '=', 'ztransportprices.transport_markup_percentage_id')
            ->selectRaw('ztransportprices.*, bs.base_price, ms.percentage, (bs.base_price / ( 100 - ms.percentage ) ) * 100 AS price');
    }



}

?>