<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class ItineryLeg extends Model
{
    protected $fillable = [
                            'itinerary_id', 'from_city_id','to_city_id','travel_sequence','transport_type','hotel_type','from_date','to_date',
                            'activity_type'
                        ];
    protected $table = 'zitinerieslegs';
    protected $primaryKey = 'id';
    use SoftDeletes;

    protected $dates = ['deleted_at'];
}
