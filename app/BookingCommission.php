<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingCommission extends Model
{
    protected $table = 'booking_commissions';
    protected $guarded = [];
}
