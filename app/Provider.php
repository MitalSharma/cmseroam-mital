<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    protected $fillable = [
        'provider_name','is_xml','currency_id','Logo','markup','Group_ID','is_active','abbriviation','folder_path','Password','login_name',
        'url','account_contacts','reservation_contacts','meta_title','meta_keyword','meta_desc','special_notes','supplier_desc','standard_remarks',
        'sp_name','sp_title','sp_email','sp_phone','sr_name','sr_email','sr_landline','sr_freephone','sa_name','sa_title','sa_email','sa_phone'
    ];
    protected $table = 'tblproviders';
    protected $primaryKey = 'provider_id';
}
