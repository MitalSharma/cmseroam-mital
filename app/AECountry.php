<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AECountry extends Model
{
    protected $fillable = [
        'CountryId','Code','Name'
    ];
    protected $table = 'zaecountries';
    protected $primaryKey = 'id';
    
    
}
