<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::match(['get','post'],'getTours',  ['as' => 'getTours', 'uses' => 'ApiTourController@getTours']);
Route::match(['get','post'],'searchTour',['as' => 'searchTour','uses'=>'ApiTourController@searchTour']);
Route::match(['get','post'],'tourCountryCount',['as' => 'tourCountryCount','uses'=>'ApiTourController@getTotalTourByCountry']);
Route::match(['get','post'],'tourDetail',['as' => 'tourDetail','uses'=>'ApiTourController@getTourDetail']);
Route::match(['get','post'],'getTourCountriesAvailable',['as' =>'getTourCountriesAvailable','uses'=>'ApiTourController@getTotalTourByCountryLists']);
Route::match(['get','post'],'getTourCitiesAvailable',['as' =>'getTourCitiesAvailable','uses'=>'ApiTourController@getTourCitiesAvailable']);
Route::any('tourRegionCount','ApiTourController@getTotalTourByRegion');
Route::any('tourCityCount','ApiTourController@getTotalTourByCity');
Route::any('tourBooking','ApiTourController@getTourBooking'); //there is created tourbookingnew function as old cms / this is new function 
Route::any('tourCityCountsAll','ApiTourController@getTotalTourByCityId');
/*
    Status : Start
    Routes for APIS call from @frontend

*/
Route::prefix('map')->group(function () {
    Route::match(['get','post'],'related/cities/{country_id}','MapApiController@getAllRelatedCities');
    Route::match(['get','post'],'city/from-and-to-v2','MapApiController@get_all_transport_by_city_ids_v2');
    Route::match(['get','post'],'city/hotel','MapApiController@hotel_by_city_id');
    Route::match(['get','post'],'get/all/countries','MapApiController@getAllCountries');
    Route::match(['get','post'],'get/all/countriesBookingPro','MapApiController@getAllCountries');
    Route::match(['get','post'],'city/activity','MapApiController@get_all_activity_by_city_id_v2');
    Route::match(['get','post'],'city/{id}','MapApiController@getCityByCityId');
    Route::match(['get','post'],'nearby-cities','MapApiController@getNearbyCities');
    Route::match(['get','post'],'get-transport-by-city-ids','MapApiController@getTransportByCitieIDs');
    Route::match(['get','post'],'auto-routes','MapApiController@getFixedCities');
    Route::match(['get','post'],'city','MapApiController@getAllCities');
    Route::match(['get','post'],'city-defaults','MapApiController@findAllDefault');
    Route::match(['get','post'],'city-defaults-test','MapApiController@findAllDefaultTest');
    Route::match(['get','post'],'get-hotels','MapApiController@get_hotels_by_city_id_v2');
    Route::match(['get','post'],'get-hotel-details','MapApiController@get_hotel_details');
    Route::match(['get','post'],'get-hotel-info','MapApiController@get_hotel_info');
    Route::match(['get','post'],'hotel-price-check','MapApiController@hotel_price_check');
    Route::match(['get','post'],'hotel-booking','MapApiController@hotel_booking');
    Route::match(['get','post'],'get-all-activity','MapApiController@get_all_activity_by_city_id_v2');
    Route::match(['get','post'],'hotel-by-city-id','MapApiController@hotel_by_city_id');
    Route::match(['get','post'],'city/hotel','MapApiController@hotel_by_city_id');
    Route::match(['get','post'],'city/from-and-to-v2','MapApiController@get_all_transport_by_city_ids_v2');
    
    Route::match(['get','post'],'gethotelstesting','MapApiController@get_hotel_by_city_id_v2');
    Route::match(['get','post'],'city/hotel/v2','ApiHotelController@hotel_by_city_id_v2');
    //Route::any('city/{id}',  'MapApiController@getCityByCityId');
    //Route::any('city',  'MapApiController@getAllCities');

    Route::any('getRefreshCache','MapApiController@getRefreshCache');
    Route::any('updateRefreshCache','MapApiController@updateRefreshCache');

    Route::match(['get','post'],'yalago_hotel','MapApiController@get_yalago_hotel_by_city_id');
});

/* 
    Calls Api Calls from frontend
*/
Route::any('activity',  'MapApiController@getActivity');
Route::any('transport-type',  'MapApiController@get_transport_types');
Route::any('room-type',  'MapApiController@get_room_types');
Route::any('eroam/hotel/{id}',  'MapApiController@viewEroamHotel');
Route::any('eroam/activity/{id}',  'MapApiController@viewEroamActivity');
Route::any('aot/hotel/{SupplierCode}','MapApiController@viewAOTHotel');
Route::match(['get','post'],'viator/category','MapApiController@get_all_label');
Route::match(['get','post'],'suburb-by-city/{city_id}','MapApiController@get_all_suburb_by_city');
Route::any('get_currencies', 'MapApiController@get_currency_values');
Route::any('all-currencies', 'MapApiController@get_all');
Route::any('regions-countries-cities-suburbs', 'MapApiController@getRegions');
Route::any('traveler-options',  'MapApiController@get_traveler_profile_options');
Route::match(['get','post'],'city/hotel','MapApiController@hotel_by_city_id');
Route::any('Expedia-Map-Hotels','MapApiController@expediaMappedHotels');
Route::any('hotel-categories','MapApiController@get_all_hotel_categories');
/* 
    Calls related Viator
*/
Route::match(['get','post'],'service/search/products', 'ViatorApiController@activities');
Route::match(['get','post'],'service/taxonomy/locations', 'ViatorApiController@location');
Route::match(['get','post'],'service/booking/hotels', 'ViatorApiController@hotels');
Route::match(['get','post'],'service/location', 'ViatorApiController@search_location'); 
Route::match(['get','post'],'search/product', 'ViatorApiController@view_activity');
Route::match(['get','post'],'booking/calculateprice', 'ViatorApiController@calculate_price');
Route::match(['get','post'],'save/viator/cagetory', 'ViatorApiController@save_category');
Route::match(['get','post'],'book-activities', 'ViatorApiController@bookActivities');   
Route::match(['get','post'],'get-tour-code', 'ViatorApiController@getTourCode');                
Route::match(['get','post'],'viator/category-list/{sType}', 'ViatorApiController@getCategoryList');                
Route::match(['get','post'],'service/tours', 'ViatorApiController@create_tours');   
Route::match(['get','post'],'tour/dates', 'ViatorApiController@get_tour_dates');
Route::match(['get','post'],'labels','MapApiController@get_all_label');
Route::match(['get','post'],'getTourGrades', 'ViatorApiController@getTourGrades');
Route::match(['get','post'],'getVoucher', 'ViatorApiController@getvoucher');

/* 
    Calls related BusBud
*/

Route::match(['get','post'],'busbud/transportList', 'BusBudApiController@getAllTransport'); 
Route::match(['get','post'],'bookBusBud', 'BusBudApiController@bookingProcess');
Route::match(['get','post'],'createBusBudCart','BusBudApiController@createBusBudCart');

/* 
    Calls related Itenary Save
*/

Route::any('saveOrderDetail','ItenaryOrderController@saveOrderDetail');
Route::any('getItineraryDetail/{orderId}','ItenaryOrderController@getItineraryDetail');

/* 
    Calls related Mystifly
*/

Route::any('mystifly/flight/search', 'MystiflyController@airLowFareSearchController');
Route::match(['get','post'],'mystifly/fareRules','MystiflyController@fareRulesCall');
Route::match(['get','post'],'mystifly/airRevalidate','MystiflyController@airRevalidateCall');
Route::any('mystifly/ticket/order',  'MystiflyController@ticketOrderController');
Route::any('mystifly/trip-details',   'MystiflyController@tripDetailsController');
Route::any('mystifly/flight/book',   'MystiflyController@bookingCall');
Route::any('get-city-airport-code',   'MystiflyController@getCityAirportCode');
Route::any('get-mystifly-voucher',   'MystiflyController@tripDetailsCall');
Route::any('mystifly/session',   'MystiflyController@get_session_id');
/* 
    End Calls related Mystifly
*/
Route::prefix('user')->group(function () {
    Route::any('check-customer','UserController@check_customer');
    Route::any('create_customer','UserController@customer_registeration');
    Route::any('get-by-id/{id}', 'UserController@user_get_by_id');
    Route::any('update-profile/step1','UserController@update_user_profile_step1');
    Route::any('update-profile/step2','UserController@update_user_profile_step2');
    Route::any('update-profile/step3','UserController@update_user_profile_step3');
    Route::any('update-profile/step4','UserController@update_user_profile_step4');
    Route::any('update-preferences', 'UserController@update_user_preferences');
    Route::any('get-itinerary', 'UserController@get_user_itinerary');
    Route::any('request-password-reset', 'UserController@request_change_password');
    Route::any('check-token', 'UserController@check_token');
    Route::any('reset-password', 'UserController@reset_user_password_from_app');
    Route::any('confirm-registration', 'UserController@confirm_registration');
    Route::any('get-itineraries', 'UserController@get_user_itineraries');
    Route::any('check', 'UserController@user_check_login');
}); 

Route::prefix('gadventure')->group(function () {
    Route::any('store-category','GadventureApiController@store_category');
    Route::any('store-tour','GadventureApiController@store_tour');
});
Route::any('get_trips/{user_id}','UserController@get_user_trips');
Route::any('getCardInformation','MapApiController@getCardinfo');
Route::get('get-itinerary-detail/{order_id}','ApiController@getItineraryDetail');
Route::get('apis','ApiManagerController@get');
Route::any('user/user-log', 'UserLogController@callUserLog');

/*
    Status : End
    Routes for APIS call from @frontend
*/


/*
    API for save trips
*/

Route::any('save-trip-detail','ApiController@saveTripDetail');
Route::get('get-saved-trips/{user_id}','ApiController@getSavedTrips');
Route::get('saved-trip-detail/{trip_id}','ApiController@getSavedTripDetail');
Route::get('all-country-for-select','ApiController@allCountryForSelect');
Route::get('all-country-for-select-temp','ApiController@allCountryForSelectTemp');
Route::get('flight-meal-select','ApiController@mealTypeForSelect');
/*Routes for expedia*/
Route::prefix('expedia')->group(function () {
    Route::match(['get','post'],'get-hotels','ExpediaApiController@getHotels');
    Route::match(['get','post'],'get-hotel-details','ExpediaApiController@getHotelDetails');
    Route::match(['get','post'],'get-hotel-info','ExpediaApiController@hotelInfo');
    Route::match(['get','post'],'get-booking','ExpediaApiController@expediaBooking');
    Route::match(['get','post'],'get-voucher','ExpediaApiController@hotelVoucher');
    Route::match(['get','post'],'get-percentage','ExpediaApiController@getPercentage');
});

/*Routes for Yalago*/
Route::prefix('yalago')->group(function () {
    Route::match(['get','post'],'get-countries','YalagoApiController@GetCountries');
    Route::match(['get','post'],'get-hotels','YalagoApiController@getHotels');
    Route::match(['get','post'],'get-hotels-availability','YalagoApiController@getHotelsAvailability');
    Route::match(['get','post'],'get-hotel-details','YalagoApiController@getHotelDetails');
    Route::match(['get','post'],'get-hotel-detail','YalagoApiController@getHotelDetails_book');
    Route::match(['get','post'],'get-booking','YalagoApiController@yalagoBooking');
    Route::match(['get','post'],'get-voucher','YalagoApiController@hotelVoucher');

    Route::match(['get','post'],'get-hotel-extra','YalagoApiController@getHotelDetails_book');
});





/* API for frontend */

Route::any('get-domain-logo','ApiController@getDomainLogo');
Route::any('get-social-links','ApiController@getSocialLinks');
Route::any('get-domain-product','ApiController@getDomainProduct');
Route::any('get-domain-product-dhara','ApiController@getDomainProductDhara');
Route::any('licensee-account','ApiController@getLicenseeAccount');
Route::any('get-inventory','ApiController@getInventoryData');

Route::any('get-default-activity','MapApiController@getDefaultActivity');
Route::any('get-default-activity-test','MapApiController@getDefaultActivityTest');


Route::prefix('freedom')->group(function () {
    Route::post('/', 'FreedomApiController@synchronize');
});

/* 
    Calls related RoomerFlex
*/
Route::prefix('roomerflex')->group(function () {
    Route::match(['get','post'],'requestToken','RoomerFlexController@requestToken');
    Route::match(['get','post'],'protectionToken','RoomerFlexController@protectionToken');
    
});
/* 
    End Calls related Mystifly
*/
    